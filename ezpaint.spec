# Note that this is NOT a relocatable package
%define ver      0.0.1
%define rel      SNAP
%define prefix   /usr

Summary: ezpaint
Name: ezpaint
Version: %ver
Release: %rel
Copyright: LGPL
Group: X11/Programs
Source: ftp://ftp.gnome.org/pub/GNOME/sources/ezpaint/ezpaint-%{ver}.tar.gz
BuildRoot: /tmp/ezpaint-root
URL: http://www.gnome.org
Docdir: %{prefix}/doc

%description
A library used to provide simple shape drawing capabilities to Gnome
office applications.

%package devel
Summary: ezpaint-devel
Group: X11/Libraries
Requires: ezpaint = %{PACKAGE_VERSION}

%description devel

%changelog

* Sat Jan 16 1999 Martin Baulig <martin@home-of-linux.org>

- Created this spec file.

%prep
%setup

%build
# Needed for snapshot releases.
if [ ! -f configure ]; then
  CFLAGS="$RPM_OPT_FLAGS" ./autogen.sh --prefix=%prefix
else
  CFLAGS="$RPM_OPT_FLAGS" ./configure --prefix=%prefix
fi

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT

make prefix=$RPM_BUILD_ROOT%{prefix} install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)

%doc AUTHORS COPYING ChangeLog NEWS README
%{prefix}/lib/lib*.so.*
%{prefix}/share/locale/*/*/*
%{prefix}/share/pixmaps/*

%files devel
%defattr(-, root, root)

%{prefix}/lib/lib*.so
%{prefix}/lib/*.a
%{prefix}/include/*
