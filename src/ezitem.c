/* Base class for EZ editable canvas items.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezitem.h"
#include "ezcontainer.h"

#include <gtk/gtksignal.h>

#include <config.h>

/* For internal use */
#ifdef GNOME_ENABLE_DEBUG


#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>

#define ez_item_invariants(x) G_STMT_START { \
  EZItem* _ezi = EZ_ITEM(x);  \
  if (_ezi->moving) { \
    g_assert(!_ezi->sizing); \
  }  \
  if (_ezi->sizing) { \
    g_assert(!_ezi->moving); \
  } \
  g_assert( (_ezi->parent && _ezi->paste) ||      \
            (!_ezi->parent && _ezi->floating && _ezi->paste && (!(GTK_OBJECT(_ezi)->flags & GNOME_CANVAS_ITEM_VISIBLE))) || \
            (!_ezi->parent && _ezi->toplevel && _ezi->paste) \
            );  \
  g_assert( (_ezi->parent && (EZ_ITEM(_ezi->parent)->paste == _ezi->paste)) || \
            (_ezi->parent == NULL) );              \
  \
  if (_ezi->editing || _ezi->sizing || _ezi->selected || _ezi->moving) { \
    g_assert(_ezi->toplevel || (_ezi->parent != NULL)); \
    g_assert(_ezi->floating == FALSE); \
  } \
  if (_ezi->floating) { \
    g_assert(_ezi->parent == NULL); \
    g_assert(!_ezi->toplevel); \
  }  \
  if (_ezi->toplevel) { \
    g_assert(_ezi->parent == NULL); \
  } \
  } G_STMT_END
#else 

#define ez_item_invariants(x)

#endif

static void ez_item_class_init (EZItemClass *class);
static void ez_item_init       (EZItem      *item);
static void ez_item_destroy    (GtkObject            *object);
static void ez_item_set_arg    (GtkObject            *object,
                                  GtkArg               *arg,
                                  guint                 arg_id);
static void ez_item_get_arg    (GtkObject            *object,
                                  GtkArg               *arg,
                                  guint                 arg_id);
static void ez_item_realize    (GnomeCanvasItem *item);

static void ez_item_unrealize   (GnomeCanvasItem *item);
static void ez_item_translate   (GnomeCanvasItem *item, double dx, double dy);

static void ez_item_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags);

static gint ez_item_event(GnomeCanvasItem* item, GdkEvent* event);

static double ez_item_point (GnomeCanvasItem *item, double x, double y, 
                               int cx, int cy, GnomeCanvasItem **actual_item);

static void ez_item_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);

static void ez_item_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);

static void gtk_marshal_NONE__ANCHOR_DOUBLE_DOUBLE_DOUBLE_DOUBLE (GtkObject * object,
                                                                   GtkSignalFunc func,
                                                                   gpointer func_data,
                                                                   GtkArg * args);


static void ez_item_real_start_editing (EZItem* ezi);

static void ez_item_real_stop_editing  (EZItem* ezi);

static void ez_item_real_start_moving  (EZItem* ezi);

static void ez_item_real_stop_moving   (EZItem* ezi);

static void ez_item_real_start_sizing  (EZItem* ezi);

static void ez_item_real_stop_sizing   (EZItem* ezi);

static void ez_item_real_select        (EZItem* ezi);

static void ez_item_real_unselect      (EZItem* ezi);

static void ez_item_real_move_resize   (EZItem* ezi, 
                                        EZAnchorType which_handle,
                                        double x1, double y1, 
                                        double x2, double y2);

static void ez_item_real_construct     (EZItem* ezi, EZPaste* paste);

static gboolean ez_item_real_pass_event    (EZItem* ezi, EZItem* origin, 
                                            GdkEvent* event);

enum {
  ARG_0
};


enum {
  START_EDITING,
  STOP_EDITING,
  
  START_MOVING,
  STOP_MOVING,

  START_SIZING,
  STOP_SIZING,

  SELECT,
  UNSELECT,
  
  MOVE_RESIZE,
  
  LAST_SIGNAL
};

static GnomeCanvasGroupClass *parent_class;

static gint item_signals[LAST_SIGNAL] = { 0 };

GtkType
ez_item_get_type (void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "EZItem",
      sizeof (EZItem),
      sizeof (EZItemClass),
      (GtkClassInitFunc) ez_item_class_init,
      (GtkObjectInitFunc) ez_item_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (gnome_canvas_group_get_type (), &item_info);
  }

  return item_type;
}

static void
ez_item_class_init (EZItemClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = gtk_type_class (gnome_canvas_group_get_type ());

  item_signals[START_EDITING] = 
    gtk_signal_new("start_editing",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, start_editing),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[STOP_EDITING] = 
    gtk_signal_new("stop_editing",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, stop_editing),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[START_SIZING] = 
    gtk_signal_new("start_sizing",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, start_sizing),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[STOP_SIZING] = 
    gtk_signal_new("stop_sizing",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, stop_sizing),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[START_MOVING] = 
    gtk_signal_new("start_moving",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, start_moving),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[STOP_MOVING] = 
    gtk_signal_new("stop_moving",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, stop_moving),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[SELECT] = 
    gtk_signal_new("select",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, select),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[UNSELECT] = 
    gtk_signal_new("unselect",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZItemClass, unselect),
                   gtk_signal_default_marshaller,
                   GTK_TYPE_NONE, 0);

  item_signals[MOVE_RESIZE] =
    gtk_signal_new ("move_resize",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (EZItemClass, move_resize),
		    gtk_marshal_NONE__ANCHOR_DOUBLE_DOUBLE_DOUBLE_DOUBLE,
		    GTK_TYPE_NONE, 5,
                    GTK_TYPE_ENUM,
                    GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE,
                    GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

  gtk_object_class_add_signals (object_class, item_signals, 
				LAST_SIGNAL);

  klass->start_editing = ez_item_real_start_editing;
  klass->stop_editing  = ez_item_real_stop_editing;

  klass->start_moving  = ez_item_real_start_moving;
  klass->stop_moving   = ez_item_real_stop_moving;

  klass->start_sizing  = ez_item_real_start_sizing;
  klass->stop_sizing   = ez_item_real_stop_sizing;

  klass->select        = ez_item_real_select;
  klass->unselect      = ez_item_real_unselect;

  klass->move_resize   = ez_item_real_move_resize;

  klass->construct     = ez_item_real_construct;

  klass->pass_event    = ez_item_real_pass_event;

  klass->get_size      = NULL;

  /* Some of these should not be implemented */

  item_class->realize     = ez_item_realize;
  item_class->update      = ez_item_update;
  item_class->render      = ez_item_render;
  item_class->event       = ez_item_event;
  item_class->point       = ez_item_point;
  item_class->unrealize   = ez_item_unrealize;
  item_class->translate   = ez_item_translate;
  item_class->bounds      = ez_item_bounds;

  object_class->destroy = ez_item_destroy;
  object_class->set_arg = ez_item_set_arg;
  object_class->get_arg = ez_item_get_arg;
}

static void
ez_item_init (EZItem *item)
{
  item->parent   = NULL;
  item->paste    = NULL;
  item->editing  = FALSE;
  item->moving   = FALSE;
  item->sizing   = FALSE;
  item->selected = FALSE;
  item->floating = TRUE;
  item->toplevel = FALSE;
}

static void
ez_item_destroy (GtkObject *object)
{
  EZItem *ezi;

  g_return_if_fail (object != NULL);
  g_return_if_fail (EZ_IS_ITEM (object));

  ezi = EZ_ITEM (object);

  ez_debug("Destroying EZItem %p", object);  

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ez_item_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  EZItem *ezi;

  item = GNOME_CANVAS_ITEM (object);
  ezi = EZ_ITEM (object);

  switch (arg_id) {

  default:
    g_warning("EZItem got an unknown arg type.");
    break;
  }

#if 0
  gnome_canvas_item_request_update(item);
#endif
}


static void
ez_item_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  EZItem* ezi;

  ezi = EZ_ITEM (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void
ez_item_realize (GnomeCanvasItem *item)
{
  EZItem* ezi = EZ_ITEM(item);

  ez_debug("Realizing EZItem %p", item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->realize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->realize) (item);
}


static void 
ez_item_unrealize   (GnomeCanvasItem *item)
{
  EZItem* ezi = EZ_ITEM(item);

  ez_debug("Unrealizing EZItem %p", item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize) (item);
}

static void 
ez_item_translate   (GnomeCanvasItem *item, double dx, double dy)
{
  EZItem* ezi = EZ_ITEM(item);
  
  ez_debug("Translating EZItem %p", item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->translate)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->translate) (item, dx, dy);

  gnome_canvas_item_request_update(item);
}


static void
ez_item_bounds (GnomeCanvasItem *item, double *x1, double *y1, 
                  double *x2, double *y2)
{
  EZItem* ezi = EZ_ITEM (item);

  ez_debug("Bounds of EZItem %p", item);
  
  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->bounds)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->bounds) (item, x1, y1, x2, y2);
}

static void 
ez_item_update (GnomeCanvasItem *item, double *affine, 
                  ArtSVP *clip_path, int flags)
{
  EZItem* ezi;

  ez_debug("Updating EZItem %p", item);

#ifdef GNOME_ENABLE_DEBUG_NO
  /* Draw little dot at 0,0 in our coordinates */
  {    
    if (item->canvas->aa)
      {
        ArtVpath vpath[20];
        ArtVpath *vpath2;
        ArtSVP* svp = NULL;
        int i;
        
        svp = gtk_object_get_data(GTK_OBJECT(item), "item_svp");
        
        i = 0;
        vpath[i].code = ART_MOVETO;
        vpath[i].x = -4.0;
        vpath[i].y = -4.0;
        i++;
        vpath[i].code = ART_LINETO;
        vpath[i].x = 0.0;
        vpath[i].y = 0.0;
        i++;
        vpath[i].code = ART_LINETO;
        vpath[i].x = 4.0;
        vpath[i].y = -4.0;
        i++;
        vpath[i].code = ART_MOVETO;
        vpath[i].x = -4.0;
        vpath[i].y = 4.0;
        i++;
        vpath[i].code = ART_LINETO;
        vpath[i].x = 0.0;
        vpath[i].y = 0.0;
        i++;
        vpath[i].code = ART_LINETO;
        vpath[i].x = 4.0;
        vpath[i].y = 4.0;
        i++;

        vpath[i].code = ART_END;
        vpath[i].x = 0;
        vpath[i].y = 0;
        
        vpath2 = art_vpath_affine_transform (vpath, affine);
        
        svp = art_svp_vpath_stroke (vpath2,
                                    ART_PATH_STROKE_JOIN_MITER,
                                    ART_PATH_STROKE_CAP_BUTT,
                                    1.0,
                                    4,
                                    0.5);

        gnome_canvas_item_update_svp (item, &svp, 
                                      art_svp_from_vpath (vpath2));
        
        gtk_object_set_data(GTK_OBJECT(item), "item_svp", svp);
        
        art_free (vpath2);
      }
  }
#endif

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->update)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->update) (item, affine, clip_path, flags);
}

static void 
ez_item_render(GnomeCanvasItem* item, GnomeCanvasBuf* buf)
{
  EZItem* ezi;
  ezi = EZ_ITEM (item);

  /*  ez_debug("Rendering EZItem %p", item); */

  /* Now call parent method to render children on top */
  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->render)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->render)(item, buf);
  

#ifdef GNOME_ENABLE_DEBUG_NO
  {
    ArtSVP* svp = gtk_object_get_data(GTK_OBJECT(item), "item_svp");
    if (svp != NULL) 
      {
        guint color = 0xff;
        
        gnome_canvas_render_svp (buf, svp, color);
      }
  }
#endif
}

static gint 
ez_item_event(GnomeCanvasItem* item, GdkEvent* event)
{
  EZItem* ezi = EZ_ITEM(item);
  EZContainer* parent = ezi->parent;
  EZItem* toplevel = ezi;

  g_return_val_if_fail(!ezi->floating, TRUE);

  /*  ez_debug("Event to EZItem %p", item);   */

  /* Pass the event to the paste */

  while (parent)
    {
      toplevel = EZ_ITEM(parent);
      parent = toplevel->parent;
    }

  g_return_val_if_fail(toplevel != NULL, TRUE);
  g_return_val_if_fail(toplevel->toplevel, TRUE);

  ez_paste_pass_event(ezi->paste, ezi, toplevel, event);

  return TRUE;
}

static double
ez_item_point (GnomeCanvasItem *item, double x, double y, 
               int cx, int cy, GnomeCanvasItem **actual_item)
{
  EZItem* ezi;

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->point)
    return (* GNOME_CANVAS_ITEM_CLASS(parent_class)->point)(item, x,y,cx,cy,actual_item);

  g_assert_not_reached();

  return 10000.0;
}

/************ End Canvas Item Functions ******************/


/* "Real" variants (our default handlers) */


static void
ez_item_real_start_editing (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Start editing EZItem %p", ezi);  

  ez_item_invariants(ezi);

  if (!ezi->editing)
    {
      ezi->editing = TRUE;

    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_stop_editing  (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Stop editing EZItem %p", ezi);  

  ez_item_invariants(ezi);

  if (ezi->editing)
    {
      if (ezi->moving)
        {
          ez_item_stop_moving(ezi);
        }

      ezi->editing = FALSE;

      /* This is needed if the cursor is on top of the item when we stop. */
      gdk_window_set_cursor(GTK_WIDGET(GNOME_CANVAS_ITEM(ezi)->canvas)->window, 
                            NULL);
    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_start_moving  (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Start moving EZItem %p", ezi);  

  ez_item_invariants(ezi);

  if (!ezi->moving)
    {
      GnomeCanvasItem* item = GNOME_CANVAS_ITEM(ezi);
      gint lastx, lasty;
      double dlastx, dlasty;
      GdkModifierType state;
      
      ezi->moving = TRUE;

      gnome_canvas_item_grab (item,
                              GDK_POINTER_MOTION_MASK | 
                              GDK_BUTTON_RELEASE_MASK | 
                              GDK_BUTTON_PRESS_MASK,
                              NULL, /* Set on enter */
                              GDK_CURRENT_TIME);
      
      /* This, and GDK_CURRENT_TIME, is sucky; but passing all that 
         to the start_moving signal is suckier, I'm guessing. */
      gdk_window_get_pointer(item->canvas->layout.bin_window,
                             &lastx, &lasty, &state);

      /* This is also sucky, but not as sucky as eating memory with this
         in every EZItem struct */
      gtk_object_set_data(GTK_OBJECT(ezi), "last_x", GINT_TO_POINTER(lastx)); 
      gtk_object_set_data(GTK_OBJECT(ezi), "last_y", GINT_TO_POINTER(lasty)); 
    }

  ez_item_invariants(ezi);
}

static void
ez_item_real_stop_moving   (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Stop moving EZItem %p", ezi);  

  ez_item_invariants(ezi);

  if (ezi->moving)
    {
      ezi->moving = FALSE;

      gnome_canvas_item_ungrab(GNOME_CANVAS_ITEM(ezi), GDK_CURRENT_TIME);

      /* Save some memory */
      gtk_object_remove_data(GTK_OBJECT(ezi), "last_x");
      gtk_object_remove_data(GTK_OBJECT(ezi), "last_y");
    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_start_sizing  (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Start sizing EZItem %p", ezi);  

  ez_item_invariants(ezi);

  if (!ezi->sizing)
    {
      ezi->sizing = TRUE;

    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_stop_sizing   (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Stop sizing EZItem %p", ezi);  

  ez_item_invariants(ezi);

  if (ezi->sizing)
    {
      ezi->sizing = FALSE;

    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_select        (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Select EZItem %p", ezi);  

  ez_item_invariants(ezi);
  
  if (!ezi->selected)
    {
      ezi->selected = TRUE;

    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_unselect      (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Unselect EZItem %p", ezi);  

  ez_item_invariants(ezi);
  
  if (ezi->selected)
    {
      ezi->selected = FALSE;

    }

  ez_item_invariants(ezi);


}

static void
ez_item_real_move_resize   (EZItem* ezi, 
                            EZAnchorType which_handle,
                            double x1, double y1, 
                            double x2, double y2)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  ez_debug("Move-resize EZItem %p  (%g,%g) (%g,%g) ", ezi, x1, y1, x2, y2);  

  g_return_if_fail(x2 >= x1);
  g_return_if_fail(y2 >= y1);

  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(ezi));
}

static gboolean
ez_item_real_pass_event    (EZItem* ezi, EZItem* origin, 
                            GdkEvent* event)
{
  GnomeCanvasItem* item;

  g_return_val_if_fail(ezi != NULL, FALSE);
  g_return_val_if_fail(EZ_IS_ITEM(ezi), FALSE);
  g_return_val_if_fail(!ezi->floating, FALSE);

  item = GNOME_CANVAS_ITEM(ezi);

  ez_debug("Pass event to %s %p", 
           gtk_type_name(GTK_OBJECT(ezi)->klass->type),
           ezi);    

  switch (event->type){
    
  case GDK_ENTER_NOTIFY:
    {
      GdkCursor* ptr;
      ptr = gdk_cursor_new(GDK_FLEUR);
      gdk_window_set_cursor(GTK_WIDGET(item->canvas)->window, ptr);
      gdk_cursor_destroy (ptr);
      return TRUE;
    }
    break;
    
  case GDK_LEAVE_NOTIFY:
    {
      gdk_window_set_cursor(GTK_WIDGET(item->canvas)->window, NULL);
      return TRUE;
    }
    break;
    
  case GDK_BUTTON_PRESS:
    {
      if (event->button.button != 1) return FALSE;      

      if (!ezi->moving)
        {
          ez_item_start_moving(ezi);
          return FALSE; /* Because we may want to do more with this in container */
        }
    }
    break;
    
  case GDK_BUTTON_RELEASE:
    {
      if (event->button.button != 1) return FALSE;

      if (ezi->moving)
        {
          ez_item_stop_moving(ezi);
          return TRUE;
        }
    }
    break;

  case GDK_MOTION_NOTIFY:
    {
      if (ezi->moving) {
        double newx, newy, dx, dy;
        gpointer lastxp, lastyp;
        gint lastx, lasty;
        double x1, y1, x2, y2;
        
        newx = event->motion.x;
        newy = event->motion.y;
                
        lastxp = gtk_object_get_data(GTK_OBJECT(ezi), "last_x");
        lastyp = gtk_object_get_data(GTK_OBJECT(ezi), "last_y");
        
        lastx = GPOINTER_TO_INT(lastxp);
        lasty = GPOINTER_TO_INT(lastyp);

        dx = newx - lastx;
        dy = newy - lasty;

        gtk_object_set_data(GTK_OBJECT(ezi), "last_x", GINT_TO_POINTER((gint)newx));
        gtk_object_set_data(GTK_OBJECT(ezi), "last_y", GINT_TO_POINTER((gint)newy));

        ez_item_get_size(ezi, &x1, &y1, &x2, &y2);

        x1 += dx; 
        x2 += dx;
        y1 += dy;
        y2 += dy;

        ez_item_move_resize(ezi, EZ_ANCHOR_CENTER, x1, y1, x2, y2);

        return TRUE;
      } 
    }
    break; /* Motion notify */

  default:
    break;
  }

  return FALSE;
}

static void 
ez_item_real_construct     (EZItem* ezi, EZPaste* paste)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));
  g_return_if_fail(paste != NULL);
  g_return_if_fail(EZ_IS_PASTE(paste));

  ez_debug("Constructing EZItem %p (actual type: %s)", ezi, 
           gtk_type_name(GTK_OBJECT(ezi)->klass->type));    

  gnome_canvas_item_hide(GNOME_CANVAS_ITEM(ezi));

  ezi->paste = paste;
}

/* "Fake" variants (exported for users to invoke the signal) */

void
ez_item_start_editing (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (!EZ_ITEM_EDITING(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[START_EDITING]);
    }
}

void
ez_item_stop_editing  (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (EZ_ITEM_EDITING(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[STOP_EDITING]);
    }
}

void
ez_item_start_moving  (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (!EZ_ITEM_MOVING(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[START_MOVING]);
    }
}

void
ez_item_stop_moving   (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (EZ_ITEM_MOVING(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[STOP_MOVING]);
    }
}

void
ez_item_start_sizing  (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (!EZ_ITEM_SIZING(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[START_SIZING]);
    }
}

void
ez_item_stop_sizing   (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (EZ_ITEM_SIZING(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[STOP_SIZING]);
    }
}

void
ez_item_select        (EZItem* ezi)
{
  if (!EZ_ITEM_SELECTED(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[SELECT]);
    }
}

void
ez_item_unselect      (EZItem* ezi)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (EZ_ITEM_SELECTED(ezi))
    {
      gtk_signal_emit(GTK_OBJECT(ezi), item_signals[UNSELECT]);
    }
}

void
ez_item_move_resize   (EZItem* ezi, 
                       EZAnchorType which_handle,
                       double x1, double y1, 
                       double x2, double y2)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));
  g_return_if_fail(x2 >= x1);
  g_return_if_fail(y2 >= y1);

  ez_debug("Emitting move-resize %p (%g,%g) (%g,%g)", ezi, x1, y1, x2, y2);

  gtk_signal_emit(GTK_OBJECT(ezi), item_signals[MOVE_RESIZE],
                  which_handle,
                  x1, y1, x2, y2);
}

gboolean
ez_item_pass_event    (EZItem* ezi, EZItem* origin, GdkEvent* event)
{
  EZItemClass* klass;

  g_return_val_if_fail(ezi != NULL, FALSE);
  g_return_val_if_fail(EZ_IS_ITEM(ezi), FALSE);

  klass = EZ_ITEM_CLASS(GTK_OBJECT(ezi)->klass);

  if (klass->pass_event)
    return (*klass->pass_event)(ezi, origin, event);
  else
    return FALSE;
}

void      
ez_item_get_size      (EZItem* ezi, double* x1, double* y1, double* x2, double* y2)
{
  EZItemClass* klass;

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));
  

  klass = EZ_ITEM_CLASS(GTK_OBJECT(ezi)->klass);

  g_return_if_fail(klass->get_size);

  (*klass->get_size)(ezi, x1, y1, x2, y2);
}


/*** Misc. additional functions ***/


void      
ez_item_raise         (EZItem* ezi)
{

  g_warning(__FUNCTION__);
}

void      
ez_item_lower         (EZItem* ezi)
{

  g_warning(__FUNCTION__);
}

/* Build a new item */

EZItem*   
ez_item_build         (EZPaste* paste, GtkType type)
{
  GnomeCanvasItem* item;
  EZItemClass* klass;
  EZItem* ezi;

  g_return_val_if_fail(paste != NULL, NULL);
  g_return_val_if_fail(EZ_IS_PASTE(paste), NULL);
  
  item = gnome_canvas_item_new ( GNOME_CANVAS_GROUP(paste),
                                 type,
                                 NULL );

  ezi = EZ_ITEM(item);

  klass = EZ_ITEM_CLASS(GTK_OBJECT(item)->klass);

  if (klass->construct)
    (*klass->construct) (ezi, paste);

  ez_item_invariants(ezi);

  return ezi;
}

/* Marshaller */

typedef void (*GtkSignal_NONE__ANCHOR_DOUBLE_DOUBLE_DOUBLE_DOUBLE) (GtkObject * object,
                                                                    EZAnchorType anchor,
                                                                    gdouble arg1,
                                                                    gdouble arg2,
                                                                    gdouble arg3,
                                                                    gdouble arg4,
                                                                    gpointer user_data);

static void gtk_marshal_NONE__ANCHOR_DOUBLE_DOUBLE_DOUBLE_DOUBLE (GtkObject * object,
                                                                  GtkSignalFunc func,
                                                                  gpointer func_data,
                                                                  GtkArg * args)
{
  GtkSignal_NONE__ANCHOR_DOUBLE_DOUBLE_DOUBLE_DOUBLE rfunc;
  rfunc = (GtkSignal_NONE__ANCHOR_DOUBLE_DOUBLE_DOUBLE_DOUBLE) func;
  (*rfunc) (object,
            GTK_VALUE_ENUM   (args[0]),
	    GTK_VALUE_DOUBLE (args[1]),
	    GTK_VALUE_DOUBLE (args[2]),
	    GTK_VALUE_DOUBLE (args[3]),
	    GTK_VALUE_DOUBLE (args[4]),
	    func_data);
}

