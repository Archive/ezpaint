/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZHandle: Canvas item representing XORd handle
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_HANDLE_H
#define EZ_HANDLE_H

#include <libgnomeui/gnome-canvas.h>

#include <libart_lgpl/art_svp.h>

BEGIN_GNOME_DECLS

/* For now this is like GtkAnchorType, but I want to add new 
   values eventually */

typedef enum
{
  EZ_ANCHOR_CENTER,
  EZ_ANCHOR_NORTH,
  EZ_ANCHOR_NORTH_WEST,
  EZ_ANCHOR_NORTH_EAST,
  EZ_ANCHOR_SOUTH,
  EZ_ANCHOR_SOUTH_WEST,
  EZ_ANCHOR_SOUTH_EAST,
  EZ_ANCHOR_WEST,
  EZ_ANCHOR_EAST,
  EZ_ANCHOR_N	=	EZ_ANCHOR_NORTH,
  EZ_ANCHOR_NW	=	EZ_ANCHOR_NORTH_WEST,
  EZ_ANCHOR_NE	=	EZ_ANCHOR_NORTH_EAST,
  EZ_ANCHOR_S	=	EZ_ANCHOR_SOUTH,
  EZ_ANCHOR_SW	=	EZ_ANCHOR_SOUTH_WEST,
  EZ_ANCHOR_SE	=	EZ_ANCHOR_SOUTH_EAST,
  EZ_ANCHOR_W	=	EZ_ANCHOR_WEST,
  EZ_ANCHOR_E	=	EZ_ANCHOR_EAST
} EZAnchorType;

#define EZ_TYPE_HANDLE            (ez_handle_get_type ())
#define EZ_HANDLE(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_HANDLE, EZHandle))
#define EZ_HANDLE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_HANDLE, EZHandleClass))
#define EZ_IS_HANDLE(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_HANDLE))
#define EZ_IS_HANDLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_HANDLE))

typedef struct _EZHandle EZHandle;
typedef struct _EZHandleClass EZHandleClass;

struct _EZHandle {
  GnomeCanvasItem item;

  /* struct members are all private. */

  /* Misleading: the corner or side of the object we're handling, not
     the anchor point of the handle. So if you want to have a control
     point on the bottom left corner, you want EZ_ANCHOR_SW. */
  EZAnchorType anchor;
  
  /* Item coords position; this is the position opposite the anchor
     point, and should be the point on the object you are trying to
     control. If the user flips an object you can just flip the anchor 
     without changing x and y. */
  double x;  
  double y;

  /* Our vector path to draw to the canvas */
  ArtSVP* svp;

  int prelight  : 1;   /* show that the user has successfully gotten the mouse
                          over this tiny thing */
  int dragging  : 1;   /* we're dragging the handle */ 
  int sensitive : 1;  /* allow drags */
};

struct _EZHandleClass {
  GnomeCanvasItemClass parent_class;

  /* This is called only if the *user* moves the handle,
     not if the programmer sets x and y. Item coords. */
  void (* moved)(EZHandle* handle, double new_x, double new_y, double dx, double dy);

  /* Called when the handle starts to move in response to user input. */
  
  void (* start_move)(EZHandle* handle);

  /* Called when it stops. */

  void (* stop_move)(EZHandle* handle);
};

/* Standard Gtk function */

GtkType   ez_handle_get_type (void);

/* Basically for debugging */
const gchar* ez_anchor_name(EZAnchorType a);

END_GNOME_DECLS

#endif

