/* Paste board for EZ editable objects.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezitem.h"
#include "ezcontainer.h"

#include <gtk/gtksignal.h>
#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>

#include <math.h>

#include <config.h>

static void ez_paste_class_init (EZPasteClass *class);
static void ez_paste_init       (EZPaste      *ezi);
static void ez_paste_destroy    (GtkObject            *object);
static void ez_paste_set_arg    (GtkObject            *object,
                                 GtkArg               *arg,
                                 guint                 arg_id);
static void ez_paste_get_arg    (GtkObject            *object,
                                 GtkArg               *arg,
                                 guint                 arg_id);
static void ez_paste_realize    (GnomeCanvasItem *item);

static void ez_paste_unrealize   (GnomeCanvasItem *item);
static void ez_paste_translate   (GnomeCanvasItem *item, double dx, double dy);

static void ez_paste_update      (GnomeCanvasItem *item, double *affine, 
                                  ArtSVP *clip_path, int flags);

static gint ez_paste_event(GnomeCanvasItem* item, GdkEvent* event);

static double ez_paste_point (GnomeCanvasItem *item, double x, double y, 
                              int cx, int cy, GnomeCanvasItem **actual_item);

static void ez_paste_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);

static void ez_paste_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);

enum {
  ARG_0,
  ARG_X1,
  ARG_Y1,
  ARG_X2,
  ARG_Y2,
  ARG_BG_COLOR,
  ARG_BG_COLOR_GDK,
  ARG_BG_COLOR_RGBA,
  ARG_GRID_COLOR,
  ARG_GRID_COLOR_GDK,
  ARG_GRID_COLOR_RGBA,
  ARG_HGRIDSPACE,
  ARG_VGRIDSPACE,
  ARG_GRID,
  ARG_BG
};

enum {
  
  LAST_SIGNAL
};

static GnomeCanvasGroupClass *parent_class;

static gint item_signals[LAST_SIGNAL] = { 0 };

GtkType
ez_paste_get_type (void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "EZPaste",
      sizeof (EZPaste),
      sizeof (EZPasteClass),
      (GtkClassInitFunc) ez_paste_class_init,
      (GtkObjectInitFunc) ez_paste_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (gnome_canvas_group_get_type (), &item_info);
  }

  return item_type;
}

static void
ez_paste_class_init (EZPasteClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  EZItemClass* ezitem_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  ezitem_class = (EZItemClass* ) klass;

  parent_class = gtk_type_class (gnome_canvas_group_get_type ());
  
  gtk_object_add_arg_type ("EZPaste::x1", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X1);
  gtk_object_add_arg_type ("EZPaste::y1", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y1);
  gtk_object_add_arg_type ("EZPaste::x2", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X2);
  gtk_object_add_arg_type ("EZPaste::y2", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y2);

  gtk_object_add_arg_type ("EZPaste::bg_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_BG_COLOR);
  gtk_object_add_arg_type ("EZPaste::bg_color_gdk", GTK_TYPE_GDK_COLOR, GTK_ARG_READWRITE, ARG_BG_COLOR_GDK);
  gtk_object_add_arg_type ("EZPaste::bg_color_rgba", GTK_TYPE_UINT, GTK_ARG_READWRITE, ARG_BG_COLOR_RGBA);

  gtk_object_add_arg_type ("EZPaste::grid_color", GTK_TYPE_STRING, GTK_ARG_WRITABLE, ARG_GRID_COLOR);
  gtk_object_add_arg_type ("EZPaste::grid_color_gdk", GTK_TYPE_GDK_COLOR, GTK_ARG_READWRITE, ARG_GRID_COLOR_GDK);
  gtk_object_add_arg_type ("EZPaste::grid_color_rgba", GTK_TYPE_UINT, GTK_ARG_READWRITE, ARG_GRID_COLOR_RGBA);

  gtk_object_add_arg_type ("EZPaste::hgridspace", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_HGRIDSPACE);
  gtk_object_add_arg_type ("EZPaste::vgridspace", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_VGRIDSPACE);

  gtk_object_add_arg_type ("EZPaste::grid", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_GRID);
  gtk_object_add_arg_type ("EZPaste::bg", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_BG);

  /* Some of these should not be implemented */

  item_class->realize     = ez_paste_realize;
  item_class->update      = ez_paste_update;
  item_class->render      = ez_paste_render;
  item_class->event       = ez_paste_event;
  item_class->point       = ez_paste_point;
  item_class->unrealize   = ez_paste_unrealize;
  item_class->translate   = ez_paste_translate;
  item_class->bounds      = ez_paste_bounds;

  object_class->destroy = ez_paste_destroy;
  object_class->set_arg = ez_paste_set_arg;
  object_class->get_arg = ez_paste_get_arg;
}

static void
ez_paste_init (EZPaste *paste)
{
  paste->x1 = paste->y1 = paste->x2 = paste->y2 = 0.0;
  paste->items = NULL;
  paste->editing_item = NULL;
  paste->grid_color = 0xff; /* black */
  paste->bg_color = 0xffffffff; /* white */
  paste->grid_svp = NULL;
  paste->bg_svp = NULL;
  paste->has_grid = FALSE;
  paste->has_bg = FALSE;
  paste->recalc_grid = TRUE;
  paste->recalc_bg = TRUE;
  paste->vgridspace = 5.0;
  paste->hgridspace = 5.0;
}

static void
ez_paste_destroy (GtkObject *object)
{
  EZPaste *paste;

  ez_debug("Destroying EZPaste %p", object);

  g_return_if_fail (object != NULL);
  g_return_if_fail (EZ_IS_PASTE(object));

  paste = EZ_PASTE (object);

  if (paste->bg_svp) art_svp_free(paste->bg_svp);
  if (paste->grid_svp) art_svp_free(paste->grid_svp);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ez_paste_set_bg_color(EZPaste* paste, guint color)
{
  paste->bg_color = color;

  if (paste->bg_svp != NULL)
    {
      gnome_canvas_item_request_redraw_svp (GNOME_CANVAS_ITEM(paste), 
                                            paste->bg_svp);
    }
  else 
    {
      paste->has_bg = TRUE;
      paste->recalc_bg = TRUE;
    }
}

static void
ez_paste_set_grid_color(EZPaste* paste, guint color)
{
  paste->grid_color = color;

  if (paste->grid_svp != NULL)
    {
      gnome_canvas_item_request_redraw_svp (GNOME_CANVAS_ITEM(paste), 
                                            paste->grid_svp);
    }
  else 
    {
      paste->has_grid = TRUE;
      paste->recalc_grid = TRUE;
    }
}

static void
ez_paste_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  EZPaste *paste;

  item = GNOME_CANVAS_ITEM (object);
  paste = EZ_PASTE (object);

  switch (arg_id) {

  case ARG_X1:
    paste->x1 = GTK_VALUE_DOUBLE(*arg);
    paste->recalc_bg = TRUE;
    paste->recalc_grid = TRUE;
    break;

  case ARG_Y1:
    paste->y1 = GTK_VALUE_DOUBLE(*arg);
    paste->recalc_bg = TRUE;
    paste->recalc_grid = TRUE;
    break;

  case ARG_X2:
    paste->x2 = GTK_VALUE_DOUBLE(*arg);
    paste->recalc_bg = TRUE;
    paste->recalc_grid = TRUE;
    break;

  case ARG_Y2:
    paste->y2 = GTK_VALUE_DOUBLE(*arg);
    paste->recalc_bg = TRUE;
    paste->recalc_grid = TRUE;
    break;

  case ARG_BG_COLOR:
    {
      GdkColor color;
      if (gnome_canvas_get_color (item->canvas, GTK_VALUE_STRING (*arg), &color))
        {
          ez_paste_set_bg_color(paste,
                                ((color.red & 0xff00) << 16) |
                                ((color.green & 0xff00) << 8) |
                                (color.blue & 0xff00) |
                                0xff);
          
        }
      else 
        {
          paste->has_bg = FALSE;
          paste->recalc_bg = TRUE;
        }
    }
    break;

  case ARG_BG_COLOR_GDK:
    {
      GdkColor color = * ((GdkColor *) GTK_VALUE_BOXED (*arg));
      ez_paste_set_bg_color(paste,
                            ((color.red & 0xff00) << 16) |
                            ((color.green & 0xff00) << 8) |
                            (color.blue & 0xff00) |
                            0xff);
    }
    break;

  case ARG_BG_COLOR_RGBA:
    {
      ez_paste_set_bg_color(paste, GTK_VALUE_UINT(*arg));
    }  
    break;





  case ARG_GRID_COLOR:
    {
      GdkColor color;
      if (gnome_canvas_get_color (item->canvas, GTK_VALUE_STRING (*arg), &color))
        {
          ez_paste_set_grid_color(paste,
                                  ((color.red & 0xff00) << 16) |
                                  ((color.green & 0xff00) << 8) |
                                  (color.blue & 0xff00) |
                                  0xff);
          
        }
      else 
        {
          paste->has_grid = FALSE;
          paste->recalc_grid = TRUE;
        }
    }
    break;

  case ARG_GRID_COLOR_GDK:
    {
      GdkColor color = * ((GdkColor *) GTK_VALUE_BOXED (*arg));
      ez_paste_set_grid_color(paste,
                            ((color.red & 0xff00) << 16) |
                            ((color.green & 0xff00) << 8) |
                            (color.blue & 0xff00) |
                            0xff);
    }
    break;

  case ARG_GRID_COLOR_RGBA:
    {
      ez_paste_set_grid_color(paste, GTK_VALUE_UINT(*arg));
    }  
    break;

  case ARG_HGRIDSPACE:
    {
      paste->hgridspace = GTK_VALUE_DOUBLE(*arg);
      if (paste->hgridspace < 0.5)
        paste->hgridspace = 0.5;
      paste->recalc_grid = TRUE;
    }
    break;

  case ARG_VGRIDSPACE:
    {
      paste->vgridspace = GTK_VALUE_DOUBLE(*arg);
      if (paste->vgridspace < 0.5)
        paste->vgridspace = 0.5;
      paste->recalc_grid = TRUE;
    }
    break;

  case ARG_GRID:
    {
      if (GTK_VALUE_BOOL(*arg) != paste->has_grid)
        {
          paste->has_grid = GTK_VALUE_BOOL(*arg);
          paste->recalc_grid = TRUE;
        }
    }
    break;

  case ARG_BG:
    {
      if (GTK_VALUE_BOOL(*arg) != paste->has_bg)
        {
          paste->has_bg = GTK_VALUE_BOOL(*arg);
          paste->recalc_bg = TRUE;
        }
    }
    break;

  default:
    g_warning("EZPaste got an unknown arg type.");
    break;
  }

  if (paste->recalc_bg || paste->recalc_grid)
    {
      gnome_canvas_item_request_update(item);
    }
}

static void
ez_paste_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  EZPaste* paste;

  paste = EZ_PASTE(object);

  switch (arg_id) {


  case ARG_X1:
    GTK_VALUE_DOUBLE(*arg) = paste->x1;
    break;

  case ARG_Y1:
    GTK_VALUE_DOUBLE(*arg) = paste->y1;
    break;

  case ARG_X2:
    GTK_VALUE_DOUBLE(*arg) = paste->x2;
    break;

  case ARG_Y2:
    GTK_VALUE_DOUBLE(*arg) = paste->y2;
    break;

  case ARG_BG_COLOR:
    g_warning("getting color not implemented");
    GTK_VALUE_BOXED(*arg) = NULL;
    break;

  case ARG_BG_COLOR_GDK:
    g_warning("getting color not implemented");
    GTK_VALUE_BOXED(*arg) = NULL;
    break;

  case ARG_BG_COLOR_RGBA:
    GTK_VALUE_UINT(*arg) = paste->bg_color;
    break;

  case ARG_GRID_COLOR:
    g_warning("getting color not implemented");
    GTK_VALUE_BOXED(*arg) = NULL;
    break;

  case ARG_GRID_COLOR_GDK:
    g_warning("getting color not implemented");
    GTK_VALUE_BOXED(*arg) = NULL;
    break;

  case ARG_GRID_COLOR_RGBA:
    GTK_VALUE_UINT(*arg) = paste->bg_color;
    break;

  case ARG_HGRIDSPACE:
    GTK_VALUE_DOUBLE(*arg) = paste->hgridspace;
    break;

  case ARG_VGRIDSPACE:
    GTK_VALUE_DOUBLE(*arg) = paste->vgridspace;
    break;

  case ARG_GRID:
    GTK_VALUE_BOOL(*arg) = paste->has_grid;
    break;

  case ARG_BG:
    GTK_VALUE_BOOL(*arg) = paste->has_bg;
    break;

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void
ez_paste_realize (GnomeCanvasItem *item)
{
  EZPaste* paste = EZ_PASTE(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->realize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->realize) (item);
}


static void 
ez_paste_unrealize   (GnomeCanvasItem *item)
{
  EZPaste* paste = EZ_PASTE(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize) (item);
}

static void 
ez_paste_translate   (GnomeCanvasItem *item, double dx, double dy)
{
  EZPaste* paste = EZ_PASTE(item);
  
  paste->x1 += dx;
  paste->y1 += dy;
  paste->x2 += dx;
  paste->y2 += dy;

  paste->recalc_bg = TRUE;
  paste->recalc_grid = TRUE;

  gnome_canvas_item_request_update(item);
}

static void
ez_paste_bounds (GnomeCanvasItem *item, 
                 double *x1, double *y1, 
                 double *x2, double *y2)
{
  EZPaste* paste = EZ_PASTE (item);

  *x1 = paste->x1;
  *y1 = paste->y1;
  *x2 = paste->x2;
  *y2 = paste->y2;
}

#include <time.h>

static void 
ez_paste_update (GnomeCanvasItem *item, double *affine, 
                 ArtSVP *clip_path, int flags)
{
  EZPaste* paste = EZ_PASTE(item);
  time_t t = time(NULL);
  int total_reupdate = 0;

  ez_debug("Updating EZPaste %p", item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->update)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->update) (item, affine, clip_path, flags);

  ez_debug("Update begin");
  
  total_reupdate |= GNOME_CANVAS_UPDATE_AFFINE |  GNOME_CANVAS_UPDATE_CLIP | GNOME_CANVAS_UPDATE_VISIBILITY;
  
  /*  if (flags & total_reupdate) */
    {
      paste->recalc_bg = paste->recalc_grid = TRUE;
    }

  if (paste->recalc_bg)
    {      
      if (paste->has_bg)
        {
          ArtVpath vpath[6];
          ArtVpath *vpath2;
          const double x1 = paste->x1, y1 = paste->y1, 
            x2 = paste->x2, y2 = paste->y2;
          int i;
          
          gnome_canvas_item_reset_bounds (item);
          
          i = 0;
          vpath[i].code = ART_MOVETO;
          vpath[i].x = x1;
          vpath[i].y = y1;
          i++;
          vpath[i].code = ART_LINETO;
          vpath[i].x = x1;
          vpath[i].y = y2;
          i++;
          vpath[i].code = ART_LINETO;
          vpath[i].x = x2;
          vpath[i].y = y2;
          i++;
          vpath[i].code = ART_LINETO;
          vpath[i].x = x2;
          vpath[i].y = y1;
          i++;
          vpath[i].code = ART_LINETO;
          vpath[i].x = x1;
          vpath[i].y = y1;
          i++;

          vpath[i].code = ART_END;
          vpath[i].x = 0;
          vpath[i].y = 0;
          
          vpath2 = art_vpath_affine_transform (vpath, affine);
          
          gnome_canvas_item_update_svp_clip (item, &paste->bg_svp, 
                                             art_svp_from_vpath (vpath2), 
                                             clip_path);
          ez_debug("bg updated");

          art_free (vpath2);
        }
      else if (paste->bg_svp != NULL)
        {
          art_svp_free(paste->bg_svp);
          paste->bg_svp = NULL;
        }
    }

  if (paste->recalc_grid)
    {
      
      if (paste->has_grid)
        {
          ArtVpath* vpath;
          const double x1 = paste->x1, y1 = paste->y1, 
            x2 = paste->x2, y2 = paste->y2;
          ArtPoint pi, pc;
          double linewidth;
          ArtSVP *svp;
          int i;
          const double width = x2 - x1;
          const double height = y2 - y1;
          double counter;
          double nlines; 

          gnome_canvas_item_reset_bounds (item);

          /* Each line is a moveto and a lineto. Then we have an
             ART_END. 2 extras are also added just in case the
             rounding is broken. */
          
          nlines = (width/paste->vgridspace + height/paste->hgridspace) * 2 + 3;

          vpath = art_new (ArtVpath, (gsize)nlines);
          
          /* First fill in the horizontal lines */
          
          i = 0;
          counter = y1;
          while (counter < y2)
            {
              pi.x = x1;
              pi.y = counter;
              art_affine_point (&pc, &pi, affine);
              vpath[i].code = ART_MOVETO;
              vpath[i].x = pc.x;
              vpath[i].y = pc.y;
              ++i;

              pi.x = x2;
              pi.y = counter;
              art_affine_point (&pc, &pi, affine);
              vpath[i].code = ART_LINETO;
              vpath[i].x = pc.x;
              vpath[i].y = pc.y;
              ++i;

              counter += paste->vgridspace;
            }

          /* Now the vertical */
     
          counter = x1;
          while (counter < x2)
            {
              pi.x = counter;
              pi.y = y1;
              art_affine_point (&pc, &pi, affine);
              vpath[i].code = ART_MOVETO;
              vpath[i].x = pc.x;
              vpath[i].y = pc.y;
              ++i;

              pi.x = counter;
              pi.y = y2;
              art_affine_point (&pc, &pi, affine);
              vpath[i].code = ART_LINETO;
              vpath[i].x = pc.x;
              vpath[i].y = pc.y;
              ++i;

              counter += paste->hgridspace;
            }

          vpath[i].code = ART_END;
          vpath[i].x = 0;
          vpath[i].y = 0;
          
          /* Always 1 pixel wide for now; should eventually be
             configurable? */
          linewidth = 1.0 * art_affine_expansion (affine);
          
          if (linewidth < 0.5)
            linewidth = 0.5;

          ez_debug("Filled vpath (%lu sec)", time(NULL) - t);
          
          svp = art_svp_vpath_stroke (vpath,
                                      ART_PATH_STROKE_JOIN_MITER,
                                      ART_PATH_STROKE_CAP_BUTT,
                                      linewidth,
                                      4,
                                      0.5);
          ez_debug("Stroked (%lu sec)", time(NULL) - t);

          art_free (vpath);
          
          gnome_canvas_item_update_svp_clip (item, &paste->grid_svp, 
                                             svp, clip_path);

          ez_debug("Clipped (%lu sec)", time(NULL) - t);

        }
      else if (paste->grid_svp != NULL)
        {
          art_svp_free(paste->grid_svp);
          paste->grid_svp = NULL;
        }
    }

  ez_debug("Done (%lu sec)", time(NULL) - t);

  paste->recalc_bg = FALSE;
  paste->recalc_grid = FALSE;
}

static void 
ez_paste_render(GnomeCanvasItem* item, GnomeCanvasBuf* buf)
{
  EZPaste* paste;
  paste = EZ_PASTE (item);

  /*  ez_debug("Rendering EZPaste %p", item); */

  if (paste->bg_svp != NULL) 
    {
      gnome_canvas_render_svp (buf, paste->bg_svp, paste->bg_color);
    }

  if (paste->grid_svp != NULL) 
    {
      gnome_canvas_render_svp (buf, paste->grid_svp, paste->grid_color);
    }

  /* Now call parent method to render children on top */
  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->render)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->render)(item, buf);
}

static gint 
ez_paste_event(GnomeCanvasItem* item, GdkEvent* event)
{
  EZPaste* paste = EZ_PASTE(item);

  ez_paste_pass_event(paste, NULL, NULL, event);
  
  return TRUE;
}

/* There are "issues" with our GnomeCanvasGroup heritage and the
   bounds of the EZPaste. We want to guarantee that these issues do
   not exist by forcing all items to stay within the pasteboard. I'm
   not sure yet how to do that. */

static double
ez_paste_point (GnomeCanvasItem *item, double x, double y, 
                int cx, int cy, GnomeCanvasItem **actual_item)
{
  EZPaste* paste = EZ_PASTE(item);

  const double x1 = paste->x1, y1 = paste->y1, 
    x2 = paste->x2, y2 = paste->y2;
  double dx, dy;

  /* On top of the pasteboard */

  if ((x >= x1) && (y >= y1) && (x <= x2) && (y <= y2)) {
    double ret = 0.0;

    /* Set actual item properly */
    if (*GNOME_CANVAS_ITEM_CLASS(parent_class)->point)
      ret = (*GNOME_CANVAS_ITEM_CLASS(parent_class)->point)(item, x, y, cx, cy, actual_item);

    if (*actual_item == NULL && paste->has_bg)
      {
        *actual_item = item; /* Group sets this to NULL */
        return 0.0;          /* We're on the background. */
      }
    else 
      return ret;
  }

  *actual_item = NULL;

  /* Point is outside */

  if (x < x1)
    dx = x1 - x;
  else if (x > x2)
    dx = x - x2;
  else
    dx = 0.0;

  if (y < y1)
    dy = y1 - y;
  else if (y > y2)
    dy = y - y2;
  else
    dy = 0.0;

  return sqrt (dx * dx + dy * dy);
}

/************ End Canvas Item Functions ******************/

void           
ez_item_set_toplevel (struct _EZItem* ezi, gboolean setting)
{
  EZPaste* paste; 

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));
  g_return_if_fail(ezi->paste != NULL);
  g_return_if_fail((setting && !ezi->toplevel) || 
                   (!setting && ezi->toplevel));

  paste = ezi->paste;

  if (setting)
    {
      g_return_if_fail(g_slist_find(paste->items, ezi) == NULL);

      if (!ezi->floating)
        {
          g_return_if_fail(ezi->parent != NULL);
          
          ez_container_remove(ezi->parent, ezi);
        }

      g_return_if_fail(ezi->floating);

      paste->items = g_slist_prepend(paste->items, ezi);

      gnome_canvas_item_show(GNOME_CANVAS_ITEM(ezi));
      /* Already in our group, since it was floating */

      ezi->floating = FALSE;
      ezi->toplevel = TRUE;

      ez_debug("EZItem %p is now toplevel", ezi);
    }
  else 
    {
      g_return_if_fail(g_slist_find(paste->items, ezi) != NULL);
      g_return_if_fail(!ezi->floating);

      paste->items = g_slist_remove(paste->items, ezi);

      gnome_canvas_item_hide(GNOME_CANVAS_ITEM(ezi));
      /* It's already in the paste group, since it was toplevel */
      
      ezi->floating = TRUE;
      ezi->toplevel = FALSE;

      ez_debug("EZItem %p is now floating", ezi);
    }

  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(paste));
}

void       
ez_paste_pass_event(EZPaste* paste, 
                    struct _EZItem* origin, 
                    struct _EZItem* toplevel,
                    GdkEvent* event)
{
  g_return_if_fail(paste != NULL);
  g_return_if_fail(EZ_IS_PASTE(paste));
  g_return_if_fail((origin == NULL) || EZ_IS_ITEM(origin));
  g_return_if_fail((toplevel == NULL) || toplevel->toplevel);
  g_return_if_fail((origin == NULL && toplevel == NULL) ||
                   (origin != NULL && toplevel != NULL));

  /* Current editing item got the event, give it back */
  if (origin != NULL && paste->editing_item == toplevel)
    {
      ez_item_pass_event(paste->editing_item, origin, event); 
    }
  else 
    {
      switch (event->type) {
        /* If origin/toplevel == NULL, it means the event started on the
           EZPaste */
      case GDK_ENTER_NOTIFY:
        ez_debug("Enter notify event passed to EZPaste %p", paste);
        break;
    
      case GDK_LEAVE_NOTIFY:
        ez_debug("Leave notify event passed to EZPaste %p", paste);
        break;
    
      case GDK_BUTTON_PRESS:
        {
          ez_debug("Button press event passed to EZPaste %p", paste);

          if (event->button.button == 1) 
            {
              /* Current editing item didn't get the event - stop 
                 editing it. */
              if (paste->editing_item != NULL)
                {
                  ez_item_stop_editing(paste->editing_item);
                  paste->editing_item = NULL;
                }
              /* There's no current editing item, and no item got the event */
              else if (toplevel == NULL)
                {
                  return;
                }
              /* There's no current editing item, but some item did get the
                 event */
              else 
                {
                  g_return_if_fail(paste->editing_item == NULL);
              
                  paste->editing_item = toplevel;
                  ez_item_start_editing(toplevel);
                  ez_item_start_moving(toplevel);
                }
            }

          return;
        }
        break; /* Button press */
    
      case GDK_BUTTON_RELEASE:
        ez_debug("Button release event passed to EZPaste %p", paste);
        break; /* Button release */
    
      case GDK_MOTION_NOTIFY:
        break; /* Motion notify */
    
      default:
        break;
      }
    }
  return;
}
