/* Handled block arrow for GnomeCanvas
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>

#include <libgnomeui/gnome-canvas-util.h> /* FIXME */
#include "gnome-canvas-handle-arrow.h"

#include <gtk/gtksignal.h>

#include <math.h>
#include <float.h>

static void gnome_canvas_handle_arrow_class_init (GnomeCanvasHandleArrowClass *class);
static void gnome_canvas_handle_arrow_init       (GnomeCanvasHandleArrow      *handle);
static void gnome_canvas_handle_arrow_destroy    (GtkObject            *object);
static void gnome_canvas_handle_arrow_set_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);
static void gnome_canvas_handle_arrow_get_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);

static void gnome_canvas_handle_arrow_create    (GnomeCanvasHandled* handled, GdkEventButton* event);
static void gnome_canvas_handle_arrow_set_snap_func(GnomeCanvasHandled* handled,
						    GnomeCanvasSnapFunc func,
						    gpointer data);

static void gnome_canvas_handle_arrow_size(GnomeCanvasHandled* handled,
					   double* x1, double* y1, 
					   double* x2, double* y2);

static void gnome_canvas_handle_arrow_scale(GnomeCanvasHandled* handled,
					    double x1, double y1,
					    double x2, double y2);

static void gnome_canvas_handle_arrow_realize (GnomeCanvasItem *item);

static void gnome_canvas_handle_arrow_unrealize   (GnomeCanvasItem *item);

static void gnome_canvas_handle_arrow_reconfigure (GnomeCanvasItem *item);

static void gnome_canvas_handle_arrow_handle_moved(GnomeCanvasItem* item, gdouble x, gdouble y,
						 gpointer data);

static gint gnome_canvas_handle_arrow_poly_event(GnomeCanvasItem* item, GdkEvent* event, 
						 GnomeCanvasHandleArrow* handle_arrow);


static void gtk_marshal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE (GtkObject * object,
							   GtkSignalFunc func,
							   gpointer func_data,
							   GtkArg * args);

/* "left" and "right" are kind of arbitrary; depends on arrow
 rotation.  Just think of them as "one" and "the other" */
typedef enum {
  BACK_LEFT,
  BACK_RIGHT,
  SHAFT_LEFT,
  SHAFT_RIGHT,
  TRAILING_LEFT,
  TRAILING_RIGHT,
  BASE,
  POINT,
  LAST_ARROW_HANDLE
} ArrowHandle;

enum {
  ARG_0,
  ARG_X1,
  ARG_Y1,
  ARG_X2,
  ARG_Y2,
  ARG_ARROW_SHAPE_A,
  ARG_ARROW_SHAPE_B,
  ARG_ARROW_SHAPE_C  
};


enum {
  SHAPE_CHANGE,
  LAST_SIGNAL
};

static GnomeCanvasHandledClass *parent_class;

static gint handle_arrow_signals[LAST_SIGNAL] = { 0 };

GtkType
gnome_canvas_handle_arrow_get_type (void)
{
  static GtkType handle_arrow_type = 0;

  if (!handle_arrow_type) {
    GtkTypeInfo handle_arrow_info = {
      "GnomeCanvasHandleArrow",
      sizeof (GnomeCanvasHandleArrow),
      sizeof (GnomeCanvasHandleArrowClass),
      (GtkClassInitFunc) gnome_canvas_handle_arrow_class_init,
      (GtkObjectInitFunc) gnome_canvas_handle_arrow_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    handle_arrow_type = gtk_type_unique (gnome_canvas_handled_get_type (), &handle_arrow_info);
  }

  return handle_arrow_type;
}

static void
gnome_canvas_handle_arrow_class_init (GnomeCanvasHandleArrowClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  GnomeCanvasHandledClass *handled_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  handled_class = (GnomeCanvasHandledClass*) klass;

  parent_class = gtk_type_class (gnome_canvas_handled_get_type ());

  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::x1", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X1);
  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::y1", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y1);
  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::x2", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X2);
  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::y2", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y2);
  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::arrow_shape_a", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_ARROW_SHAPE_A);
  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::arrow_shape_b", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_ARROW_SHAPE_B);
  gtk_object_add_arg_type ("GnomeCanvasHandleArrow::arrow_shape_c", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_ARROW_SHAPE_C);

  handle_arrow_signals[SHAPE_CHANGE] =
    gtk_signal_new ("shape_change",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GnomeCanvasHandleArrowClass, shape_change),
		    gtk_marshal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE,
		    GTK_TYPE_NONE, 4, 
		    GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE, 
		    GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

  gtk_object_class_add_signals (object_class, handle_arrow_signals, 
				LAST_SIGNAL);

  klass->shape_change           = NULL;

  handled_class->create         = gnome_canvas_handle_arrow_create;
  handled_class->set_snap_func  = gnome_canvas_handle_arrow_set_snap_func;
  handled_class->size           = gnome_canvas_handle_arrow_size;
  handled_class->scale          = gnome_canvas_handle_arrow_scale;

  item_class->realize     = gnome_canvas_handle_arrow_realize;
  item_class->reconfigure = gnome_canvas_handle_arrow_reconfigure;
  item_class->unrealize   = gnome_canvas_handle_arrow_unrealize;

  object_class->destroy = gnome_canvas_handle_arrow_destroy;
  object_class->set_arg = gnome_canvas_handle_arrow_set_arg;
  object_class->get_arg = gnome_canvas_handle_arrow_get_arg;
}

static void
gnome_canvas_handle_arrow_init (GnomeCanvasHandleArrow *handle_arrow)
{
  handle_arrow->poly = NULL;
  handle_arrow->handles = NULL;

  handle_arrow->x1 = handle_arrow->y1 = 0.0; 

  handle_arrow->x2 = 
    handle_arrow->y2 = 10.0; 

  handle_arrow->a = 15.0;
  handle_arrow->b = 20.0;
  handle_arrow->c = 15.0;
  handle_arrow->width = 10.0;

  handle_arrow->stipple = NULL;
}

static void
create_handles(GnomeCanvasHandleArrow* handle_arrow)
{
  g_return_if_fail(GNOME_CANVAS_HANDLED_IS_SELECTED(GNOME_CANVAS_HANDLED(handle_arrow)));

  if (handle_arrow->handles == NULL) {
    GnomeCanvasHandle** newhandles = g_new(GnomeCanvasHandle*,LAST_ARROW_HANDLE);
    ArrowHandle i = BACK_LEFT;

    while ( i < LAST_ARROW_HANDLE ) {
      newhandles[i] = GNOME_CANVAS_HANDLE(gnome_canvas_item_new(GNOME_CANVAS_GROUP(handle_arrow),
								gnome_canvas_handle_get_type(),
								"anchor", GTK_ANCHOR_CENTER,
								NULL));

      gtk_object_set_user_data(GTK_OBJECT(newhandles[i]), handle_arrow);

      gtk_signal_connect(GTK_OBJECT(newhandles[i]), "moved",
			 GTK_SIGNAL_FUNC(gnome_canvas_handle_arrow_handle_moved),
			 GINT_TO_POINTER(i));
      ++i;
    }

    handle_arrow->handles = newhandles;
  }
}

static void
destroy_handles(GnomeCanvasHandleArrow* handle_arrow)
{
  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));
  g_return_if_fail(!GNOME_CANVAS_HANDLED_IS_SELECTED(GNOME_CANVAS_HANDLED(handle_arrow)));

  if (handle_arrow->handles) {
    ArrowHandle i = BACK_LEFT;
    while ( i < LAST_ARROW_HANDLE ) {
      gtk_object_destroy(GTK_OBJECT(handle_arrow->handles[i]));
      ++i;
    }
    g_free(handle_arrow->handles);
    handle_arrow->handles = NULL;
  }
}

static void
show_handles(GnomeCanvasHandleArrow* handle_arrow)
{
  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));
  g_return_if_fail(GNOME_CANVAS_HANDLED_IS_SELECTED(GNOME_CANVAS_HANDLED(handle_arrow)));
  g_return_if_fail(handle_arrow->handles);

  if (handle_arrow->handles) {
    ArrowHandle i = BACK_LEFT;
    while ( i < LAST_ARROW_HANDLE ) {
      gnome_canvas_item_show(GNOME_CANVAS_ITEM(handle_arrow->handles[i]));
      ++i;
    }
  }
}

static void
hide_handles(GnomeCanvasHandleArrow* handle_arrow)
{
  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));
  g_return_if_fail(GNOME_CANVAS_HANDLED_IS_SELECTED(GNOME_CANVAS_HANDLED(handle_arrow)));
  g_return_if_fail(handle_arrow->handles);

  if (handle_arrow->handles) {
    ArrowHandle i = BACK_LEFT;
    while ( i < LAST_ARROW_HANDLE ) {
      gnome_canvas_item_hide(GNOME_CANVAS_ITEM(handle_arrow->handles[i]));
      ++i;
    }
  }
}

/* What goes in 'unrealize' and what in 'destroy'? I think either or 
   both can be called. So both are OK with this object. */
static void
gnome_canvas_handle_arrow_destroy (GtkObject *object)
{
  GnomeCanvasHandleArrow *handle_arrow;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_HANDLE_ARROW (object));

  handle_arrow = GNOME_CANVAS_HANDLE_ARROW (object);

  /* Group frees it. */
  handle_arrow->poly = NULL;

  /* Want to emit unselect if appropriate */
  if (GNOME_CANVAS_HANDLED_IS_SELECTED(GNOME_CANVAS_HANDLED(object))) 
    gnome_canvas_handled_set_selected(GNOME_CANVAS_HANDLED(object), FALSE);

  /* Group destroy method will destroy the handles and item */
  g_free(handle_arrow->handles);
  handle_arrow->handles = NULL;

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_handle_arrow_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasHandleArrow *handle_arrow;

  item = GNOME_CANVAS_ITEM (object);
  handle_arrow = GNOME_CANVAS_HANDLE_ARROW (object);

  switch (arg_id) {
  case ARG_X1:
    handle_arrow->x1 = GTK_VALUE_DOUBLE (*arg);
    break;
  case ARG_Y1:
    handle_arrow->y1 = GTK_VALUE_DOUBLE (*arg);
    break;
  case ARG_X2:
    handle_arrow->x2 = GTK_VALUE_DOUBLE (*arg);
    break;
  case ARG_Y2:
    handle_arrow->y2 = GTK_VALUE_DOUBLE (*arg);
    break;
    
  case ARG_ARROW_SHAPE_A:
    handle_arrow->a = fabs (GTK_VALUE_DOUBLE (*arg));
    break;
    
  case ARG_ARROW_SHAPE_B:
    handle_arrow->b = fabs (GTK_VALUE_DOUBLE (*arg));
    break;
    
  case ARG_ARROW_SHAPE_C:
    handle_arrow->c = fabs (GTK_VALUE_DOUBLE (*arg));
    break;

  default:
    g_warning("GnomeCanvasHandleArrow got an unknown arg type.");
    break;
  }

  gnome_canvas_handle_arrow_reconfigure(item);
}

static void
gnome_canvas_handle_arrow_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasHandleArrow *handle_arrow;

  handle_arrow = GNOME_CANVAS_HANDLE_ARROW (object);

  switch (arg_id) {
  case ARG_X1:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->x1;
    break;
  case ARG_Y1:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->y1;
    break;
  case ARG_X2:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->x2;
    break;
  case ARG_Y2:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->y2;
    break;

  case ARG_ARROW_SHAPE_A:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->a;
    break;
    
  case ARG_ARROW_SHAPE_B:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->b;
    break;
    
  case ARG_ARROW_SHAPE_C:
    GTK_VALUE_DOUBLE (*arg) = handle_arrow->c;
    break;

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}
 
#define gray50_width 2
#define gray50_height 2
static char gray50_bits[] = {
  0x02, 0x01, };

static void
gnome_canvas_handle_arrow_realize (GnomeCanvasItem *item)
{
  GnomeCanvasHandleArrow* handle_arrow = GNOME_CANVAS_HANDLE_ARROW(item);

  g_return_if_fail(handle_arrow->poly == NULL);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->realize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->realize) (item);

  if (handle_arrow->stipple == NULL) {
    handle_arrow->stipple =  
      gdk_bitmap_create_from_data (NULL, gray50_bits, 
				   gray50_width, gray50_height);
  }

  /* This is a horrible hack - it should be in gnome_canvas_handle_arrow_new(GnomeCanvasGroup*),
     which does not exist and would be all weird in the canvas context. 
     However, people might want to change the arrow characteristics at creation time. 
     Which is currently impossible. */

  handle_arrow->poly = 
    GNOME_CANVAS_POLYGON(gnome_canvas_item_new(GNOME_CANVAS_GROUP(handle_arrow),
					       gnome_canvas_polygon_get_type(),
					       "fill_color", "light green",
					       "outline_color", "purple",
					       "width_units", 3.0,
					       NULL));

  gtk_signal_connect(GTK_OBJECT(handle_arrow->poly), "event",
		     GTK_SIGNAL_FUNC(gnome_canvas_handle_arrow_poly_event),
		     handle_arrow);

  gnome_canvas_handle_arrow_reconfigure(item);
}


static void 
gnome_canvas_handle_arrow_unrealize   (GnomeCanvasItem *item)
{
  GnomeCanvasHandleArrow *handle_arrow;

  g_return_if_fail (item != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_HANDLE_ARROW (item));

  handle_arrow = GNOME_CANVAS_HANDLE_ARROW (item);

  /* Want to emit unselect if appropriate */
  if (GNOME_CANVAS_HANDLED_IS_SELECTED(GNOME_CANVAS_HANDLED(item))) {
    gnome_canvas_handled_set_selected(GNOME_CANVAS_HANDLED(item), FALSE);
  }

  /* Group unrealize method will unrealize item, handles 
     went away in above call. */

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize) (item);
}

/* Points should have a 14-element double[] already in it. */
static void
compute_coords(GnomeCanvasHandleArrow* handle_arrow,
	       GnomeCanvasPoints* points)
{
  /* We are going to have a huge environment, 
     but save calculations. maybe it will be faster, 
     i dunno, but it's easier to read. */

  /* Copies of the struct contents, out of pure laziness (I guess it
     makes things easier to read, too) */
  double x1, y1, x2, y2, w, a, b, c; 
  /* dx, dy are the x and y deltas from x1,y1 to x2, y2 */
  double dx, dy;
  /* A is the normalized vector from x1, y1, to x2, y2;
     AP is the perpendicular vector. */
  double Ax, Ay, APx, APy;
  /* L is the distance between x1,y1 and x2, y2 */
  double L;
  /* Half the base width times AP */
  double half_base_x, half_base_y;
  /* Shaft side length (L - a)*A*/
  double shaft_side_x, shaft_side_y;
  /* Distance from base to trailing point (L - b)*A */
  double shaft_to_point_x, shaft_to_point_y;
  /* Distance from center line to trailing point */
  double center_to_point_x, center_to_point_y;

  /* Convenience. */
  double* coords;

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));
  g_return_if_fail(points != 0);

  do {
    coords = points->coords;
    
    x1 = handle_arrow->x1;
    y1 = handle_arrow->y1;
    x2 = handle_arrow->x2;
    y2 = handle_arrow->y2;
    w  = handle_arrow->width;
    a  = handle_arrow->a;
    b  = handle_arrow->b;
    c  = handle_arrow->c;
    
    dx = x2 - x1;
    dy = y2 - y1;
    
    L = sqrt(dx*dx + dy*dy);
    
    points->num_points = 7;

    /* If L == 0.0, then we can't really draw the arrow in any sensible
       way, since we'd get FPE */
    if (L == 0.0) {
      handle_arrow->x2 += handle_arrow->a + .05;
      handle_arrow->y2 += handle_arrow->a + .05;
    }
  } while (L == 0.0);

  /* Fill two components of vector A, normalizing */
  Ax = dx/L;
  Ay = dy/L;

  /* Fill the perpendicular */
  APx = - Ay;
  APy = Ax;

  half_base_x = (w/2 * APx);
  half_base_y = (w/2 * APy);

  /* The two base corners are the origin (x1,y1) plus or minus 
     half the base width * AP, since the base is a segment of AP */
  
  coords[0] = x1 - half_base_x;
  coords[1] = y1 - half_base_y;

  coords[12] = x1 + half_base_x;
  coords[13] = y1 + half_base_y;

  /* The next corners are the origin plus (L minus a * vector A), 
     plus or minus half the base width times AP */

  shaft_side_x = (L - a) * Ax;
  shaft_side_y = (L - a) * Ay;

  coords[2] = x1 + shaft_side_x - half_base_x;
  coords[3] = y1 + shaft_side_y - half_base_y;
  
  coords[10] = x1 + shaft_side_x + half_base_x;
  coords[11] = y1 + shaft_side_y + half_base_y;
    
  /* Trailing arrow points are similar */
  
  shaft_to_point_x = (L - b) * Ax;
  shaft_to_point_y = (L - b) * Ay;

  center_to_point_x = (w/2 + c) * APx;
  center_to_point_y = (w/2 + c) * APy;
  
  coords[4] = x1 + shaft_to_point_x - center_to_point_x;
  coords[5] = y1 + shaft_to_point_y - center_to_point_y;

  coords[8] = x1 + shaft_to_point_x + center_to_point_x;
  coords[9] = y1 + shaft_to_point_y + center_to_point_y;

  /* And the point itself is known from the start. */
  coords[6] = x2;
  coords[7] = y2;
}

static void
sync_poly(GnomeCanvasHandleArrow* handle_arrow, GnomeCanvasPoints* points)
{
  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));
  g_assert(handle_arrow->poly != NULL);

  if (points->num_points < 7) {
    return;
  }

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(handle_arrow->poly),
			"points", points,
			NULL);
}

static void 
sync_handles(GnomeCanvasHandleArrow* handle_arrow, GnomeCanvasPoints* points)
{
  GnomeCanvasHandled* handled = GNOME_CANVAS_HANDLED(handle_arrow);
  GnomeCanvasHandle** h = handle_arrow->handles; /* Save typing; lazy. */
  GtkAnchorType* anchors;
  gboolean x_reversed;
  gboolean y_reversed;
  double* coords;

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));
  g_return_if_fail(points != NULL);
  g_assert(handle_arrow->poly != NULL);
  g_assert(h != NULL);

  coords = points->coords;

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[BASE]),
			"x", handle_arrow->x1,
			"y", handle_arrow->y1,
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);


  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[POINT]),
			"x", handle_arrow->x2,
			"y", handle_arrow->y2,
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);


  /* If we don't know where the handles go, stick them 
     somewhere random. */
  if (points->num_points < 7) {
    int i = 0;
    while (i < 14) {
      if (i % 2 == 0)
	coords[i] = handle_arrow->x1 - 5.0;
      else 
	coords[i] = handle_arrow->y1 - 5.0;
      ++i;
    }
  }

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[BACK_LEFT]),
			"x", coords[0],
			"y", coords[1],
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[BACK_RIGHT]),
			"x", coords[12],
			"y", coords[13],
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[SHAFT_LEFT]),
			"x", coords[2],
			"y", coords[3],
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);


  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[SHAFT_RIGHT]),
			"x", coords[10],
			"y", coords[11],
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[TRAILING_LEFT]),
			"x", coords[4],
			"y", coords[5],
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);


  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[TRAILING_RIGHT]),
			"x", coords[8],
			"y", coords[9],
			"sensitive", 
			GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled),
			NULL);
}

static void 
gnome_canvas_handle_arrow_reconfigure (GnomeCanvasItem *item)
{
  GnomeCanvasHandleArrow* handle_arrow = GNOME_CANVAS_HANDLE_ARROW(item);
  GnomeCanvasHandled* handled =        GNOME_CANVAS_HANDLED(item);
  GnomeCanvasPoints points;
  double coords[14];

  points.coords = coords;
  points.num_points = 0;

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure) (item);
  
  if (handle_arrow->poly == NULL) return; /* Not realized yet. */

  compute_coords(handle_arrow, &points);

  if (GNOME_CANVAS_HANDLED_IS_SELECTED(handled)) {

    create_handles(handle_arrow); /* checks for handles == NULL */
    
    sync_handles(handle_arrow, &points);
  }
  /* Not selected/sensitive */
  else {
    destroy_handles(handle_arrow); /* checks for NULL */
  }

  sync_poly(handle_arrow, &points);
}

static gint 
gnome_canvas_handle_arrow_poly_event(GnomeCanvasItem* item, GdkEvent* event, 
				     GnomeCanvasHandleArrow* handle_arrow)
{
  /*  GnomeCanvasArrow* arrow = GNOME_CANVAS_ARROW(item); */
  static double lastx = 0.0, lasty = 0.0;
  GdkCursor* ptr;
  GnomeCanvasHandled* handled = GNOME_CANVAS_HANDLED(handle_arrow);

  switch (event->type){

  case GDK_ENTER_NOTIFY:
    if (!GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled)) return FALSE;
    ptr = gdk_cursor_new(GDK_FLEUR);
    gdk_window_set_cursor(GTK_WIDGET(item->canvas)->window, ptr);
    gdk_cursor_destroy (ptr);
    break;
    
  case GDK_LEAVE_NOTIFY:
    /* Unset the cursor, even if insensitive 
       (in case we set it while sensitive) */
    gdk_window_set_cursor(GTK_WIDGET(item->canvas)->window, NULL);
    break;
    
  case GDK_BUTTON_PRESS:
    if (!GNOME_CANVAS_HANDLED_IS_SENSITIVE(handled)) return FALSE;
    switch (event->button.button) {
    case 1:
      if (!handled->dragging) {
	gnome_canvas_item_grab (item,
				GDK_POINTER_MOTION_MASK | 
				GDK_BUTTON_RELEASE_MASK | 
				GDK_BUTTON_PRESS_MASK,
				NULL, /* Set on enter */
				event->button.time);

	gnome_canvas_item_set(GNOME_CANVAS_ITEM(handle_arrow->poly),
			      "outline_stipple", handle_arrow->stipple,
			      "fill_stipple", handle_arrow->stipple,
			      NULL);

	lastx = event->button.x;
	lasty = event->button.y;
	gnome_canvas_item_w2i(item, &lastx, &lasty);

	if (handled->snap_func) 
	  (*handled->snap_func)(&lastx, &lasty, handled->snap_func_data);
	
	handled->dragging = TRUE;
	gnome_canvas_handled_set_selected(handled, TRUE); /* implies reconfigure */
	hide_handles(handle_arrow);
      } else return FALSE;
      break;
      
    case 2:
      if (!handled->sensitive) return FALSE;

      if (event->button.state & GDK_CONTROL_MASK) 
	gnome_canvas_item_lower(GNOME_CANVAS_ITEM(handle_arrow),1);
      else 
	gnome_canvas_item_raise(GNOME_CANVAS_ITEM(handle_arrow),1);

      gnome_canvas_handle_arrow_reconfigure(GNOME_CANVAS_ITEM(handle_arrow));
      break;
      
    default:
      return FALSE;
      break;
    }
    break; /* Button press */
    
  case GDK_BUTTON_RELEASE:
    switch (event->button.button) {
    case 1:
      if (handled->dragging) {
	gnome_canvas_item_ungrab(item, event->button.time);
	handled->dragging = FALSE;
	gnome_canvas_item_set(GNOME_CANVAS_ITEM(handle_arrow->poly),
			      "outline_stipple", NULL,
			      "fill_stipple", NULL,
			      NULL);
	show_handles(handle_arrow);
      } else return FALSE;
      break;

    default:
      return FALSE;
      break;
    }
    break; /* Button release */
    
  case GDK_MOTION_NOTIFY:
    /* Want to keep dragging even if insensitive */
    if (handled->dragging) {
      double newx, newy, dx, dy;

      newx = event->motion.x;
      newy = event->motion.y;

      gnome_canvas_item_w2i(item, &newx, &newy);

      if (handled->snap_func) 
	(*handled->snap_func)(&newx, &newy, handled->snap_func_data);

      dx = newx - lastx;
      dy = newy - lasty;
      lastx = newx;
      lasty = newy;

      handle_arrow->x1 += dx;
      handle_arrow->y1 += dy;
      handle_arrow->x2 += dx;
      handle_arrow->y2 += dy;
      gnome_canvas_handle_arrow_reconfigure(GNOME_CANVAS_ITEM(handle_arrow));
      gnome_canvas_handled_resized(handled,
				   handle_arrow->x1, 
				   handle_arrow->y1,
				   handle_arrow->x2,
				   handle_arrow->y2);
    } else return FALSE;
    break; /* Motion notify */
    
  default:
    return FALSE;
  }
  
  return TRUE;
}

static void 
gnome_canvas_handle_arrow_set_snap_func(GnomeCanvasHandled* handled,
				       GnomeCanvasSnapFunc func,
				       gpointer data)
{
  handled->snap_func = func;
  handled->snap_func_data = data;
}

static void 
gnome_canvas_handle_arrow_create    (GnomeCanvasHandled* handled, GdkEventButton* event)
{
  GnomeCanvasHandleArrow* handle_arrow = GNOME_CANVAS_HANDLE_ARROW(handled);  
  double x1, y1, x2, y2;

  handled->selected = TRUE;

  if (event == NULL) {
    /* Make up some defaults */
    gnome_canvas_get_scroll_region(GNOME_CANVAS_ITEM(handled)->canvas,
				   &x1, &y1, &x2, &y2);
    gnome_canvas_item_w2i(GNOME_CANVAS_ITEM(handled),
			  &x1, &y1);
    gnome_canvas_item_w2i(GNOME_CANVAS_ITEM(handled),
			  &x2, &y2);
    
    handle_arrow->x1 = x1;
    handle_arrow->y1 = y1;
    handle_arrow->x2 = x1+(x2-x1)/2;
    handle_arrow->y2 = y1+(y2-y1)/2;

    if (handled->snap_func) {
      (*handled->snap_func)(&handle_arrow->x1, 
			    &handle_arrow->y1, 
			    handled->snap_func_data);

      (*handled->snap_func)(&handle_arrow->x2, 
			    &handle_arrow->y2, 
			    handled->snap_func_data);
    }

    gnome_canvas_handle_arrow_reconfigure(GNOME_CANVAS_ITEM(handled));
  }
  else {
    x1 = event->x;
    y1 = event->y;
    gnome_canvas_item_w2i(GNOME_CANVAS_ITEM(handled), &x1, &y1);

    if (handled->snap_func)
      (*handled->snap_func)(&x1, &y1, handled->snap_func_data);

    handle_arrow->x1 = handle_arrow->x2 = x1;
    handle_arrow->y1 = handle_arrow->y2 = y1;

    gnome_canvas_handle_arrow_reconfigure(GNOME_CANVAS_ITEM(handled));

    gnome_canvas_handle_transfer_drag(NULL,handle_arrow->handles[POINT],event->time);
  }
}


static void 
gnome_canvas_handle_arrow_handle_moved(GnomeCanvasItem* item, gdouble x, gdouble y,
				       gpointer data)
{
  ArrowHandle whichone = GPOINTER_TO_INT(data);
  GnomeCanvasHandleArrow* handle_arrow;
  GnomeCanvasHandled* handled;
  /* Copies of the struct contents, out of pure laziness (I guess it
     makes things easier to read, too) */
  double x1, y1, x2, y2, w, a, b, c; 
  /* dx, dy are the x and y deltas from x1,y1 to x2, y2 */
  double dx, dy;
  /* A is the normalized vector from x1, y1, to x2, y2;
     AP is the perpendicular vector. */
  double Ax, Ay, APx, APy;
  /* L is the distance between x1,y1 and x2, y2 */
  double L;
  /* dl is the amount to add to L */
  double dl;
  /* Change in width */
  double dw;

  double tmp;

  GnomeCanvasPoints points;
  double coords[14];

  gboolean resized = FALSE; /* Set to true if we change x1,y1 or x2,y2 */
  gboolean shape_changed = FALSE; /* Set if we change width, a, b, or c */

  points.coords = coords;
  points.num_points = 0;

  handle_arrow = gtk_object_get_user_data(GTK_OBJECT(item));
  g_assert(handle_arrow != NULL);
  handled = GNOME_CANVAS_HANDLED(handle_arrow);

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handle_arrow));

  /* Convert from item coordinates of the handle to item coordinates
     of the HandleArrow. */
  gnome_canvas_item_i2w(item, &x, &y);
  gnome_canvas_item_w2i(GNOME_CANVAS_ITEM(handle_arrow), &x, &y);
  
  switch (whichone) {
    /* These are the only ones that can sensibly be snapped. */
  case POINT:
  case BASE:
    if (handled->snap_func) {
      (*handled->snap_func)(&x, &y, handled->snap_func_data);
    }
    break;
  default:
    break;
  }

  x1 = handle_arrow->x1;
  y1 = handle_arrow->y1;
  x2 = handle_arrow->x2;
  y2 = handle_arrow->y2;
  w  = handle_arrow->width;
  a  = handle_arrow->a;
  b  = handle_arrow->b;
  c  = handle_arrow->c;
  
  dx = x2 - x1;
  dy = y2 - y1;
  
  L = sqrt(dx*dx + dy*dy);

  /* Normally you don't do an exact compare for floats,
     but we only want to know if it's going to give FPE */
  if (L != 0.0) {
    /* Fill two components of vector A, normalizing */
    Ax = dx/L;
    Ay = dy/L;
  }    
  else {
    /* Set them to nonsense */
    Ax = Ay = 0.0;
    switch (whichone) {
      /* Point and base don't depend on vector computations. */
    case POINT:
    case BASE:
      break;
      /* Everything else does, so just sync and bail */
    default:
      compute_coords(handle_arrow, &points);
      
      sync_poly(handle_arrow, &points);
      sync_handles(handle_arrow, &points);
      return;
    }
  }

  /* Fill the perpendicular */
  APx = - Ay;
  APy = Ax;

  /* change the meaning of dx and dy here */
  dx = x - x1;
  dy = y - y1;

  /* Amount to change the length of the arrow */
  dl = dx*Ax + dy*Ay;

  /* Width setting switch */
  switch (whichone) {
  case BACK_LEFT:
  case BACK_RIGHT:
  case SHAFT_LEFT:
  case SHAFT_RIGHT:
    handle_arrow->width = 2*sqrt(dx*dx + dy*dy - dl*dl);
    shape_changed = TRUE;
    break;

  case TRAILING_LEFT:
  case TRAILING_RIGHT:
    handle_arrow->c = MAX(sqrt(dx*dx + dy*dy - dl*dl) - w, 0);
    shape_changed = TRUE;
    break;    
    
  default:
    break;
  }

  /* Set other stuff */
  switch (whichone) {
  case BACK_LEFT:
  case BACK_RIGHT:
    handle_arrow->x1 += dl * Ax;
    handle_arrow->y1 += dl * Ay;
    resized = TRUE;
    break;

  case SHAFT_LEFT:
  case SHAFT_RIGHT:
    /* a must be between 0 and b, so increase b if needed. */
    handle_arrow->a = MAX(L - dl, 0);
    if (handle_arrow->a > b) handle_arrow->b = handle_arrow->a;
    shape_changed = TRUE;
    break;

  case TRAILING_LEFT:
  case TRAILING_RIGHT:
    /* b must be between at least a and at least 0 */
    handle_arrow->b = MAX(L - dl, 0);
    if (handle_arrow->b < a) handle_arrow->a = handle_arrow->b;
    shape_changed = TRUE;
    break;

    /* In cases BASE and POINT, Ax/Ay and dl are NOT valid */
  case BASE:    
    dx = handle_arrow->x2 - x;
    dy = handle_arrow->y2 - y;
    L = sqrt(dx*dx + dy*dy);
    if (L > fabs(a)) {
      handle_arrow->x1 = x;
      handle_arrow->y1 = y;
      resized = TRUE;
    }
    break;

    /* In cases BASE and POINT, Ax/Ay and dl are NOT valid */
  case POINT:
    dx = x - handle_arrow->x1;
    dy = y - handle_arrow->y1;
    L = sqrt(dx*dx + dy*dy);
    if (L > fabs(a)) {
      handle_arrow->x2 = x;
      handle_arrow->y2 = y;
      resized = TRUE;
    }
    break;

  default:
    g_assert_not_reached();
    break;
  }

  compute_coords(handle_arrow, &points);

  sync_poly(handle_arrow, &points);
  sync_handles(handle_arrow, &points);

  if (resized) {
    gnome_canvas_handled_resized(handled,  
				 handle_arrow->x1, 
				 handle_arrow->y1,
				 handle_arrow->x2,
				 handle_arrow->y2);
  }

  if (shape_changed) {
    gtk_signal_emit(GTK_OBJECT(handle_arrow),
		    handle_arrow_signals[SHAPE_CHANGE],
		    handle_arrow->width,
		    handle_arrow->a, 
		    handle_arrow->b,
		    handle_arrow->c);

  }
}

static void 
gnome_canvas_handle_arrow_size(GnomeCanvasHandled* handled,
			       double* x1, double* y1, 
			       double* x2, double* y2)
{
  GnomeCanvasHandleArrow* handle_arrow;

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handled));

  handle_arrow = GNOME_CANVAS_HANDLE_ARROW(handled);
  
  if (x1) *x1 = MIN(handle_arrow->x1, handle_arrow->x2);
  if (x2) *x2 = MAX(handle_arrow->x1, handle_arrow->x2);
  if (y1) *y1 = MIN(handle_arrow->y1, handle_arrow->y2);
  if (y2) *y2 = MAX(handle_arrow->y1, handle_arrow->y2);
}

static void 
gnome_canvas_handle_arrow_scale(GnomeCanvasHandled* handled,
				double x1, double y1,
				double x2, double y2)
{
  GnomeCanvasHandleArrow* handle_arrow;

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ARROW(handled));

  handle_arrow = GNOME_CANVAS_HANDLE_ARROW(handled);

  handle_arrow->x1 = x1;
  handle_arrow->x2 = x2;
  handle_arrow->y1 = y1;
  handle_arrow->y2 = y2;

  gnome_canvas_handle_arrow_reconfigure(GNOME_CANVAS_ITEM(handle_arrow));
}


typedef void (*GtkSignal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE) (GtkObject * object,
							     gdouble arg1,
							     gdouble arg2,
							     gdouble arg3,
							     gdouble arg4,
							     gpointer user_data);


static void gtk_marshal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE (GtkObject * object,
							   GtkSignalFunc func,
							   gpointer func_data,
							   GtkArg * args)
{
  GtkSignal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE rfunc;
  rfunc = (GtkSignal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE) func;
  (*rfunc) (object,
	    GTK_VALUE_DOUBLE (args[0]),
	    GTK_VALUE_DOUBLE (args[1]),
	    GTK_VALUE_DOUBLE (args[2]),
	    GTK_VALUE_DOUBLE (args[3]),
	    func_data);
}
