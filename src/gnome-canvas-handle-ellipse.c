/* Handle ellipse type for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>

#include <libgnomeui/gnome-canvas-util.h> /* FIXME */

#include "gnome-canvas-handle-ellipse.h"

#include <math.h>
#include <time.h>

#include <gtk/gtksignal.h>

static void gnome_canvas_handle_ellipse_class_init (GnomeCanvasHandleEllipseClass *class);
static void gnome_canvas_handle_ellipse_init       (GnomeCanvasHandleEllipse      *handle);
static void gnome_canvas_handle_ellipse_destroy    (GtkObject            *object);
static void gnome_canvas_handle_ellipse_set_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);
static void gnome_canvas_handle_ellipse_get_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);

static void gnome_canvas_handle_ellipse_sync_item  (GnomeCanvasHandleBox* box,
						    GtkAnchorType which_handle);
static void gnome_canvas_handle_ellipse_create_item  (GnomeCanvasHandleBox* box);

enum {
  LAST_SIGNAL
};


static GnomeCanvasHandledClass *parent_class;

/* static gint handle_ellipse_signals[LAST_SIGNAL] = { 0 };*/

GtkType
gnome_canvas_handle_ellipse_get_type (void)
{
  static GtkType handle_ellipse_type = 0;

  if (!handle_ellipse_type) {
    GtkTypeInfo handle_ellipse_info = {
      "GnomeCanvasHandleEllipse",
      sizeof (GnomeCanvasHandleEllipse),
      sizeof (GnomeCanvasHandleEllipseClass),
      (GtkClassInitFunc) gnome_canvas_handle_ellipse_class_init,
      (GtkObjectInitFunc) gnome_canvas_handle_ellipse_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    handle_ellipse_type = gtk_type_unique (gnome_canvas_handle_box_get_type (), 
					&handle_ellipse_info);
  }

  return handle_ellipse_type;
}

static void
gnome_canvas_handle_ellipse_class_init (GnomeCanvasHandleEllipseClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  GnomeCanvasHandleBoxClass *handle_box_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  handle_box_class = (GnomeCanvasHandleBoxClass*) klass;

  parent_class = gtk_type_class (gnome_canvas_handle_box_get_type ());

  handle_box_class->create_item = gnome_canvas_handle_ellipse_create_item;
  handle_box_class->sync_item   = gnome_canvas_handle_ellipse_sync_item;

  object_class->destroy = gnome_canvas_handle_ellipse_destroy;
  object_class->set_arg = gnome_canvas_handle_ellipse_set_arg;
  object_class->get_arg = gnome_canvas_handle_ellipse_get_arg;
}

static void
gnome_canvas_handle_ellipse_init (GnomeCanvasHandleEllipse *handle_ellipse)
{
  handle_ellipse->stipple = NULL;
}

static void
gnome_canvas_handle_ellipse_destroy (GtkObject *object)
{
  GnomeCanvasHandleEllipse *handle_ellipse;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_HANDLE_ELLIPSE (object));

  handle_ellipse = GNOME_CANVAS_HANDLE_ELLIPSE (object);

  if (handle_ellipse->stipple) gdk_bitmap_unref(handle_ellipse->stipple);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_handle_ellipse_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasHandleEllipse *handle_ellipse;

  item = GNOME_CANVAS_ITEM (object);
  handle_ellipse = GNOME_CANVAS_HANDLE_ELLIPSE (object);

  switch (arg_id) {

  default:
    g_warning("GnomeCanvasHandleEllipse got an unknown arg type.");
    break;
  }
}

static void
gnome_canvas_handle_ellipse_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasHandleEllipse *handle_ellipse;

  handle_ellipse = GNOME_CANVAS_HANDLE_ELLIPSE (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void 
gnome_canvas_handle_ellipse_sync_item  (GnomeCanvasHandleBox* box, 
					GtkAnchorType which_handle)
{
  GnomeCanvasHandleEllipse* handle_ellipse = GNOME_CANVAS_HANDLE_ELLIPSE (box);
  double x1, y1, x2, y2;

  x1 = MIN(box->coords[0], box->coords[2]);
  x2 = MAX(box->coords[0], box->coords[2]);
  y1 = MIN(box->coords[1], box->coords[3]);
  y2 = MAX(box->coords[1], box->coords[3]);
  
  gnome_canvas_item_set(box->item,
			"x1", x1,
			"y1", y1,
			"x2", x2,
			"y2", y2,
			"outline_stipple",
			GNOME_CANVAS_HANDLED(box)->dragging ? 
			handle_ellipse->stipple : NULL,
			"fill_stipple",
			GNOME_CANVAS_HANDLED(box)->dragging ? 
			handle_ellipse->stipple : NULL,
			 NULL);  
}


    
#define gray50_width 2
#define gray50_height 2
static char gray50_bits[] = {
  0x02, 0x01, };

static void 
gnome_canvas_handle_ellipse_create_item  (GnomeCanvasHandleBox* box)
{
  GnomeCanvasHandleEllipse* handle_ellipse;

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_ELLIPSE(box));

  handle_ellipse = GNOME_CANVAS_HANDLE_ELLIPSE(box);
  
  if (handle_ellipse->stipple == NULL) {
    handle_ellipse->stipple =  
      gdk_bitmap_create_from_data (NULL, gray50_bits, 
				   gray50_width, gray50_height);
  }


  box->item = 
    gnome_canvas_item_new(GNOME_CANVAS_GROUP(box),
			  gnome_canvas_ellipse_get_type(), 
			  "outline_color", "green",
			  "width_units", 5.0,
			  NULL);
}


