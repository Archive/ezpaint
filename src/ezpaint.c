/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   ezpaint: cheesy paint program.
 *
 *   Copyright (C) 1998 Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezhandle.h"
#include "ezitem.h"
#include "ezpaste.h"
#include "ezcontainer.h"
#include "ezrect.h"

#include <config.h>
#include <gnome.h>


#define APPNAME "ezpaint"

/* GNU gettext doesn't find the text to translate if it is on a define */

char COPYRIGHT_NOTICE[]=N_("Copyright 1998 Havoc Pennington, GNU General Public License.");

typedef enum {
  EZ_LINE,
  EZ_HRULE,
  EZ_VRULE,
  EZ_RECT,
  EZ_ELLIPSE,
  EZ_ARROW,
  EZ_RUBBERBAND,
  EZ_NONE
} EZTool;

/* Yikes, this dumb demo program is rapidly getting enough features to
   warrant good software design - I should put this object in another
   file and have official functions to access it! */

struct _EZApp {
  GnomeApp    * app;    /* GnomeApp widget  */
  GnomeCanvas * canvas; /* GnomeCanvas widget */
  GnomeAppBar * appbar; /* appbar in GnomeApp, for convenience */
  EZTool tool;
  GList       * items;
  GList       * selection;
  EZPaste     * paste;
  gboolean    snapping;
  gboolean    sensitive;
  GtkToggleButton* snap_button;
  GtkToggleButton* sensitive_button;
  gdouble snap_force;
};

typedef struct _EZApp EZApp;

static void ezpaint_new_ezapp(void);

void ezapp_set_snapping(EZApp* ez, gboolean snapping);
void ezapp_add_item    (EZApp* ez, EZItem* item);

/* File menu */
static void new_app_cb(GtkWidget * w, gpointer data);
static void open_cb(GtkWidget * w, gpointer data);
static void close_cb(GtkWidget * w, gpointer data);
static void save_as_cb(GtkWidget * w, gpointer data);
static void exit_cb(GtkWidget * w, gpointer data);

/* Help menu */
static void about_cb(GtkWidget * w, gpointer data);

/* Tools */
static void line_tool_cb(GtkWidget * w, gpointer data);
static void hrule_tool_cb(GtkWidget * w, gpointer data);
static void vrule_tool_cb(GtkWidget * w, gpointer data);
static void rect_tool_cb(GtkWidget * w, gpointer data);
static void ellipse_tool_cb(GtkWidget * w, gpointer data);
static void arrow_tool_cb(GtkWidget * w, gpointer data);
static void rubberband_tool_cb(GtkWidget * w, gpointer data);
static void none_tool_cb(GtkWidget * w, gpointer data);

static void snapping_cb(GtkWidget * w, gpointer data);
static void sensitive_cb(GtkWidget * w, gpointer data);

static void delete_selection_cb(GtkWidget * w, gpointer data);
static void group_selection_cb(GtkWidget * w, gpointer data);

static void unimplemented_cb(GtkWidget * w, gpointer data);

static void new_line_cb(GtkWidget * w, gpointer data);
static void new_hrule_cb(GtkWidget * w, gpointer data);
static void new_vrule_cb(GtkWidget * w, gpointer data);
static void new_rect_cb(GtkWidget * w, gpointer data);
static void new_ellipse_cb(GtkWidget * w, gpointer data);
static void new_arrow_cb(GtkWidget * w, gpointer data);

static GdkImlibImage* gnome_canvas_snapshot_imlib(GnomeCanvas* canvas,
                                                  double x, 
                                                  double y, 
                                                  double width,
                                                  double height);

static GdkPixmap* gnome_canvas_snapshot_pixmap(GnomeCanvas* canvas,
                                               double x, 
                                               double y, 
                                               double width,
                                               double height);

/******************* 
		     Globals
*/

GList* apps;

int 
main(int argc, char** argv)
{
  apps = 0;

  /* Initialize the i18n stuff */
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  gnome_init (APPNAME, VERSION, argc, argv);

  ezpaint_new_ezapp();

  gtk_main();

  exit(EXIT_SUCCESS);
}


static GnomeUIInfo file_menu[] = {
  {GNOME_APP_UI_ITEM, 
   N_("_New Window"), N_("New window"), 
   new_app_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'n', 
   GDK_CONTROL_MASK, NULL },
  {GNOME_APP_UI_ITEM, 
   N_("_Open..."), N_("Open a new file in this window"), 
   open_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN, 'o', 
   GDK_CONTROL_MASK, NULL },
  {GNOME_APP_UI_ITEM, 
   N_("_Save As..."), N_("Save file to disk"), 
   save_as_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE, 's', 
   GDK_CONTROL_MASK, NULL },
  {GNOME_APP_UI_ITEM, N_("Close"), 
   N_("_Close this window"),
   close_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 'c', 
   GDK_CONTROL_MASK, NULL },
  GNOMEUIINFO_SEPARATOR,
  {GNOME_APP_UI_ITEM, N_("Exit"), 
   N_("_Quit the application"),
   exit_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 'q', 
   GDK_CONTROL_MASK, NULL },
  GNOMEUIINFO_END
};


static GnomeUIInfo items_menu[] = {
  {GNOME_APP_UI_ITEM, 
   N_("New _Rectangle"), N_("Create a rectangle at a default location"), 
   new_rect_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'r', 
   GDK_CONTROL_MASK, NULL },

  {GNOME_APP_UI_ITEM, 
   N_("New _Ellipse"), N_("Create an ellipse at a default location"), 
   new_ellipse_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'e', 
   GDK_CONTROL_MASK, NULL },

  {GNOME_APP_UI_ITEM, 
   N_("New _Arrow"), N_("Create an arrow at a default location"), 
   new_arrow_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'a', 
   GDK_CONTROL_MASK, NULL },

  {GNOME_APP_UI_ITEM, 
   N_("New _Line"), N_("Create a line at a default location"), 
   new_line_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'l', 
   GDK_CONTROL_MASK, NULL },

  {GNOME_APP_UI_ITEM, 
   N_("New _HRule"), N_("Create a horizontal rule at a default location"), 
   new_hrule_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'h', 
   GDK_CONTROL_MASK, NULL },

  {GNOME_APP_UI_ITEM, 
   N_("New _VRule"), N_("Create a vertical rule at a default location"), 
   new_vrule_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 'v', 
   GDK_CONTROL_MASK, NULL },

  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
  GNOMEUIINFO_HELP(APPNAME),
  {GNOME_APP_UI_ITEM, N_("About..."), 
   N_("Tell about this application"), 
   about_cb, NULL, NULL,
   GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 0, 0, NULL },
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
  GNOMEUIINFO_SUBTREE (N_("File"), file_menu),
  GNOMEUIINFO_SUBTREE (N_("Items"), items_menu),
  GNOMEUIINFO_SUBTREE (N_("Help"), help_menu),
  GNOMEUIINFO_END
};

static GnomeUIInfo toolbar[] = {
#define SENSITIVE_TOGGLE_INDEX 0
  /* Snstv?=Toggle Sensitivity */
  GNOMEUIINFO_TOGGLEITEM (N_("Snstv?"), N_("Toggle sensitivity"), 
                          sensitive_cb, NULL),
  { GNOME_APP_UI_ITEM, NULL, N_("Line tool"),                 
    line_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_line_tool.png", 0, (GdkModifierType)0, NULL }, 
  { GNOME_APP_UI_ITEM, NULL, N_("Horizontal Rule tool"),      
     hrule_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
     "pixmaps/ez_hrule_tool.png", 0, (GdkModifierType)0, NULL }, 
  { GNOME_APP_UI_ITEM, NULL, N_("Vertical Rule tool"),      
    vrule_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_vrule_tool.png", 0, (GdkModifierType)0, NULL }, 
  { GNOME_APP_UI_ITEM, NULL, N_("Rectangle tool"),          
    rect_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_rect_tool.png", 0, (GdkModifierType)0, NULL }, 
  { GNOME_APP_UI_ITEM, NULL,  N_("Ellipse tool"),          
    ellipse_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_ellipse_tool.png", 0, (GdkModifierType)0, NULL }, 
  { GNOME_APP_UI_ITEM, NULL,  N_("Arrow tool"),          
    arrow_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_arrow_tool.png", 0, (GdkModifierType)0, NULL }, 
  { GNOME_APP_UI_ITEM, NULL,  N_("Selection tool"),          
    rubberband_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_selection_tool.png", 0, (GdkModifierType)0, NULL }, 
 { GNOME_APP_UI_ITEM, NULL,   N_("Just edit, no active tool"),         
    none_tool_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_edit_tool.png", 0, (GdkModifierType)0, NULL }, 

#define SNAP_TOGGLE_INDEX 9
 { GNOME_APP_UI_TOGGLEITEM, NULL,   N_("Toggle snapping"), 
    snapping_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_snap_to_grid.png", 0, (GdkModifierType)0, NULL }, 

  GNOMEUIINFO_ITEM_STOCK (NULL, 
                          N_("Delete selected items (not really cut; no clipboard)"), 
                         delete_selection_cb, GNOME_STOCK_PIXMAP_CUT),

  GNOMEUIINFO_ITEM_STOCK (NULL, 
                          N_("Group selected items (not really cut; no clipboard)"), 
                          group_selection_cb, GNOME_STOCK_PIXMAP_MULTIPLE),


  { GNOME_APP_UI_ITEM, NULL, N_("Arc tool (unimplemented)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_arc_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Text tool (unimplemented)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_text_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Polygon tool (unimplemented)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_polygon_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Multiline/Irregular Polygon tool (unimplemented)"),
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_multiline_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Image tool (unimplemented)"),
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_image_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Freehand tool (unimplemented)"),
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_freehand_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Arrowhead Line tool (need canvas item constructors for this to work, see TODO)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_linearrow_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Filled Rectangle tool (need canvas item constructors for this to work, see TODO)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_fillrect_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Filled Ellipse tool (need canvas item constructors for this to work, see TODO)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_fillellipse_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Filled Arrow tool (need canvas item constructors for this to work, see TODO)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_fillarrow_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Filled arc tool (unimplemented)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_fillarc_tool.png", 0, (GdkModifierType)0, NULL }, 

  { GNOME_APP_UI_ITEM, NULL, N_("Fill Polygon tool (unimplemented)"),      
    unimplemented_cb, NULL, NULL, GNOME_APP_PIXMAP_FILENAME,
    "pixmaps/ez_fillpolygon_tool.png", 0, (GdkModifierType)0, NULL }, 


  GNOMEUIINFO_END
};

static void
ezpaint_destroy_ezapp(EZApp* ez)
{
  apps = g_list_remove(apps, ez);
  gtk_widget_destroy(GTK_WIDGET(ez->app));
  ez->app = NULL;
  g_free(ez);
  if (apps == NULL) gtk_main_quit();
}

static gint 
ezapp_delete_event(GtkWidget* app, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  ezpaint_destroy_ezapp(ez);
  return TRUE;
}

static gint
page_clicked(GnomeCanvasItem* item, GdkEvent* event, EZApp* app)
{
  GdkEventButton* button_event;

  if (event->type != GDK_BUTTON_PRESS) return FALSE;

  button_event = (GdkEventButton*)event;

  if (button_event->button != 1) return FALSE;

  switch (app->tool) {
  case EZ_LINE:

    break;

  case EZ_HRULE:

    break;

  case EZ_VRULE:

    break;

  case EZ_RECT:

    break;

  case EZ_ELLIPSE:

    break;

  case EZ_ARROW:

    break;


  case EZ_RUBBERBAND:

    break;
    
  case EZ_NONE:
    
    return FALSE;
    break;

  default:
    g_assert_not_reached();
    break;
  }

  return TRUE;
}

void 
ezapp_add_item    (EZApp* ez, EZItem* item)
{
  ez->items = g_list_prepend(ez->items, item);

}

void
ezapp_set_snapping(EZApp* ez, gboolean snapping)
{
  
  if (ez->snapping) {
    gnome_appbar_set_status(ez->appbar, _("Snapping enabled"));
  }
  else {
    gnome_appbar_set_status(ez->appbar, _("Snapping off"));
  }
}

static void
zoom_adjustment_changed(GtkAdjustment* adj, EZApp* ez)
{
  gnome_canvas_set_pixels_per_unit(ez->canvas, adj->value);
}


static void
snap_adjustment_changed(GtkAdjustment* adj, EZApp* ez)
{
  ez->snap_force = adj->value;
}


static void 
ezpaint_new_ezapp()
{
  EZApp * app;
  GtkWidget * app_box;
  GnomeCanvasItem* group;
  GnomeCanvasItem* handle_group, *item;
  GtkWidget * scale, * frame;
  GtkWidget * hbox;
  GtkWidget * label;
  GtkAdjustment * adj;
  GtkWidget * vbar, * hbar;
  GtkWidget * table;
  GtkAttachOptions options;
  double i, j;
  EZItem* ezi1,* ezi2;

  gint width = 480, height = 500;

  app = g_new(EZApp, 1);

  apps = g_list_append(apps, app);

  app->tool = EZ_NONE;
  app->items = NULL;
  app->selection = NULL;

  app->app = GNOME_APP(gnome_app_new( APPNAME, _("EZ Paint"))); 
  app->appbar = GNOME_APPBAR(gnome_appbar_new(FALSE, TRUE, 
                                              GNOME_PREFERENCES_USER));
  gnome_app_set_statusbar(GNOME_APP(app->app), GTK_WIDGET(app->appbar));

  gtk_window_set_policy(GTK_WINDOW(app->app), TRUE, TRUE, TRUE);
  gtk_widget_set_usize (GTK_WIDGET(app->app), width, height);

  gnome_app_create_menus_with_data(GNOME_APP(app->app), main_menu, app);
  gnome_app_create_toolbar_with_data(GNOME_APP(app->app), toolbar, app);

  app_box = gtk_vbox_new ( FALSE, GNOME_PAD );
  gtk_container_border_width(GTK_CONTAINER(app_box), GNOME_PAD);

  gnome_app_set_contents ( GNOME_APP(app->app), app_box );

  gtk_signal_connect ( GTK_OBJECT (app->app), "delete_event",
                       GTK_SIGNAL_FUNC (ezapp_delete_event),
                       app );

  /* Sensitive */
  app->sensitive = FALSE;
  
  app->sensitive_button = GTK_TOGGLE_BUTTON(toolbar[SENSITIVE_TOGGLE_INDEX].widget);

  /* Snapping */

  ezapp_set_snapping(app, FALSE);

  app->snap_button = GTK_TOGGLE_BUTTON(toolbar[SNAP_TOGGLE_INDEX].widget);

  hbox = gtk_hbox_new(FALSE,0);
  label = gtk_label_new(_("Snapping force"));

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  app->snap_force = 10.0;

  adj = GTK_ADJUSTMENT(gtk_adjustment_new(app->snap_force,
                                          1.0, 
                                          10.0,
                                          0.1,
                                          0.5,
                                          0.5));

  scale = gtk_hscale_new(adj);

  gtk_box_pack_end(GTK_BOX(hbox), scale, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX(app_box), GTK_WIDGET(hbox), 
                     FALSE, FALSE, 0);  

  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
                     GTK_SIGNAL_FUNC(snap_adjustment_changed), 
                     app);


  /* Zooming */

  hbox = gtk_hbox_new(FALSE,0);
  label = gtk_label_new(_("Zoom"));

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);

  adj = GTK_ADJUSTMENT(gtk_adjustment_new(1.0,
                                          0.1, 
                                          10.0,
                                          0.1,
                                          0.5,
                                          0.5));

  scale = gtk_hscale_new(adj);

  gtk_box_pack_end(GTK_BOX(hbox), scale, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX(app_box), GTK_WIDGET(hbox), 
                     FALSE, FALSE, 0);  

  gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
                     GTK_SIGNAL_FUNC(zoom_adjustment_changed), app);

  frame = gtk_frame_new(NULL);
  gtk_frame_set_shadow_type(GTK_FRAME(frame), GTK_SHADOW_IN);

  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());

  app->canvas = GNOME_CANVAS(gnome_canvas_new_aa());

  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  gnome_canvas_set_scroll_region(app->canvas, -1000.0, -1000.0, 
                                 1000.0, 1000.0);

  gtk_container_add(GTK_CONTAINER(frame), GTK_WIDGET(app->canvas));
  
  table = gtk_table_new(2,2,FALSE);

  vbar = gtk_vscrollbar_new(gtk_layout_get_vadjustment(GTK_LAYOUT(app->canvas)));
  hbar = gtk_hscrollbar_new(gtk_layout_get_hadjustment(GTK_LAYOUT(app->canvas)));

  options = GTK_EXPAND | GTK_FILL;
  
  gtk_table_attach(GTK_TABLE(table), GTK_WIDGET(frame),
                   0,1,0,1, options, options, 0, 0);
  
  gtk_table_attach(GTK_TABLE(table), GTK_WIDGET(vbar),
                   1,2,0,1, GTK_SHRINK, options, 0, 0);
  
  gtk_table_attach(GTK_TABLE(table), GTK_WIDGET(hbar),
                   0,1,1,2, options, GTK_SHRINK, 0, 0);
  
  gtk_box_pack_start(GTK_BOX(app_box), GTK_WIDGET(table), 
                     TRUE, TRUE, GNOME_PAD);

  
  app->paste = 
    (EZPaste*)gnome_canvas_item_new(GNOME_CANVAS_GROUP(app->canvas->root),
                                    ez_paste_get_type(),
                                    "x1", -300.0,
                                    "y1", -300.0,
                                    "x2", 600.0,
                                    "y2", 600.0,
                                    /* "bg", TRUE,  */
                                    /*       "grid", TRUE,
                                             "hgridspace", 20.0,
                                             "vgridspace", 20.0,
                                             "grid_color_rgba", 0x00000080, */
                                    NULL);

  gtk_signal_connect(GTK_OBJECT(app->paste), "event",
                     GTK_SIGNAL_FUNC(page_clicked), app);

  
  ezi1 = ez_rect_new(app->paste);

  ez_item_set_toplevel(ezi1, TRUE);

  ez_item_move_resize(ezi1, EZ_ANCHOR_CENTER, 150.0, 150.0, 350.0, 360.0);

  ezi2 = ez_rect_new(app->paste);

  ez_container_add(EZ_CONTAINER(ezi1), ezi2);

  /* Note that these coords are relative to its parent */
  ez_item_move_resize(ezi2, EZ_ANCHOR_CENTER, 30.0, 10.0, 60.0, 90.0);

  ezi2 = ez_rect_new(app->paste);

  ez_container_add(EZ_CONTAINER(ezi1), ezi2);

  ez_item_move_resize(ezi2, EZ_ANCHOR_CENTER, 70.0, 100.0, 100.0, 120.0);

  ezi1 = ez_rect_new(app->paste);

  ez_item_set_toplevel(ezi1, TRUE);

  ez_item_move_resize(ezi1, EZ_ANCHOR_CENTER, 50.0, 50.0, 190.0, 110.0);

  ezi2 = ez_rect_new(app->paste);

  ez_container_add(EZ_CONTAINER(ezi1), ezi2);

  /* Note that these coords are relative to its parent */
  ez_item_move_resize(ezi2, EZ_ANCHOR_CENTER, 30.0, 10.0, 60.0, 90.0);


  /* So we can find the handle. */
  gnome_canvas_item_new(GNOME_CANVAS_GROUP(app->canvas->root),
                        gnome_canvas_rect_get_type(),
                        "x1", 90.0, 
                        "y1", 90.0,
                        "x2", 110.0,
                        "y2", 110.0,
                        "outline_color", "green",
                        NULL);

  gnome_canvas_item_new(GNOME_CANVAS_GROUP(app->canvas->root),
                        ez_handle_get_type(),
                        "x", 100.0, 
                        "y", 100.0,
                        NULL);

  gtk_widget_show_all(GTK_WIDGET(app->app));
}

/* Toolbar callbacks */

static void 
line_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_LINE;
}


static void 
hrule_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_HRULE;
}


static void 
vrule_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_VRULE;
}


static void 
rect_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_RECT;
}

static void 
ellipse_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_ELLIPSE;
}

static void 
arrow_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_ARROW;
}

static void 
rubberband_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_RUBBERBAND;
}

static void 
none_tool_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ez->tool = EZ_NONE;
}

static void 
snapping_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

  ezapp_set_snapping(ez, !ez->snapping);
}

static void 
sensitive_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  GList* tmp;
  GnomeCanvasItem* item;

  ez->sensitive = ! ez->sensitive;

  if (ez->sensitive)
    gnome_appbar_set_status(ez->appbar, _("Sensitivity on"));
  else 
    gnome_appbar_set_status(ez->appbar, _("Sensitivity off"));
}


static void 
delete_selection_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  GList * tmp = g_list_copy(ez->selection);

  while (tmp) {
    gtk_object_destroy(GTK_OBJECT(tmp->data));
    ez->items = g_list_remove(ez->items, tmp->data);
    tmp = g_list_next(tmp);
  }
  g_assert(ez->selection == NULL);
  
  g_list_free(tmp);
}

static void 
group_selection_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  GList* tmp;

  /* Create and add group */

}

static void 
unimplemented_cb(GtkWidget * w, gpointer data)
{
  gnome_warning_dialog("This feature is incomplete or totally unimplemented.\n"
                       "See TODO or mail hp@pobox.com for notes, then jump in\n"
                       "and add this function!");
}

/* New item menu */

static void 
new_line_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

}

static void 
new_hrule_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

}

static void 
new_vrule_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

}

static void 
new_rect_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

}


static void 
new_ellipse_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

}


static void 
new_arrow_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;

}

static void popup_about()
{
  GtkWidget * ga;
  gchar * authors[] = { "Havoc Pennington <hp@pobox.com>",
                        NULL };

  ga = gnome_about_new (APPNAME,
                        VERSION, 
                        _(COPYRIGHT_NOTICE),
                        authors,
                        _("EZ Paint should not be taken seriously."),
                        0 );
  
  gtk_widget_show(ga);
}


static void
new_app_cb(GtkWidget * w, gpointer data)
{
  ezpaint_new_ezapp();
}
static void
open_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  gnome_app_message(ez->app, _("Open clicked"));
}

static void
close_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  ezpaint_destroy_ezapp(ez);
}
static void
save_as_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  gnome_app_message(ez->app, _("Save as clicked"));
}

static void
exit_cb(GtkWidget * w, gpointer data)
{
  EZApp* ez = (EZApp*)data;
  gtk_main_quit();
}

static void
about_cb(GtkWidget * w, gpointer data)
{
  popup_about();
}
