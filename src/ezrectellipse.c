/* Editable Rectangle and Ellipse items.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezrectellipse.h"

#include <gtk/gtksignal.h>

#include <config.h>

static void ez_re_class_init (EZREClass *class);
static void ez_re_init       (EZRE      *ezre);
static void ez_re_destroy    (GtkObject            *object);
static void ez_re_set_arg    (GtkObject            *object,
                              GtkArg               *arg,
                              guint                 arg_id);
static void ez_re_get_arg    (GtkObject            *object,
                              GtkArg               *arg,
                              guint                 arg_id);

static void ez_re_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags);

static void ez_re_start_editing (EZItem* ezi);

static void ez_re_stop_editing  (EZItem* ezi);

static void ez_re_start_moving  (EZItem* ezi);

static void ez_re_stop_moving   (EZItem* ezi);

static void ez_re_start_sizing  (EZItem* ezi);

static void ez_re_stop_sizing   (EZItem* ezi);

static void ez_re_select        (EZItem* ezi);

static void ez_re_unselect      (EZItem* ezi);

static void ez_re_move_resize   (EZItem* ezi,
                                 EZAnchorType which_handle,
                                 double x1, double y1, 
                                 double x2, double y2);

static gboolean ez_re_pass_event    (EZItem* ezi, EZItem* origin, 
                                     GdkEvent* event);

static void ez_re_construct (EZItem* ezi, EZPaste* paste);

static void ez_re_get_size  (EZItem* ezi, double* x1, double* y1, double* x2, double* y2);

static void ez_re_add           (EZContainer* ezc, EZItem* child);
static void ez_re_remove        (EZContainer* ezc, EZItem* child);

enum {
  ARG_0
};


enum {
  
  LAST_SIGNAL
};

static EZItemClass *parent_class;

static gint item_signals[LAST_SIGNAL] = { 0 };

GtkType
ez_re_get_type (void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "EZRE",
      sizeof (EZRE),
      sizeof (EZREClass),
      (GtkClassInitFunc) ez_re_class_init,
      (GtkObjectInitFunc) ez_re_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (ez_container_get_type (), &item_info);
  }

  return item_type;
}

static void
ez_re_class_init (EZREClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  EZItemClass* ezitem_class;
  EZContainerClass* container_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  ezitem_class = (EZItemClass* ) klass;
  container_class = (EZContainerClass* ) klass;

  parent_class = gtk_type_class (ez_container_get_type ());

#if 0
  gtk_object_class_add_signals (object_class, item_signals, 
				LAST_SIGNAL);
#endif

  container_class->add = ez_re_add;
  container_class->remove = ez_re_remove;

  ezitem_class->start_editing = ez_re_start_editing;
  ezitem_class->stop_editing  = ez_re_stop_editing;

  ezitem_class->start_moving  = ez_re_start_moving;
  ezitem_class->stop_moving   = ez_re_stop_moving;

  ezitem_class->start_sizing  = ez_re_start_sizing;
  ezitem_class->stop_sizing   = ez_re_stop_sizing;

  ezitem_class->select        = ez_re_select;
  ezitem_class->unselect      = ez_re_unselect;

  ezitem_class->move_resize   = ez_re_move_resize;

  ezitem_class->pass_event    = ez_re_pass_event;

  ezitem_class->construct     = ez_re_construct;

  ezitem_class->get_size      = ez_re_get_size;

  item_class->update = ez_re_update;

  object_class->destroy = ez_re_destroy;
  object_class->set_arg = ez_re_set_arg;
  object_class->get_arg = ez_re_get_arg;

  klass->create_shape = NULL;
}

static void
ez_re_init (EZRE *ezre)
{
  ezre->shape = NULL;
  ezre->handles = NULL;
  ezre->coords[0] = 0.0;
  ezre->coords[1] = 0.0;
  ezre->coords[2] = 0.0;
  ezre->coords[3] = 0.0;
}

static void
ez_re_destroy (GtkObject *object)
{
  EZRE *ezre;

  g_return_if_fail (object != NULL);
  g_return_if_fail (EZ_IS_RE(object));

  ezre = EZ_RE (object);

  ez_debug("Destroying EZRE %p", ezre);

  /* Shape goes down with the group */

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/******* Sync stuff *************/


typedef enum {
  NW,
  N,
  NE,
  W,
  E,
  SW,
  S,
  SE,
  LAST_HANDLE
} Handle;

/* Get our coordinates in shape/handle item coords */
static void
item_square(EZRE* ezre, GnomeCanvasItem* childitem,
            double* x1, double* y1, 
            double* x2, double* y2)
{
  g_return_if_fail(ezre->shape != NULL);

  ez_debug("Our coords: %g,%g %g,%g", *x1, *y1, *x2, *y2);

  gnome_canvas_item_i2w(GNOME_CANVAS_ITEM(ezre), x1, y1);
  gnome_canvas_item_w2i(childitem, x1, y1);
  gnome_canvas_item_i2w(GNOME_CANVAS_ITEM(ezre), x2, y2);
  gnome_canvas_item_w2i(childitem, x2, y2);

  ez_debug("Child coords: %g,%g %g,%g", *x1, *y1, *x2, *y2);
}

static void sync_handles(EZRE* ezre);

static void 
ez_re_handle_moved(EZHandle* handle,
                   gdouble x, gdouble y,
                   gdouble dx, gdouble dy,
                   gpointer data)
{
  Handle whichone = GPOINTER_TO_INT(data);
  EZRE* ezre;
  double coords[4];

  ezre = gtk_object_get_user_data(GTK_OBJECT(handle));
  g_return_if_fail(ezre != NULL);
  
  /* If we changed the ezre in-place, it would break the old
     size which we need in EZContainer's move_resize */

  coords[0] = ezre->coords[0];
  coords[1] = ezre->coords[1];
  coords[2] = ezre->coords[2];
  coords[3] = ezre->coords[3];
  

  switch (whichone) {
  case NW:
    coords[0] += dx;
    coords[1] += dy;
    break;

  case N:
    coords[1] += dy;
    break;

  case NE:
    coords[2] += dx;
    coords[1] += dy;
    break;
    
  case W:
    coords[0] += dx;
    break;

  case E:
    coords[2] += dx;
    break;

  case SW:
    coords[0] += dx;
    coords[3] += dy;
    break;

  case S:
    coords[3] += dy;
    break;

  case SE:
    coords[2] += dx;
    coords[3] += dy;
    break;

  default:
    g_assert_not_reached();
    break;
  }

  ez_item_move_resize(EZ_ITEM(ezre),
                      handle->anchor,
                      MIN(coords[0], coords[2]),
                      MIN(coords[1], coords[3]),
                      MAX(coords[2], coords[0]),
                      MAX(coords[3], coords[1]));
}


static EZAnchorType normal_anchors[LAST_HANDLE] = {
  EZ_ANCHOR_NW,     /* NW */
  EZ_ANCHOR_NORTH,  /* N  */
  EZ_ANCHOR_NE,     /* NE */
  EZ_ANCHOR_WEST,   /* W  */
  EZ_ANCHOR_EAST,   /* E  */
  EZ_ANCHOR_SW,     /* SW */
  EZ_ANCHOR_SOUTH,  /* S  */
  EZ_ANCHOR_SE      /* SE */
};

static EZAnchorType x_reversed_anchors[LAST_HANDLE] = {
  EZ_ANCHOR_NE,     /* NW */
  EZ_ANCHOR_NORTH,  /* N  */
  EZ_ANCHOR_NW,     /* NE */
  EZ_ANCHOR_EAST,   /* W  */
  EZ_ANCHOR_WEST,   /* E  */
  EZ_ANCHOR_SE,     /* SW */
  EZ_ANCHOR_SOUTH,  /* S  */
  EZ_ANCHOR_SW      /* SE */
};
  
static EZAnchorType y_reversed_anchors[LAST_HANDLE] = {
  EZ_ANCHOR_SW,     /* NW */
  EZ_ANCHOR_SOUTH,  /* N  */
  EZ_ANCHOR_SE,     /* NE */
  EZ_ANCHOR_WEST,   /* W  */
  EZ_ANCHOR_EAST,   /* E  */
  EZ_ANCHOR_NW,     /* SW */
  EZ_ANCHOR_NORTH,  /* S  */
  EZ_ANCHOR_NE      /* SE */
};

  
static EZAnchorType both_reversed_anchors[LAST_HANDLE] = {
  EZ_ANCHOR_SE,     /* NW */
  EZ_ANCHOR_SOUTH,  /* N  */
  EZ_ANCHOR_SW,     /* NE */
  EZ_ANCHOR_EAST,   /* W  */
  EZ_ANCHOR_WEST,   /* E  */
  EZ_ANCHOR_NE,     /* SW */
  EZ_ANCHOR_NORTH,  /* S  */
  EZ_ANCHOR_NW      /* SE */
};


static void 
sync_handles(EZRE* ezre)
{
  double x1, y1, x2, y2, midx, midy;
  EZHandle** h = ezre->handles; /* Save typing; lazy. */
  EZAnchorType* anchors;
  gboolean x_reversed;
  gboolean y_reversed;

  g_return_if_fail(h != NULL);

  x1 = 0.0;
  y1 = 0.0;
  x2 = MAX(ezre->coords[0], ezre->coords[2]) - 
    MIN(ezre->coords[0], ezre->coords[2]);
  y2 = MAX(ezre->coords[1], ezre->coords[3]) - 
    MIN(ezre->coords[1], ezre->coords[3]);

  /* Pick an arbitrary handle to adjust with */
  item_square(ezre, GNOME_CANVAS_ITEM(h[0]), &x1, &y1, &x2, &y2);

  midx = x1 + (x2-x1)/2;
  midy = y1 + (y2-y1)/2;

  x_reversed = (x1 > x2);
  y_reversed = (y1 > y2);

  if (x_reversed && y_reversed) {
    anchors = both_reversed_anchors;
  }
  else if (x_reversed) {
    anchors = x_reversed_anchors;
  }
  else if (y_reversed) {
    anchors = y_reversed_anchors;
  }
  else {
    anchors = normal_anchors;
  }

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[NW]),
                        "x", x1,
                        "y", y1,
                        "anchor", anchors[NW],
                        NULL);
  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[N]),
                        "x", midx,
                        "y", y1,
                        "anchor", anchors[N],
                        NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[NE]),
                        "x", x2,
                        "y", y1,
                        "anchor", anchors[NE],
                        NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[W]),
                        "x", x1,
                        "y", midy,
                        "anchor", anchors[W],
                        NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[E]),
                        "x", x2,
                        "y", midy,
                        "anchor", anchors[E],
                        NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[SW]),
                        "x", x1,
                        "y", y2,
                        "anchor", anchors[SW],
                        NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[S]),
                        "x", midx,
                        "y", y2,
                        "anchor", anchors[S],
                        NULL);

  gnome_canvas_item_set(GNOME_CANVAS_ITEM(h[SE]),
                        "x", x2,
                        "y", y2,
                        "anchor", anchors[SE],
                        NULL);
}

static void
ez_re_size_start(EZHandle* handle, gpointer data)
{
  EZRE* ezre = EZ_RE(data);

  ez_item_start_sizing(EZ_ITEM(ezre));
}

static void
ez_re_size_stop(EZHandle* handle, gpointer data)
{
  EZRE* ezre = EZ_RE(data);

  ez_item_stop_sizing(EZ_ITEM(ezre));
}

/* Match display to current state (sizing, editing, etc.) */
static void
sync_state (EZRE* ezre)
{
  EZItem* ezi;
  
  g_return_if_fail(ezre != NULL);
  g_return_if_fail(EZ_IS_RE(ezre));

  ezi = EZ_ITEM(ezre);

  if (ezi->editing && (ezre->handles == NULL))
    {
      /* Create the handles */
      EZHandle** h = g_malloc(sizeof(EZHandle*)*(LAST_HANDLE+1));
      int i = 0;
      while (i < LAST_HANDLE)
        {
          h[i] = (EZHandle*) gnome_canvas_item_new(GNOME_CANVAS_GROUP(ezre),
                                                   ez_handle_get_type(),
                                                   NULL);

          gtk_object_set_user_data(GTK_OBJECT(h[i]), ezre);

          gtk_signal_connect(GTK_OBJECT(h[i]), "moved",
                             GTK_SIGNAL_FUNC(ez_re_handle_moved),
                             GINT_TO_POINTER(i));

          gtk_signal_connect(GTK_OBJECT(h[i]), "start_move",
                             GTK_SIGNAL_FUNC(ez_re_size_start),
                             ezre);

          gtk_signal_connect(GTK_OBJECT(h[i]), "stop_move",
                             GTK_SIGNAL_FUNC(ez_re_size_stop),
                             ezre);

          ++i;
        }
      g_return_if_fail(i == LAST_HANDLE);

      h[LAST_HANDLE] = NULL;

      ezre->handles = h;
    }
  else if (!ezi->editing && (ezre->handles != NULL))
    {
      /* Destroy the handles */
      EZHandle** h = ezre->handles;
      while (*h)
        {
          g_return_if_fail(EZ_IS_HANDLE(*h));
          
          gtk_object_destroy(GTK_OBJECT(*h));
          
          ++h;
        }
      
      g_free(ezre->handles);
      ezre->handles = NULL;
    }

  if (ezre->shape != NULL) 
    { 
      /* EZRectEllipse doesn't care about order, but
         GnomeCanvasRect does. */

      double x1 = MIN(ezre->coords[0], ezre->coords[2]);
      double x2 = MAX(ezre->coords[0], ezre->coords[2]);

      double y1 = MIN(ezre->coords[1], ezre->coords[3]);
      double y2 = MAX(ezre->coords[1], ezre->coords[3]);

      double w = x2 - x1;
      double h = y2 - y1;

      /* This is sort of a no-no messing in the struct, but is so much
       * simpler than the alternatives it should be worth the risk of
       * breakage. */
          
      if (((ezre->shape->x2 - ezre->shape->x1) != w) ||
          ((ezre->shape->y2 - ezre->shape->y1) != h) || TRUE)
        {
          /* Shape x1, y1 is always 0,0 - group position determines that. */
          gnome_canvas_item_set (GNOME_CANVAS_ITEM(ezre->shape),
                                 "x1", 0.0,
                                 "y1", 0.0,
                                 "x2", w,
                                 "y2", h,
                                 NULL);
        }

      /* Set the group origin */
      gnome_canvas_item_set (GNOME_CANVAS_ITEM(ezre),
                             "x", x1,
                             "y", y1,
                             NULL);
    }
  
  if (ezre->handles != NULL)
    {
      sync_handles(ezre);
    }
}

static void 
ez_re_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags)
{
  sync_state(EZ_RE(item));
  
  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->update)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->update) (item, affine, clip_path, flags);
}

static void
ez_re_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  EZRE* ezre;

  item = GNOME_CANVAS_ITEM (object);
  ezre = EZ_RE (object);

  switch (arg_id) {

  default:
    g_warning("EZRE got an unknown arg type.");
    break;
  }

#if 0
  gnome_canvas_re_request_update(item);
#endif
}


static void
ez_re_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  EZRE* ezre;

  ezre = EZ_RE(object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

/************ End Canvas Item Functions ******************/


static void
ez_re_start_editing (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->start_editing)
    (* EZ_ITEM_CLASS(parent_class)->start_editing)(ezi);

  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(ezi));
}

static void
ez_re_stop_editing  (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));


  if (EZ_ITEM_CLASS(parent_class)->stop_editing)
    (* EZ_ITEM_CLASS(parent_class)->stop_editing)(ezi);

  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(ezi));
}

static void
ez_re_start_moving  (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->start_moving)
    (* EZ_ITEM_CLASS(parent_class)->start_moving)(ezi);
}

static void
ez_re_stop_moving   (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->stop_moving)
    (* EZ_ITEM_CLASS(parent_class)->stop_moving)(ezi);
}

static void
ez_re_start_sizing  (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->start_sizing)
    (* EZ_ITEM_CLASS(parent_class)->start_sizing)(ezi);
}

static void
ez_re_stop_sizing   (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->stop_sizing)
    (* EZ_ITEM_CLASS(parent_class)->stop_sizing)(ezi);
}

static void
ez_re_select        (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->select)
    (* EZ_ITEM_CLASS(parent_class)->select)(ezi);
}

static void
ez_re_unselect      (EZItem* ezi)
{
  EZRE* ezre = EZ_RE(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  if (EZ_ITEM_CLASS(parent_class)->unselect)
    (* EZ_ITEM_CLASS(parent_class)->unselect)(ezi);
}

static void
ez_re_move_resize   (EZItem* ezi, 
                     EZAnchorType which_handle,
                     double x1, double y1, 
                     double x2, double y2)
{
  EZRE* ezre = EZ_RE(ezi);
  gdouble* xmin, *xmax, *ymin, *ymax;

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));
  
  ez_debug("EZRE move_resize %p", ezi);

  /* Essential to do container first, because it needs access to 
     our old size */
  if (EZ_ITEM_CLASS(parent_class)->move_resize)
    (* EZ_ITEM_CLASS(parent_class)->move_resize)(ezi, which_handle, x1, y1, x2, y2);

  /* If we're out of order, reverse things. */
  if (ezre->coords[0] > ezre->coords[2]) {
    xmin = &ezre->coords[2];
    xmax = &ezre->coords[0];
  }
  else {
    xmax = &ezre->coords[2];
    xmin = &ezre->coords[0];
  }
  
  if (ezre->coords[1] > ezre->coords[3]) {
    ymin = &ezre->coords[3];
    ymax = &ezre->coords[1];
  }
  else {
    ymax = &ezre->coords[3];
    ymin = &ezre->coords[1];
  }
  
  *xmin = x1;
  *xmax = x2;
  *ymin = y1;
  *ymax = y2;
}

static gboolean 
ez_re_pass_event    (EZItem* ezi, EZItem* origin, GdkEvent* event)
{
  EZRE* ezre = EZ_RE(ezi);
  gboolean handled = FALSE;

  g_return_val_if_fail(ezi != NULL, FALSE);
  g_return_val_if_fail(EZ_IS_RE(ezi), FALSE);
  g_return_val_if_fail(origin != NULL, FALSE);
  g_return_val_if_fail(EZ_IS_ITEM(origin), FALSE);
  g_return_val_if_fail(!origin->floating, FALSE);
  g_return_val_if_fail(!ezi->floating, FALSE);
  
  if (EZ_ITEM_CLASS(parent_class)->pass_event)
    handled = (* EZ_ITEM_CLASS(parent_class)->pass_event)(ezi, origin, event);

  if (handled)
    return TRUE;
  else
    {
      switch (event->type){
        
      case GDK_ENTER_NOTIFY:
        break;
        
      case GDK_LEAVE_NOTIFY:
        break;
        
      case GDK_BUTTON_PRESS:
        {


          return FALSE;
        }
        break;

      default:
        break;
      }
    }

  return FALSE;
}

static void 
ez_re_construct     (EZItem* ezi, EZPaste* paste)
{
  EZRE* ezre;

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));
  g_return_if_fail(paste != NULL);
  g_return_if_fail(EZ_IS_PASTE(paste));

  ezre = EZ_RE(ezi);

  ez_debug("Constructing EZRE %p", ezi);

  if (EZ_ITEM_CLASS(parent_class)->construct)
    (*EZ_ITEM_CLASS(parent_class)->construct)(ezi,paste);

  if (EZ_RE_CLASS(GTK_OBJECT(ezre)->klass)->create_shape)
    (* EZ_RE_CLASS(GTK_OBJECT(ezre)->klass)->create_shape)(ezre);

  gnome_canvas_item_lower_to_bottom(GNOME_CANVAS_ITEM(ezre->shape));

  /* Set the group origin */
  gnome_canvas_item_set(GNOME_CANVAS_ITEM(ezre),
                        "x", ezre->coords[0],
                        "y", ezre->coords[1],
                        NULL);

  /* x1,y1 is always at the group origin */
  gnome_canvas_item_set(GNOME_CANVAS_ITEM(ezre->shape),
                        "x1", 0.0,
                        "y1", 0.0,
                        NULL);

#ifdef GNOME_ENABLE_DEBUG
  gnome_canvas_item_set(GNOME_CANVAS_ITEM(ezre->shape),
                        "outline_color", "red",
                        "fill_color", "lavender",
                        NULL);
#endif

}


static void 
ez_re_get_size  (EZItem* ezi, double* x1, double* y1, double* x2, double* y2)
{
  EZRE* ezre;

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RE(ezi));

  ezre = EZ_RE(ezi);

  *x1 = MIN(ezre->coords[0], ezre->coords[2]);
  *y1 = MIN(ezre->coords[1], ezre->coords[3]);
  *x2 = MAX(ezre->coords[2], ezre->coords[0]);
  *y2 = MAX(ezre->coords[3], ezre->coords[1]);
}

/***** Container functions *********/

static void 
ez_re_add      (EZContainer* ezc, EZItem* child)
{
  EZRE* ezre;

  g_return_if_fail(ezc != NULL);
  g_return_if_fail(EZ_IS_RE(ezc));

  ezre = EZ_RE(ezc);

  if (EZ_CONTAINER_CLASS(parent_class)->add)
    (*EZ_CONTAINER_CLASS(parent_class)->add)(ezc,child);
}

static void 
ez_re_remove   (EZContainer* ezc, EZItem* child)
{
  EZRE* ezre;

  g_return_if_fail(ezc != NULL);
  g_return_if_fail(EZ_IS_RE(ezc));

  ezre = EZ_RE(ezc);

  if (EZ_CONTAINER_CLASS(parent_class)->remove)
    (*EZ_CONTAINER_CLASS(parent_class)->remove)(ezc,child);
}

