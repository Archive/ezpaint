/* Block arrow; for a line-type arrow use HandleLine
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLE_ARROW_H
#define GNOME_CANVAS_HANDLE_ARROW_H

#include <libgnome/gnome-defs.h>

#include <libgnomeui/gnome-canvas-polygon.h> /* fixme */

#include "gnome-canvas-handle.h"
#include "gnome-canvas-handled.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_HANDLE_ARROW            (gnome_canvas_handle_arrow_get_type ())
#define GNOME_CANVAS_HANDLE_ARROW(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLE_ARROW, GnomeCanvasHandleArrow))
#define GNOME_CANVAS_HANDLE_ARROW_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLE_ARROW, GnomeCanvasHandleArrowClass))
#define GNOME_IS_CANVAS_HANDLE_ARROW(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLE_ARROW))
#define GNOME_IS_CANVAS_HANDLE_ARROW_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLE_ARROW))

typedef struct _GnomeCanvasHandleArrow GnomeCanvasHandleArrow;
typedef struct _GnomeCanvasHandleArrowClass GnomeCanvasHandleArrowClass;

struct _GnomeCanvasHandleArrow {
  GnomeCanvasHandled handled;

  /* Can set fill color, border, etc. on this;
     don't set the position */
  GnomeCanvasPolygon* poly;

  /* don't play with these members: */

  double x1, y1, x2, y2;       /* The line down the center of the arrow */
  double width;                /* Width of the arrow shaft */

  /* See test-gnome canvas demo for a visual illustration of these */
  double a;                    /* Same as for lines; perpendicular
				  from the edge of the shaft to the
				  trailing arrowhead point */
  double b;                    /* Dimension parallel to shaft, from
				  trailing arrowhead point to
				  tip of arrowhead. */
  double c;                    /* Parallel to shaft, from intersection
				  of side of shaft with arrowhead to
				  the arrowhead point. */

  GnomeCanvasHandle** handles; /* There are 8 handles */

  GdkBitmap* stipple;
};

struct _GnomeCanvasHandleArrowClass {
  GnomeCanvasHandledClass parent_class;

  void (* shape_change)(GnomeCanvasHandleArrow*, double width, double a, double b, double c);
};

/* Standard Gtk function */
GtkType gnome_canvas_handle_arrow_get_type (void);

END_GNOME_DECLS

#endif

