/* Handle type for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLE_H
#define GNOME_CANVAS_HANDLE_H

#include <libgnome/gnome-defs.h>
/*#include "gnome-canvas.h"*/
#include <libgnomeui/gnome-canvas.h>
#include <gtk/gtkpacker.h> /* GtkAnchorType */

BEGIN_GNOME_DECLS


#define GNOME_TYPE_CANVAS_HANDLE            (gnome_canvas_handle_get_type ())
#define GNOME_CANVAS_HANDLE(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLE, GnomeCanvasHandle))
#define GNOME_CANVAS_HANDLE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLE, GnomeCanvasHandleClass))
#define GNOME_IS_CANVAS_HANDLE(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLE))
#define GNOME_IS_CANVAS_HANDLE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLE))

typedef void (*GnomeCanvasSnapFunc)(double* x, double* y, gpointer data);

typedef struct _GnomeCanvasHandle GnomeCanvasHandle;
typedef struct _GnomeCanvasHandleClass GnomeCanvasHandleClass;

struct _GnomeCanvasHandle {
  GnomeCanvasItem item;
  
  GdkGC* gc;

  /* Misleading: the corner or side of the object we're handling, not
     the anchor point of the handle. So if you want to have a control
     point on the bottom left corner, you want GTK_ANCHOR_SW. */
  GtkAnchorType anchor;

  /* Item coords position; this is the position opposite the anchor
     point, and should be the point on the object you are trying to
     control. If the user flips an object you can just flip the anchor 
     without changing x and y. */
  double x;  
  double y;
  
  GnomeCanvasSnapFunc snap_func;
  gpointer snap_func_data;

  int prelight  : 1;   /* show that the user has successfully gotten the mouse
			 over this tiny thing */
  int dragging  : 1;   /* we're dragging the handle */ 
  int sensitive : 1;  /* allow drags */
};

struct _GnomeCanvasHandleClass {
  GnomeCanvasItemClass parent_class;

  /* This is called only if the *user* moves the handle,
     not if the programmer sets x and y. World coords. */
  void (* moved)(GnomeCanvasHandle* handle, double new_x, double new_y);
};

/* Standard Gtk function */
GtkType gnome_canvas_handle_get_type (void);

/* typedef void (*GnomeCanvasSnapFunc)(double* x, double* y, gpointer data); */

/* Snap function takes two double* and a data arg (passed in to
   set_snap_func).  The double*'s will start out with where the user
   is trying to position the handle, and the snap function can modify
   the position, or not, as it sees fit.  double*'s passed in are item
   coordinates */

/* The snap function is called *before* enforcing anchor-imposed
   constraints. */

void    gnome_canvas_handle_set_snap_func(GnomeCanvasHandle* handle,
					  GnomeCanvasSnapFunc func, 
					  gpointer data);

/* If the user is dragging the first handle, transfer the pointer grab
   so they're now dragging the second handle. If either is NULL, just
   do half the operation (start a drag, or end one) */
void    gnome_canvas_handle_transfer_drag(GnomeCanvasHandle* current_grab, 
					  GnomeCanvasHandle* new_grab,
					  guint32 event_time);


END_GNOME_DECLS

#endif

