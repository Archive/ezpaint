/* 8-handled box; subclasses put an item in the box.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLE_BOX_H
#define GNOME_CANVAS_HANDLE_BOX_H

#include <libgnome/gnome-defs.h>

#include "gnome-canvas-handle.h"
#include "gnome-canvas-handled.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_HANDLE_BOX            (gnome_canvas_handle_box_get_type ())
#define GNOME_CANVAS_HANDLE_BOX(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLE_BOX, GnomeCanvasHandleBox))
#define GNOME_CANVAS_HANDLE_BOX_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLE_BOX, GnomeCanvasHandleBoxClass))
#define GNOME_IS_CANVAS_HANDLE_BOX(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLE_BOX))
#define GNOME_IS_CANVAS_HANDLE_BOX_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLE_BOX))

typedef struct _GnomeCanvasHandleBox GnomeCanvasHandleBox;
typedef struct _GnomeCanvasHandleBoxClass GnomeCanvasHandleBoxClass;

struct _GnomeCanvasHandleBox {
  GnomeCanvasHandled handled;

  /* This is the item contained in the box; NULL for 
     some subclasses. */
  GnomeCanvasItem* item;

  /* don't play with these members: */

  double coords[4];

  GnomeCanvasHandle** handles;
};

struct _GnomeCanvasHandleBoxClass {
  GnomeCanvasHandledClass parent_class;

  /* Virtual function; syncs the item with the 
     state of the handle box */
  void (*sync_item)(GnomeCanvasHandleBox* handle_box,
		    GtkAnchorType which_handle);

  /* Fills in the item field */
  void (*create_item)(GnomeCanvasHandleBox* handle_box);
};

/* Standard Gtk function */
GtkType gnome_canvas_handle_box_get_type (void);

END_GNOME_DECLS

#endif

