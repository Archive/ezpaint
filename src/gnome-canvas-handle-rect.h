/* Handled rect type for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLE_RECT_H
#define GNOME_CANVAS_HANDLE_RECT_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas-rect-ellipse.h> /* fixme when we go to gnome-libs if we do */
#include "gnome-canvas-handle-box.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_HANDLE_RECT            (gnome_canvas_handle_rect_get_type ())
#define GNOME_CANVAS_HANDLE_RECT(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLE_RECT, GnomeCanvasHandleRect))
#define GNOME_CANVAS_HANDLE_RECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLE_RECT, GnomeCanvasHandleRectClass))
#define GNOME_IS_CANVAS_HANDLE_RECT(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLE_RECT))
#define GNOME_IS_CANVAS_HANDLE_RECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLE_RECT))

typedef struct _GnomeCanvasHandleRect GnomeCanvasHandleRect;
typedef struct _GnomeCanvasHandleRectClass GnomeCanvasHandleRectClass;

struct _GnomeCanvasHandleRect {
  GnomeCanvasHandleBox handle_box;

  GdkBitmap* stipple;
};

struct _GnomeCanvasHandleRectClass {
  GnomeCanvasHandleBoxClass parent_class;

};

/* Standard Gtk function */
GtkType gnome_canvas_handle_rect_get_type (void);

END_GNOME_DECLS

#endif

