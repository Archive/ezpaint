/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZItem: abstract base class for editable canvas items.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_ITEM_H
#define EZ_ITEM_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

#include "ezpaste.h"
#include "ezhandle.h"

BEGIN_GNOME_DECLS

#define EZ_TYPE_ITEM            (ez_item_get_type ())
#define EZ_ITEM(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_ITEM, EZItem))
#define EZ_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_ITEM, EZItemClass))
#define EZ_IS_ITEM(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_ITEM))
#define EZ_IS_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_ITEM))

typedef struct _EZItem EZItem;
typedef struct _EZItemClass EZItemClass;

/* All EZItem inherit from GnomeCanvasGroup; the reason is that we
 *  want to implement the EZItem as a collection of normal
 *  canvas items. Also, this way we don't have to fight with 
 *  z-order quite so much. Although it's still a pain. 
 *
 * The CanvasGroup aspect of EZItem should be considered opaque; 
 *  we are going to abuse the Group in funky ways.
 */

struct _EZContainer;

struct _EZItem {
  GnomeCanvasGroup group;

  /* struct members are all private. */

  struct _EZContainer* parent; /* container we're in */

  EZPaste* paste; /* paste board we're on; this could always be
                     computed by scanning up the container tree, so we
                     might want to take this out to save memory. */
  
  guint editing  : 1; /* We have the edit focus */
  guint moving   : 1; /* We are being dragged */
  guint sizing   : 1; /* We are being resized */
  guint selected : 1; /* We are selected */
  guint floating : 1; /* Hidden and held temporarily by the pasteboard. */
  guint toplevel : 1; /* We are a toplevel item, no parent */
};

struct _EZItemClass {
  GnomeCanvasGroupClass parent_class;

  /* Signals */

  void (* start_editing) (EZItem* ezi);
  void (* stop_editing)  (EZItem* ezi);

  void (* start_moving)  (EZItem* ezi);
  void (* stop_moving)   (EZItem* ezi);

  void (* start_sizing)  (EZItem* ezi);
  void (* stop_sizing)   (EZItem* ezi);

  void (* select)        (EZItem* ezi);
  void (* unselect)      (EZItem* ezi);

  void (* move_resize)   (EZItem* ezi, EZAnchorType which_handle, 
                          double x1, double y1, double x2, double y2);

  /* Class functions */

  void (* construct)     (EZItem* ezi, EZPaste* paste);
  
  /* Returns TRUE if the event was handled; but we go in the opposite
     direction, i.e. each child checks if the parent wants it, if not
     the child processes it. */
  gboolean (* pass_event)    (EZItem* ezi, EZItem* origin, GdkEvent* event);
  
  void (* get_size)      (EZItem* ezi, double* x1, double* y1, double* x2, double* y2);

};

/* Standard Gtk function */

GtkType   ez_item_get_type (void);

/* These defines are library-private */

#define        EZ_ITEM_EDITING(ezi)    (EZ_ITEM(ezi)->editing)
#define        EZ_ITEM_MOVING(ezi)    (EZ_ITEM(ezi)->moving)
#define        EZ_ITEM_SIZING(ezi)    (EZ_ITEM(ezi)->sizing)
#define        EZ_ITEM_SELECTED(ezi)    (EZ_ITEM(ezi)->selected)

/* Functions to emit all the signals */

void      ez_item_start_editing (EZItem* ezi);
void      ez_item_stop_editing  (EZItem* ezi);

void      ez_item_start_moving  (EZItem* ezi);
void      ez_item_stop_moving   (EZItem* ezi);

void      ez_item_start_sizing  (EZItem* ezi);
void      ez_item_stop_sizing   (EZItem* ezi);

void      ez_item_select        (EZItem* ezi);
void      ez_item_unselect      (EZItem* ezi);

/* which_handle is the position of the (possibly imaginary) handle
   being moved in order to perform the scaling. If in doubt, use
   EZ_ANCHOR_CENTER */

void      ez_item_move_resize   (EZItem* ezi, EZAnchorType which_handle,
                                 double x1, double y1, double x2, double y2);

void      ez_item_get_size      (EZItem* ezi, double* x1, double* y1, double* x2, double* y2);

/* Pass an event to the item; used internally. Return value is used to
   indicate whether the event was handled, but this determines whether
   subclasses should act on it or leave it up to the superclass, NOT
   whether the event is propagated. */

gboolean  ez_item_pass_event    (EZItem* ezi, 
                                 EZItem* origin, 
                                 GdkEvent* event);

/* Macro to access 'paste' member, in case we decide to compute it on
   the fly rather than storing it. */

#define   ez_item_get_paste(x)  (EZ_ITEM(x)->paste)

/* Use these instead of the canvas versions, we operate on EZContainer
   rather than GnomeCanvasGroup */
void      ez_item_raise         (EZItem* ezi);
void      ez_item_lower         (EZItem* ezi);

/* Internal function used in the _new() call of  subclasses. */
EZItem*   ez_item_build         (EZPaste* paste, GtkType type);

END_GNOME_DECLS

#endif

