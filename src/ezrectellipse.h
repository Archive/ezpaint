/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZRE: editable rectangle and ellipse base class
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_RECTELLIPSE_H
#define EZ_RECTELLIPSE_H

#include "ezcontainer.h"
#include "ezhandle.h"
#include <libgnomeui/gnome-canvas-rect-ellipse.h>

BEGIN_GNOME_DECLS

#define EZ_TYPE_RE            (ez_re_get_type ())
#define EZ_RE(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_RE, EZRE))
#define EZ_RE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_RE, EZREClass))
#define EZ_IS_RE(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_RE))
#define EZ_IS_RE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_RE))

typedef struct _EZRE EZRE;
typedef struct _EZREClass EZREClass;

struct _EZRE {
  EZContainer container;

  GnomeCanvasRE* shape;

  double coords[4];

  EZHandle** handles;
};

struct _EZREClass {
  EZContainerClass parent_class;

  /* Signals */

  /* Class functions */
  void (* create_shape) (EZRE* ezre);
  
};

/* Standard Gtk function */

GtkType   ez_re_get_type (void);

END_GNOME_DECLS

#endif

