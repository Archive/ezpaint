/* Handle group type for GnomeCanvas widget
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>

#include <libgnomeui/gnome-canvas-util.h> /* FIXME */

#include "gnome-canvas-handle-group.h"

#include <math.h>
#include <time.h>

#include <gtk/gtksignal.h>

#include "gnome-canvas-private-group.h"

static void gnome_canvas_handle_group_class_init (GnomeCanvasHandleGroupClass *class);
static void gnome_canvas_handle_group_init       (GnomeCanvasHandleGroup      *handle);
static void gnome_canvas_handle_group_destroy    (GtkObject            *object);
static void gnome_canvas_handle_group_set_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);
static void gnome_canvas_handle_group_get_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);

static void gnome_canvas_handle_group_sync_item  (GnomeCanvasHandleBox* box,
						  GtkAnchorType which_handle);
static void gnome_canvas_handle_group_create_item  (GnomeCanvasHandleBox* box);

static void gnome_canvas_handle_group_reconfigure(GnomeCanvasItem* item);

enum {
  LAST_SIGNAL
};


static GnomeCanvasHandleBoxClass *parent_class;

/* static gint handle_group_signals[LAST_SIGNAL] = { 0 };*/

GtkType
gnome_canvas_handle_group_get_type (void)
{
  static GtkType handle_group_type = 0;

  if (!handle_group_type) {
    GtkTypeInfo handle_group_info = {
      "GnomeCanvasHandleGroup",
      sizeof (GnomeCanvasHandleGroup),
      sizeof (GnomeCanvasHandleGroupClass),
      (GtkClassInitFunc) gnome_canvas_handle_group_class_init,
      (GtkObjectInitFunc) gnome_canvas_handle_group_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    handle_group_type = gtk_type_unique (gnome_canvas_handle_box_get_type (), 
					 &handle_group_info);
  }

  return handle_group_type;
}

static void
gnome_canvas_handle_group_class_init (GnomeCanvasHandleGroupClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  GnomeCanvasHandleBoxClass *handle_box_class;
  GnomeCanvasHandledClass *handled_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  handle_box_class = (GnomeCanvasHandleBoxClass*) klass;

  parent_class = gtk_type_class (gnome_canvas_handle_box_get_type ());

  item_class->reconfigure = gnome_canvas_handle_group_reconfigure;

  handle_box_class->create_item = gnome_canvas_handle_group_create_item;
  handle_box_class->sync_item   = gnome_canvas_handle_group_sync_item;

  object_class->destroy = gnome_canvas_handle_group_destroy;
  object_class->set_arg = gnome_canvas_handle_group_set_arg;
  object_class->get_arg = gnome_canvas_handle_group_get_arg;
}

static void
gnome_canvas_handle_group_init (GnomeCanvasHandleGroup *handle_group)
{

}

static void
gnome_canvas_handle_group_destroy (GtkObject *object)
{
  GnomeCanvasPrivateGroup *group;
  GSList* tmp;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_HANDLE_GROUP (object));

  group = GNOME_CANVAS_PRIVATE_GROUP (GNOME_CANVAS_HANDLE_BOX(object)->item);

  tmp = group->children;
  
  while (tmp) {
    GnomeCanvasHandled* child = GNOME_CANVAS_HANDLED(tmp->data);
    
    child->handle_group = NULL;
    
    tmp = g_slist_next(tmp);
  }        
  
  g_slist_free(group->children);

  group->children = NULL;

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_handle_group_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasHandleGroup *handle_group;

  item = GNOME_CANVAS_ITEM (object);
  handle_group = GNOME_CANVAS_HANDLE_GROUP (object);

  switch (arg_id) {

  default:
    g_warning("GnomeCanvasHandleGroup got an unknown arg type.");
    break;
  }
}

static void
gnome_canvas_handle_group_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasHandleGroup *handle_group;

  handle_group = GNOME_CANVAS_HANDLE_GROUP (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void 
gnome_canvas_handle_group_sync_item  (GnomeCanvasHandleBox* box,
				      GtkAnchorType which_handle)
{
  GnomeCanvasPrivateGroup* group = GNOME_CANVAS_PRIVATE_GROUP (box->item);
  double x1, y1, x2, y2;
  double newx, newy;
  double oldx, oldy;
  double xratio, yratio;
  GSList* tmp;

  x1 = MIN(box->coords[0], box->coords[2]);
  x2 = MAX(box->coords[0], box->coords[2]);
  y1 = MIN(box->coords[1], box->coords[3]);
  y2 = MAX(box->coords[1], box->coords[3]);

  oldx = (group->x2 - group->x1);
  oldy = (group->y2 - group->y1);
  newx = (x2 - x1);
  newy = (y2 - y1);

  if (oldx == 0.0) xratio = 0.0;
  else xratio = newx/oldx;
  if (oldy == 0.0) yratio = 0.0;
  else yratio = newy/oldy;

  

  tmp = group->children;
  
  while (tmp) {
    double ocx1, ocx2, ocy1, ocy2;
    double cx1, cx2, cy1, cy2;
    double oldwidth, oldheight;
    double old_x_offset, old_y_offset;

    GnomeCanvasHandled* child = GNOME_CANVAS_HANDLED(tmp->data);

    gnome_canvas_handled_size (child, &ocx1, &ocy1, &ocx2, &ocy2);

    oldheight    = ocy2 - ocy1;
    oldwidth     = ocx2 - ocx1;

    switch (which_handle) {
    case GTK_ANCHOR_WEST:
    case GTK_ANCHOR_NW:
    case GTK_ANCHOR_SW:
      old_x_offset = ocx1 - group->x1;
      cx1 = x1 + (old_x_offset * xratio);
      cx2 = cx1 + (oldwidth * xratio);
      break;
    case GTK_ANCHOR_EAST:
    case GTK_ANCHOR_SE:
    case GTK_ANCHOR_NE:
      old_x_offset = group->x2 - ocx2;
      cx2 = x2 - (old_x_offset * xratio);
      cx1 = cx2 - (oldwidth * xratio);
      break;
    default:
      /* Could just as well do this relative to x2 */
      old_x_offset = ocx1 - group->x1;
      cx1 = x1 + old_x_offset;
      cx2 = cx1 + oldwidth;
      break;
    }
    
    switch (which_handle) {
    case GTK_ANCHOR_NORTH:
    case GTK_ANCHOR_NW:
    case GTK_ANCHOR_NE:
      old_y_offset = ocy1 - group->y1;
      cy1 = y1 + (old_y_offset * yratio);
      cy2 = cy1 + (oldheight * yratio);
      break;
    case GTK_ANCHOR_SOUTH:
    case GTK_ANCHOR_SW:
    case GTK_ANCHOR_SE:
      old_y_offset = group->y2 - ocy2;
      cy2 = y2 - (old_y_offset * yratio);
      cy1 = cy2 - (oldheight * yratio);
      break;
    default:
      old_y_offset = ocy1 - group->y1;
      cy1 = y1 + old_y_offset;
      cy2 = cy1 + oldheight;
      break;
    }

    gnome_canvas_handled_scale(child, cx1, cy1, cx2, cy2);
    
    tmp = g_slist_next(tmp);
  }      

  group->x1 = x1;
  group->x2 = x2;
  group->y1 = y1;
  group->y2 = y2;
}

static void 
gnome_canvas_handle_group_create_item  (GnomeCanvasHandleBox* box)
{
  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_GROUP(box));

  box->item = gnome_canvas_item_new(GNOME_CANVAS_GROUP(box),
				    gnome_canvas_private_group_get_type(),
				    NULL);
}

void 
gnome_canvas_handle_group_add   (GnomeCanvasHandleGroup* group,
				 GnomeCanvasHandled*     child)
{
  GnomeCanvasHandleBox* handle_box;
  GnomeCanvasPrivateGroup* private_group;
  double* minx, *maxx, *miny, *maxy;
  double x1, y1, x2, y2;

  g_return_if_fail(group != NULL);
  g_return_if_fail(child != NULL);
  g_return_if_fail(GNOME_IS_CANVAS_HANDLED(child));
  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_GROUP(group));
  g_return_if_fail(GNOME_CANVAS_ITEM(group)->parent == GNOME_CANVAS_ITEM(child)->parent);

  handle_box = GNOME_CANVAS_HANDLE_BOX(group);
  private_group = GNOME_CANVAS_PRIVATE_GROUP(handle_box->item);

  g_return_if_fail(g_slist_find(private_group->children, child) == NULL);

  if (child->handle_group) {
    /* _remove, without the reconfigure. */
    private_group->children = g_slist_remove(private_group->children, child);
    
    child->handle_group = NULL;
  }

  gnome_canvas_handled_size(child,
			    &x1, &y1, &x2, &y2);
  
  if (private_group->children == NULL) {
    /* This is the first child, we just set to whatever the child is */
    private_group->x1 = handle_box->coords[0] = x1;
    private_group->y1 = handle_box->coords[1] = y1;
    private_group->x2 = handle_box->coords[2] = x2;
    private_group->y2 = handle_box->coords[3] = y2;
  }
  else {
    if (handle_box->coords[0] > handle_box->coords[2]) {
      minx = &handle_box->coords[2];
      maxx = &handle_box->coords[0];
    }
    else {
      minx = &handle_box->coords[0];
    maxx = &handle_box->coords[2];
    }
    
    if (handle_box->coords[1] > handle_box->coords[3]) {
      miny = &handle_box->coords[3];
      maxy = &handle_box->coords[1];
    }
    else {
      miny = &handle_box->coords[1];
      maxy = &handle_box->coords[3];
    }
    
    if (x1 < *minx) *minx = x1;
    if (y1 < *miny) *miny = y1;
    if (x2 > *maxx) *maxx = x2;
    if (y2 > *maxy) *maxy = y2;  

    /* Reset our "old" numbers, so we don't get confused 
       about scaling. */
    private_group->x1 = *minx;
    private_group->y1 = *miny;
    private_group->x2 = *maxx;
    private_group->y2 = *maxy;
  }


  /* parent the child */
  child->handle_group = group;
  private_group->children = g_slist_prepend(private_group->children, child);

  /* sync */
  gnome_canvas_handle_group_reconfigure(GNOME_CANVAS_ITEM(group));
}

void 
gnome_canvas_handle_group_remove(GnomeCanvasHandled*     child)
{
  GnomeCanvasHandleGroup* group = child->handle_group;
  GnomeCanvasPrivateGroup* private_group = 
    GNOME_CANVAS_PRIVATE_GROUP(GNOME_CANVAS_HANDLE_BOX(group)->item);

  g_return_if_fail(group != NULL);
  g_return_if_fail(child != NULL);
  g_return_if_fail(g_slist_find(private_group->children, child) != NULL);

  /* Keep the removal block in _add in sync */

  private_group->children = g_slist_remove(private_group->children, child);

  child->handle_group = NULL;

  gnome_canvas_handle_group_reconfigure(GNOME_CANVAS_ITEM(group));
}

static void 
gnome_canvas_handle_group_reconfigure(GnomeCanvasItem* item)
{
  GnomeCanvasHandleBox* handle_box;
  GnomeCanvasHandleGroup* group;

  g_return_if_fail(GNOME_IS_CANVAS_HANDLE_GROUP(item));

  handle_box = GNOME_CANVAS_HANDLE_BOX(item);
  group      = GNOME_CANVAS_HANDLE_GROUP(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure) (item);
}



void 
gnome_canvas_handle_group_set_open (GnomeCanvasHandleGroup* group, 
				    gboolean openness)
{
  GSList* tmp = 0;

  GnomeCanvasPrivateGroup* private_group = 
    GNOME_CANVAS_PRIVATE_GROUP(GNOME_CANVAS_HANDLE_BOX(group)->item);

  if (private_group->open == openness) return;

  if (private_group->open && !openness) {
    /* we're closing - unselect children, select ourselves if 
       any children were selected */
    gboolean yes = FALSE;
    tmp = private_group->children;
    while (tmp) {
      if (GNOME_CANVAS_HANDLED(tmp->data)->selected) yes = TRUE;

      gnome_canvas_handled_set_selected(GNOME_CANVAS_HANDLED(tmp->data), FALSE);
      tmp = g_slist_next(tmp);
    }
    if (yes) gnome_canvas_handled_set_selected(GNOME_CANVAS_HANDLED(private_group), TRUE);
  }
  else {
    /* we're opening - select all children if we were selected */
    if (GNOME_CANVAS_HANDLED(private_group)->selected) {
      tmp = private_group->children;
      while (tmp) {
	gnome_canvas_handled_set_selected(GNOME_CANVAS_HANDLED(tmp->data), TRUE);
	tmp = g_slist_next(tmp);
      }
    }
  }

  gnome_canvas_handle_group_reconfigure(GNOME_CANVAS_ITEM(group));
}
