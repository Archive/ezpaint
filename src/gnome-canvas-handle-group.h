/* Group of handled items, so you can scale them. Grouped items remain 
 *  in their original GnomeCanvasGroup, though.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLE_GROUP_H
#define GNOME_CANVAS_HANDLE_GROUP_H

#include <libgnome/gnome-defs.h>

#include "gnome-canvas-handle-box.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_HANDLE_GROUP            (gnome_canvas_handle_group_get_type ())
#define GNOME_CANVAS_HANDLE_GROUP(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLE_GROUP, GnomeCanvasHandleGroup))
#define GNOME_CANVAS_HANDLE_GROUP_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLE_GROUP, GnomeCanvasHandleGroupClass))
#define GNOME_IS_CANVAS_HANDLE_GROUP(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLE_GROUP))
#define GNOME_IS_CANVAS_HANDLE_GROUP_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLE_GROUP))

typedef struct _GnomeCanvasHandleGroup GnomeCanvasHandleGroup;
typedef struct _GnomeCanvasHandleGroupClass GnomeCanvasHandleGroupClass;

struct _GnomeCanvasHandleGroup {
  GnomeCanvasHandleBox handle_box;

};

struct _GnomeCanvasHandleGroupClass {
  GnomeCanvasHandleBoxClass parent_class;
};

/* Standard Gtk function */
GtkType gnome_canvas_handle_group_get_type (void);

/* If the child is already in another handle group, it will be
   moved. Its CanvasGroup is unaffected though. */
void gnome_canvas_handle_group_add      (GnomeCanvasHandleGroup* group,
				         GnomeCanvasHandled*     child);

void gnome_canvas_handle_group_remove   (GnomeCanvasHandled*     child);

void gnome_canvas_handle_group_set_open (GnomeCanvasHandleGroup* group, 
					 gboolean openness);

/* GnomeCanvasHandleGroup is very fragile and sensitive to random conditions.
   For example, it has to be above all items it contains in the canvas
   Z order. This stuff should be fixed. */

/* Well, it also has to be in the same CanvasGroup as the items it contains. */

END_GNOME_DECLS

#endif

