/* Internal object used in HandleGroup
 * 
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "gnome-canvas-private-group.h"

#include <config.h>

#include <libgnomeui/gnome-canvas-util.h>

#include "gnome-canvas-private-group.h"

#include <math.h>

#include <gtk/gtksignal.h>

static void gnome_canvas_private_group_class_init (GnomeCanvasPrivateGroupClass *class);
static void gnome_canvas_private_group_init       (GnomeCanvasPrivateGroup      *private);
static void gnome_canvas_private_group_destroy    (GtkObject            *object);
static void gnome_canvas_private_group_set_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);
static void gnome_canvas_private_group_get_arg    (GtkObject            *object,
						 GtkArg               *arg,
						 guint                 arg_id);

static void gnome_canvas_private_group_reconfigure(GnomeCanvasItem* item);

static double gnome_canvas_private_group_point (GnomeCanvasItem *item, 
					       double x, double y, int cx, int cy,
					       GnomeCanvasItem **actual_item);

enum {
  LAST_SIGNAL
};

static GnomeCanvasItemClass *parent_class;

/* static gint private_group_signals[LAST_SIGNAL] = { 0 };*/

GtkType
gnome_canvas_private_group_get_type (void)
{
  static GtkType private_group_type = 0;

  if (!private_group_type) {
    GtkTypeInfo private_group_info = {
      "GnomeCanvasPrivateGroup",
      sizeof (GnomeCanvasPrivateGroup),
      sizeof (GnomeCanvasPrivateGroupClass),
      (GtkClassInitFunc) gnome_canvas_private_group_class_init,
      (GtkObjectInitFunc) gnome_canvas_private_group_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    private_group_type = gtk_type_unique (gnome_canvas_item_get_type (), 
					  &private_group_info);
  }

  return private_group_type;
}

static void
gnome_canvas_private_group_class_init (GnomeCanvasPrivateGroupClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  GnomeCanvasHandledClass *handled_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = gtk_type_class (gnome_canvas_item_get_type ());

  item_class->reconfigure = gnome_canvas_private_group_reconfigure;
  item_class->point       = gnome_canvas_private_group_point;

  object_class->destroy = gnome_canvas_private_group_destroy;
  object_class->set_arg = gnome_canvas_private_group_set_arg;
  object_class->get_arg = gnome_canvas_private_group_get_arg;
}

static void
gnome_canvas_private_group_init (GnomeCanvasPrivateGroup *private_group)
{
  private_group->children = NULL;
  
  private_group->open = FALSE;

  private_group->x1 = private_group->y1 = 
    private_group->x2 = private_group->y2 = 0.0;
}

static void
gnome_canvas_private_group_destroy (GtkObject *object)
{
  GSList* tmp;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_PRIVATE_GROUP (object));

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_private_group_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasPrivateGroup *private_group;

  item = GNOME_CANVAS_ITEM (object);
  private_group = GNOME_CANVAS_PRIVATE_GROUP (object);

  switch (arg_id) {

  default:
    g_warning("GnomeCanvasPrivateGroup got an unknown arg type.");
    break;
  }
}

static void
gnome_canvas_private_group_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasPrivateGroup *private_group;

  private_group = GNOME_CANVAS_PRIVATE_GROUP (object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}


static void
recalc_bounds (GnomeCanvasPrivateGroup * group)
{
  GnomeCanvasItem *item;
  double x1, y1, x2, y2;
  
  item = GNOME_CANVAS_ITEM (group);
  
  x1 = group->x1;
  y1 = group->y1;
  x2 = group->x2;
  y2 = group->y2;
  
  gnome_canvas_item_i2w (item, &x1, &y1);
  gnome_canvas_item_i2w (item, &x2, &y2);
  gnome_canvas_w2c (item->canvas, x1, y1, &item->x1, &item->y1);
  gnome_canvas_w2c (item->canvas, x2, y2, &item->x2, &item->y2);
  
  /* Some safety fudging */
  
  item->x1 -= 2;
  item->y1 -= 2;
  item->x2 += 2;
  item->y2 += 2;
  
  gnome_canvas_group_child_bounds (GNOME_CANVAS_GROUP (item->parent), item);
}


static void 
gnome_canvas_private_group_reconfigure(GnomeCanvasItem* item)
{
  GnomeCanvasPrivateGroup* group;

  g_return_if_fail(GNOME_IS_CANVAS_PRIVATE_GROUP(item));
  
  group      = GNOME_CANVAS_PRIVATE_GROUP(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure) (item);

  recalc_bounds(group);
}

/* Point implementation stolen mostly from GnomeCanvasGroup */

/* point is overridden because we want to get events before the 
   items below us do, iff the group is closed. */

static double 
gnome_canvas_private_group_point (GnomeCanvasItem *item, 
				  double x, double y, int cx, int cy,
				  GnomeCanvasItem **actual_item)
{
  GnomeCanvasPrivateGroup *group;
  GSList *list;
  GnomeCanvasItem *child;
  GnomeCanvasItem * dummy;
  int x1, y1, x2, y2;
  double dist, best;
  int has_point;
  
  group = GNOME_CANVAS_PRIVATE_GROUP (item);

  if (group->open) { 
    return 100000000000000.0; /* Nope, that point is NOT near us. */
  }
  
  x1 = cx - item->canvas->close_enough;
  y1 = cy - item->canvas->close_enough;
  x2 = cx + item->canvas->close_enough;
  y2 = cy + item->canvas->close_enough;
  
  best = 0.0;
  *actual_item = item; /* Unlike GnomeCanvasGroup, we are the actual item. 
			  We want to edit the group, not the children. */
     
  for (list = group->children; list; list = list->next) {
    child = GNOME_CANVAS_ITEM(list->data);
    
    if ((child->x1 > x2) || (child->y1 > y2) || (child->x2 < x1) || (child->y2 < y1))
      continue;
    
    if ((child->object.flags & GNOME_CANVAS_ITEM_VISIBLE)
	&& GNOME_CANVAS_ITEM_CLASS (child->object.klass)->point) {
      dist = 
	(* GNOME_CANVAS_ITEM_CLASS (child->object.klass)->point) (child, x, y, 
								  cx, cy, &dummy);
    }

    if (dist < best) best = dist;
  }
  
  return MAX(best - 1.0, 0.0); // fudge to be sure we're getting the event
}




