/* Base class for EZ containers.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezcontainer.h"

#include <gtk/gtksignal.h>

#include <config.h>

/* For internal use */
#ifdef GNOME_ENABLE_DEBUG
#define ez_container_invariants(x) G_STMT_START { \
  EZContainer* _ezc = EZ_CONTAINER(x);  \
  } G_STMT_END
#else 
#define ez_container_invariants(x)
#endif

static void ez_container_class_init (EZContainerClass *class);
static void ez_container_init       (EZContainer      *ezi);
static void ez_container_destroy    (GtkObject            *object);
static void ez_container_set_arg    (GtkObject            *object,
                                     GtkArg               *arg,
                                     guint                 arg_id);
static void ez_container_get_arg    (GtkObject            *object,
                                     GtkArg               *arg,
                                     guint                 arg_id);

static void ez_container_start_editing (EZItem* ezi);

static void ez_container_stop_editing  (EZItem* ezi);

static void ez_container_start_moving  (EZItem* ezi);

static void ez_container_stop_moving   (EZItem* ezi);

static void ez_container_start_sizing  (EZItem* ezi);

static void ez_container_stop_sizing   (EZItem* ezi);

static void ez_container_select        (EZItem* ezi);

static void ez_container_unselect      (EZItem* ezi);

static void ez_container_move_resize   (EZItem* ezi,
                                        EZAnchorType which_handle,
                                        double x1, double y1, 
                                        double x2, double y2);

static gboolean ez_container_pass_event    (EZItem* ezi, EZItem* origin, 
                                            GdkEvent* event);

static void ez_container_real_add      (EZContainer* ezc, EZItem* child);
static void ez_container_real_remove   (EZContainer* ezc, EZItem* child);

enum {
  ARG_0
};


enum {
  ADD,
  REMOVE,
  
  LAST_SIGNAL
};

static EZItemClass *parent_class;

static gint item_signals[LAST_SIGNAL] = { 0 };

GtkType
ez_container_get_type (void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "EZContainer",
      sizeof (EZContainer),
      sizeof (EZContainerClass),
      (GtkClassInitFunc) ez_container_class_init,
      (GtkObjectInitFunc) ez_container_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (ez_item_get_type (), &item_info);
  }

  return item_type;
}

static void
ez_container_class_init (EZContainerClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  EZItemClass* ezitem_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  ezitem_class = (EZItemClass* ) klass;

  parent_class = gtk_type_class (ez_item_get_type ());

  item_signals[ADD] = 
    gtk_signal_new("add",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZContainerClass, add),
                   gtk_marshal_NONE__POINTER,
                   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  item_signals[REMOVE] = 
    gtk_signal_new("remove",
                   GTK_RUN_LAST,
                   object_class->type,
                   GTK_SIGNAL_OFFSET (EZContainerClass, remove),
                   gtk_marshal_NONE__POINTER,
                   GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, item_signals, 
				LAST_SIGNAL);

  klass->add = ez_container_real_add;
  klass->remove = ez_container_real_remove;

  ezitem_class->start_editing = ez_container_start_editing;
  ezitem_class->stop_editing  = ez_container_stop_editing;

  ezitem_class->start_moving  = ez_container_start_moving;
  ezitem_class->stop_moving   = ez_container_stop_moving;

  ezitem_class->start_sizing  = ez_container_start_sizing;
  ezitem_class->stop_sizing   = ez_container_stop_sizing;

  ezitem_class->select        = ez_container_select;
  ezitem_class->unselect      = ez_container_unselect;

  ezitem_class->move_resize   = ez_container_move_resize;

  ezitem_class->pass_event    = ez_container_pass_event;

  object_class->destroy = ez_container_destroy;
  object_class->set_arg = ez_container_set_arg;
  object_class->get_arg = ez_container_get_arg;
}

static void
ez_container_init (EZContainer *ezc)
{
  ezc->children = NULL;
  ezc->editing_child = NULL;
}

static void
ez_container_destroy (GtkObject *object)
{
  EZContainer *ezc;

  g_return_if_fail (object != NULL);
  g_return_if_fail (EZ_IS_CONTAINER(object));

  ezc = EZ_CONTAINER (object);

  ez_debug("Destroy EZContainer %p", object);      

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ez_container_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  EZContainer *ezc;

  item = GNOME_CANVAS_ITEM (object);
  ezc = EZ_CONTAINER (object);

  switch (arg_id) {

  default:
    g_warning("EZContainer got an unknown arg type.");
    break;
  }

#if 0
  gnome_canvas_container_request_update(item);
#endif
}


static void
ez_container_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  EZContainer* ezc;

  ezc = EZ_CONTAINER(object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

/************ End Canvas Item Functions ******************/


/* "Real" variants (our default handlers) */

static void
ez_container_start_editing (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_debug("Start editing EZContainer %p", ezc);      

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->start_editing)
    (* parent_class->start_editing)(ezi);
}

static void
ez_container_stop_editing  (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  ez_debug("Stop editing EZContainer %p", ezc);      

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->stop_editing)
    (* parent_class->stop_editing)(ezi);
}

static void
ez_container_start_moving  (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->start_moving)
    (* parent_class->start_moving)(ezi);
}

static void
ez_container_stop_moving   (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->stop_moving)
    (* parent_class->stop_moving)(ezi);
}

static void
ez_container_start_sizing  (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->start_sizing)
    (* parent_class->start_sizing)(ezi);
}

static void
ez_container_stop_sizing   (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->stop_sizing)
    (* parent_class->stop_sizing)(ezi);
}

static void
ez_container_select        (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->select)
    (* parent_class->select)(ezi);
}

static void
ez_container_unselect      (EZItem* ezi)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));

  ez_container_invariants(ezi);

  ez_container_invariants(ezi);

  if (parent_class->unselect)
    (* parent_class->unselect)(ezi);
}

static void
child_coords(EZContainer* ezc, EZItem* child, double* x, double* y)
{
  g_return_if_fail(ezc != NULL);
  g_return_if_fail(child != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezc));
  g_return_if_fail(EZ_IS_ITEM(child));

  gnome_canvas_item_i2w(GNOME_CANVAS_ITEM(ezc), x, y);
  gnome_canvas_item_w2i(GNOME_CANVAS_ITEM(child), x, y);
}

static void
ez_container_move_resize   (EZItem* ezi, 
                            EZAnchorType which_handle,
                            double x1, double y1, 
                            double x2, double y2)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezi));
  g_return_if_fail(x2 >= x1);
  g_return_if_fail(y2 >= y1);

  if (ezc->children != NULL)
    {
      GSList* tmp;
      double ox1, ox2, oy1, oy2; /* Our old coords */
      double newx, newy;
      double oldx, oldy;
      double xratio, yratio;

      ez_item_get_size(ezi, &ox1, &oy1, &ox2, &oy2);
      
      oldx = (ox2 - ox1);
      oldy = (oy2 - oy1);
      newx = (x2 - x1);
      newy = (y2 - y1);

      ez_debug("Container: %g,%g %g,%g  %gx%g\n"
               "Will be  : %g,%g %g,%g  %gx%g",
               ox1, oy1, ox2, oy2, oldx, oldy,
               x1, y1, x2, y2, newx, newy);
      
      if (oldx == 0.0) xratio = 1.0;
      else xratio = newx/oldx;
      if (oldy == 0.0) yratio = 1.0;
      else yratio = newy/oldy;

#if 0
      /* Need to get everything in the child coordinate system */
      /* FIXME everything still works without this I think */
      if (ezc->children != NULL)
        {
          child_coords(ezc, EZ_ITEM(ezc->children->data),
                       &ox1, &oy1);
          child_coords(ezc, EZ_ITEM(ezc->children->data),
                       &ox2, &oy2);
          child_coords(ezc, EZ_ITEM(ezc->children->data),
                       &x1, &y1);
          child_coords(ezc, EZ_ITEM(ezc->children->data),
                       &x2, &y2);

          ez_debug("Container (child-relative): %g,%g %g,%g\n"
                   "Will be                   : %g,%g %g,%g",
                   ox1, oy1, ox2, oy2,
                   x1, y1, x2, y2);

        }
#endif

      tmp = ezc->children;

      while (tmp)
        {
          EZItem* child = EZ_ITEM(tmp->data);
          double ocx1, ocx2, ocy1, ocy2; /* Child old coords */
          double cx1, cx2, cy1, cy2;
          double oldwidth, oldheight;
          double old_x_offset, old_y_offset;

          ez_item_get_size(child, &ocx1, &ocy1, &ocx2, &ocy2);

          oldheight    = ocy2 - ocy1;
          oldwidth     = ocx2 - ocx1;

          /* The width/height of the parent is also the 
             x2/y2 of the parent in child coordinates */

          switch (which_handle) {
          case EZ_ANCHOR_WEST:
          case EZ_ANCHOR_NW:
          case EZ_ANCHOR_SW:
            old_x_offset = ocx1;
            cx1 = (old_x_offset * xratio);
            cx2 = cx1 + (oldwidth * xratio);
            break;
          case EZ_ANCHOR_EAST:
          case EZ_ANCHOR_SE:
          case EZ_ANCHOR_NE:
            old_x_offset = oldx - ocx2;
            cx2 = newx - (old_x_offset * xratio);
            cx1 = cx2 - (oldwidth * xratio);
            break;
          default:
            /* Could just as well do this relative to x2 */
            old_x_offset = ocx1;
            cx1 = old_x_offset;
            cx2 = cx1 + oldwidth;
            break;
          }
    
          switch (which_handle) {
          case EZ_ANCHOR_NORTH:
          case EZ_ANCHOR_NW:
          case EZ_ANCHOR_NE:
            old_y_offset = ocy1;
            cy1 = (old_y_offset * yratio);
            cy2 = cy1 + (oldheight * yratio);
            break;
          case EZ_ANCHOR_SOUTH:
          case EZ_ANCHOR_SW:
          case EZ_ANCHOR_SE:
            old_y_offset = oldy - ocy2;
            cy2 = newy - (old_y_offset * yratio);
            cy1 = cy2 - (oldheight * yratio);
            break;
          default:
            old_y_offset = ocy1;
            cy1 = old_y_offset;
            cy2 = cy1 + oldheight;
            break;
          }

          ez_debug("Sizing child %p with %s\n (%g,%g) (%g,%g) %gx%g\n [ratios %g | %g] [was %g,%g %g,%g  %gx%g] [offsets %g %g]", 
                   child, ez_anchor_name(which_handle), 
                   cx1, cy1, cx2, cy2, cx2 - cx1, cy2 - cy1, 
                   xratio, yratio, 
                   ocx1, ocy1, ocx2, ocy2, oldwidth, oldheight,
                   old_x_offset, old_y_offset);
          
          ez_item_move_resize(child, which_handle, cx1, cy1, cx2, cy2);

          tmp = g_slist_next(tmp);
        }
    }
  

  if (parent_class->move_resize)
    (* parent_class->move_resize)(ezi, which_handle, x1, y1, x2, y2);
}

static gboolean 
ez_container_pass_event    (EZItem* ezi, EZItem* origin, GdkEvent* event)
{
  EZContainer* ezc = EZ_CONTAINER(ezi);
  gboolean handled = FALSE;
  EZItem* immed = NULL;

  g_return_val_if_fail(ezi != NULL, FALSE);
  g_return_val_if_fail(EZ_IS_CONTAINER(ezi), FALSE);
  g_return_val_if_fail(origin != NULL, FALSE);
  g_return_val_if_fail(EZ_IS_ITEM(origin), FALSE);
  g_return_val_if_fail(!origin->floating, FALSE);
  g_return_val_if_fail(!ezi->floating, FALSE);

  /*  ez_debug("Event passed to EZContainer %p", ezc);      */
  
  /* If we were the origin, we go on to handle the event. 
     Otherwise we consider passing it to a child. */
  if (origin != ezi) 
    {
      /* Determine which of our immediate children the origin 
         is or is a child of; if none, we should not have 
         gotten this event */
      immed = origin;
      while (immed && immed->parent != ezc)
        {
          immed = (EZItem*)immed->parent;
        }
      
      g_return_val_if_fail((immed && immed->parent == ezc), FALSE);
      
      /* The child being edited got the event, so bail */
      if (ezc->editing_child == immed)
        {
          return ez_item_pass_event(immed, origin, event);
        }

      /* Otherwise we handle the event ourselves. */
    }      

  if (parent_class->pass_event)
    handled = (* parent_class->pass_event)(ezi, origin, event);

  if (handled)
    return TRUE;
  else
    {
      switch (event->type){
        
      case GDK_ENTER_NOTIFY:
        break;
        
      case GDK_LEAVE_NOTIFY:
        break;
        
      case GDK_BUTTON_PRESS:
        {
          if (event->button.button != 1) return FALSE;

          /* We won't get here if the editing child was the clicked
             child. */
          if (ezc->editing_child != NULL)
            {
              /* An immediate child other than the current
                 editing one was clicked */
              ez_item_stop_editing(ezc->editing_child);
              ezc->editing_child = NULL;
              return TRUE;
            }
          else
            {
              g_assert(ezc->editing_child == NULL); /* To catch cut-and-paste screwups */
              /* Not editing a child, so start editing this one */
              ezc->editing_child = immed;
              ez_item_start_editing(ezc->editing_child);
              /*              ez_item_start_moving(ezc->editing_child); */
              return TRUE;
            }

          return FALSE;
        }
        break;

      default:
        break;
      }
    }

  return FALSE;
}

/***** Container functions *********/

static void           
ez_item_set_parent    (EZItem* ezi, EZContainer* ezc);

static void 
ez_container_real_add      (EZContainer* ezc, EZItem* child)
{
  g_return_if_fail(ezc != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezc));
  g_return_if_fail(child != NULL);
  g_return_if_fail(EZ_IS_ITEM(child));
  g_return_if_fail(child->parent == NULL);
  g_return_if_fail (g_slist_find(ezc->children, child) == NULL);

  ez_debug("Adding EZItem %p to EZContainer %p", child, ezc);      

  ezc->children = g_slist_prepend(ezc->children, child);

  gtk_object_ref(GTK_OBJECT(child));

  gnome_canvas_item_reparent(GNOME_CANVAS_ITEM(child), 
                             GNOME_CANVAS_GROUP(ezc));

  ez_item_set_parent(child, ezc);
}

static void 
ez_container_real_remove   (EZContainer* ezc, EZItem* child)
{
  g_return_if_fail(ezc != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezc));
  g_return_if_fail(child != NULL);
  g_return_if_fail(EZ_IS_ITEM(child));
  g_return_if_fail(child->parent != NULL);
  g_return_if_fail (g_slist_find(ezc->children, child) != NULL);

  ez_debug("Removing EZItem %p from EZContainer %p", child, ezc);      

  if (ezc->editing_child == child) ezc->editing_child = NULL;

  ezc->children = g_slist_remove(ezc->children, child);

  gtk_object_unref(GTK_OBJECT(child));

  ez_item_set_parent(child, NULL);
}


void           
ez_container_add      (EZContainer* ezc, EZItem* child)
{
  g_return_if_fail(ezc != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezc));
  g_return_if_fail(child != NULL);
  g_return_if_fail(EZ_IS_ITEM(child));
  g_return_if_fail(child->parent == NULL);

  gtk_signal_emit(GTK_OBJECT(ezc), item_signals[ADD], child);
}

void           
ez_container_remove   (EZContainer* ezc, EZItem* child)
{
  g_return_if_fail(ezc != NULL);
  g_return_if_fail(EZ_IS_CONTAINER(ezc));
  g_return_if_fail(child != NULL);
  g_return_if_fail(EZ_IS_ITEM(child));
  g_return_if_fail(child->parent != NULL);
  g_return_if_fail(child->parent == ezc);

  gtk_signal_emit(GTK_OBJECT(ezc), item_signals[REMOVE], child);
}

/* This sets the parent of the item, it doesn't add to or remove from
   the container. */
static void           
ez_item_set_parent    (EZItem* ezi, EZContainer* ezc)
{
  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_ITEM(ezi));

  if (ezc == NULL)
    {
      g_return_if_fail(ezi->parent != NULL);
      g_return_if_fail(!ezi->toplevel);

      gnome_canvas_item_hide(GNOME_CANVAS_ITEM(ezi));
      gnome_canvas_item_reparent(GNOME_CANVAS_ITEM(ezi), GNOME_CANVAS_GROUP(ezi->paste));
      ezi->parent = NULL;
      ezi->floating = TRUE;
    }
  else 
    {
      g_return_if_fail(EZ_IS_CONTAINER(ezc));
      g_return_if_fail(ezi->parent == NULL);

      if (ezi->toplevel)
        {
          /* This will make it floating. */
          ez_item_set_toplevel (ezi, FALSE);
        }

      g_return_if_fail(ezi->floating);

      if (ezi->floating)
        {
          ezi->floating = FALSE;

          gnome_canvas_item_show(GNOME_CANVAS_ITEM(ezi));
        }

      ezi->parent = ezc;
    }
}


