/* Selection made up of handled items.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <config.h>

#include "gnome-canvas-selection.h"

#include <gtk/gtksignal.h>

static void gnome_canvas_selection_class_init (GnomeCanvasSelectionClass *class);
static void gnome_canvas_selection_init       (GnomeCanvasSelection      *selection);
static void gnome_canvas_selection_destroy    (GtkObject            *object);
static void gnome_canvas_selection_set_arg    (GtkObject            *object,
					    GtkArg               *arg,
					    guint                 arg_id);
static void gnome_canvas_selection_get_arg    (GtkObject            *object,
					    GtkArg               *arg,
					    guint                 arg_id);
static void gnome_canvas_selection_realize (GnomeCanvasItem *item);

static void gnome_canvas_selection_unrealize   (GnomeCanvasItem *item);

static void gnome_canvas_selection_reconfigure (GnomeCanvasItem *item);


void gnome_canvas_selection_create   (GnomeCanvasHandled* handled, GdkEventButton* event);

enum {
  ARG_0
};


enum {
  LAST_SIGNAL
};

static GnomeCanvasHandledClass *parent_class;

/*static gint selection_signals[LAST_SIGNAL] = { 0 }; */

GtkType
gnome_canvas_selection_get_type (void)
{
  static GtkType selection_type = 0;

  if (!selection_type) {
    GtkTypeInfo selection_info = {
      "GnomeCanvasSelection",
      sizeof (GnomeCanvasSelection),
      sizeof (GnomeCanvasSelectionClass),
      (GtkClassInitFunc) gnome_canvas_selection_class_init,
      (GtkObjectInitFunc) gnome_canvas_selection_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    selection_type = gtk_type_unique (gnome_canvas_handled_get_type (), &selection_info);
  }

  return selection_type;
}

static void
gnome_canvas_selection_class_init (GnomeCanvasSelectionClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  GnomeCanvasHandledClass *handled_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  handled_class = (GnomeCanvasHandledClass *) klass;

  parent_class = gtk_type_class (gnome_canvas_handled_get_type ());

  handled_class->create   = gnome_canvas_selection_create;

  item_class->realize     = gnome_canvas_selection_realize;
  item_class->reconfigure = gnome_canvas_selection_reconfigure;
  item_class->unrealize   = gnome_canvas_selection_unrealize;

  object_class->destroy = gnome_canvas_selection_destroy;
  object_class->set_arg = gnome_canvas_selection_set_arg;
  object_class->get_arg = gnome_canvas_selection_get_arg;
}

static void
gnome_canvas_selection_init (GnomeCanvasSelection *selection)
{

}

static void
gnome_canvas_selection_destroy (GtkObject *object)
{
  GnomeCanvasSelection *selection;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_SELECTION (object));

  selection = GNOME_CANVAS_SELECTION (object);

  /* Someday useful? */

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_selection_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasSelection *selection;

  item = GNOME_CANVAS_ITEM (object);
  selection = GNOME_CANVAS_SELECTION (object);

  /* Someday useful? */

  switch (arg_id) {

  default:
    g_warning("GnomeCanvasSelection got an unknown arg type.");
    break;
  }
}


static void
gnome_canvas_selection_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasSelection *selection;

  selection = GNOME_CANVAS_SELECTION (object);

  /* Someday useful? */

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void
gnome_canvas_selection_realize (GnomeCanvasItem *item)
{
  GnomeCanvasSelection* selection = GNOME_CANVAS_SELECTION(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->realize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->realize) (item);

    /* Someday useful? */

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure) (item);
}


static void 
gnome_canvas_selection_unrealize   (GnomeCanvasItem *item)
{
  GnomeCanvasSelection* selection = GNOME_CANVAS_SELECTION(item);

  /* Someday useful? */

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize) (item);
}


static void 
gnome_canvas_selection_reconfigure (GnomeCanvasItem *item)
{

  /* Someday useful? */

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->reconfigure) (item);
}

void
gnome_canvas_selection_create   (GnomeCanvasHandled* handled, GdkEventButton* event)
{
  g_warning("create for GnomeCanvasSelection isn't implemented");
}


