/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZRect: editable rectangle
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_RECT_H
#define EZ_RECT_H

#include "ezrectellipse.h"

BEGIN_GNOME_DECLS

#define EZ_TYPE_RECT            (ez_rect_get_type ())
#define EZ_RECT(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_RECT, EZRect))
#define EZ_RECT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_RECT, EZRectClass))
#define EZ_IS_RECT(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_RECT))
#define EZ_IS_RECT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_RECT))

typedef struct _EZRect EZRect;
typedef struct _EZRectClass EZRectClass;

struct _EZRect {
  EZRE re;

};

struct _EZRectClass {
  EZREClass parent_class;
  
};

/* Standard Gtk function */

GtkType   ez_rect_get_type (void);

EZItem*   ez_rect_new (EZPaste* paste);


END_GNOME_DECLS

#endif

