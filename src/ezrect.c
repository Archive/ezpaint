/* Editable Rectangle item
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezrect.h"

#include <gtk/gtksignal.h>

#include <config.h>

static void ez_rect_class_init (EZRectClass *class);
static void ez_rect_init       (EZRect      *ezrect);
static void ez_rect_destroy    (GtkObject            *object);
static void ez_rect_set_arg    (GtkObject            *object,
                                GtkArg               *arg,
                                guint                 arg_id);
static void ez_rect_get_arg    (GtkObject            *object,
                                GtkArg               *arg,
                                guint                 arg_id);

static void ez_rect_construct (EZItem* ezi, EZPaste* paste);

static void ez_rect_create_shape (EZRE* ezre);

enum {
  ARG_0
};


enum {
  
  LAST_SIGNAL
};

static EZREClass *parent_class;

static gint item_signals[LAST_SIGNAL] = { 0 };

GtkType
ez_rect_get_type (void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "EZRect",
      sizeof (EZRect),
      sizeof (EZRectClass),
      (GtkClassInitFunc) ez_rect_class_init,
      (GtkObjectInitFunc) ez_rect_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (ez_re_get_type (), &item_info);
  }

  return item_type;
}

static void
ez_rect_class_init (EZRectClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  EZItemClass* ezitem_class;
  EZContainerClass* container_class;
  EZREClass* ezre_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;
  ezitem_class = (EZItemClass* ) klass;
  container_class = (EZContainerClass* ) klass;
  ezre_class = (EZREClass*)klass;

  parent_class = gtk_type_class (ez_re_get_type ());

#if 0
  gtk_object_class_add_signals (object_class, item_signals, 
				LAST_SIGNAL);
#endif

  ezitem_class->construct     = ez_rect_construct;

  object_class->destroy = ez_rect_destroy;
  object_class->set_arg = ez_rect_set_arg;
  object_class->get_arg = ez_rect_get_arg;

  ezre_class->create_shape = ez_rect_create_shape;
}

static void
ez_rect_init (EZRect *ezrect)
{

}

static void
ez_rect_destroy (GtkObject *object)
{
  EZRect* ezrect;

  g_return_if_fail (object != NULL);
  g_return_if_fail (EZ_IS_RECT(object));

  ezrect = EZ_RECT (object);


  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ez_rect_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  EZRect* ezrect;

  item = GNOME_CANVAS_ITEM (object);
  ezrect = EZ_RECT (object);

  switch (arg_id) {

  default:
    g_warning("EZRE got an unknown arg type.");
    break;
  }

#if 0
  gnome_canvas_re_request_update(item);
#endif
}


static void
ez_rect_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  EZRect* ezrect;

  ezrect = EZ_RECT(object);

  switch (arg_id) {

  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

/************ End Canvas Item Functions ******************/


static void 
ez_rect_construct     (EZItem* ezi, EZPaste* paste)
{
  EZRect* ezrect;

  g_return_if_fail(ezi != NULL);
  g_return_if_fail(EZ_IS_RECT(ezi));
  g_return_if_fail(paste != NULL);
  g_return_if_fail(EZ_IS_PASTE(paste));

  ezrect = EZ_RECT(ezi);

  ez_debug("Constructing EZRect %p", ezi);

  if (EZ_ITEM_CLASS(parent_class)->construct)
    (*EZ_ITEM_CLASS(parent_class)->construct)(ezi,paste);
}

static void 
ez_rect_create_shape(EZRE* ezre)
{
  g_return_if_fail(ezre != NULL);
  g_return_if_fail(EZ_IS_RE(ezre));

  ez_debug("Creating shape for EZRect %p", ezre);

  ezre->shape = GNOME_CANVAS_RE(gnome_canvas_item_new(GNOME_CANVAS_GROUP(ezre),
                                                      gnome_canvas_rect_get_type(),
                                                      NULL));

}

EZItem*   
ez_rect_new (EZPaste* paste)
{
  EZItem* ezi = ez_item_build(paste,
                              ez_rect_get_type());

  g_return_val_if_fail(EZ_IS_RECT(ezi), NULL);

  return ezi;
}
