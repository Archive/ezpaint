/* Draggable handle thingy
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "ezhandle.h"

#include <config.h>

#include <math.h>

#include <gtk/gtksignal.h>

#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>


static void ez_handle_class_init (EZHandleClass *class);
static void ez_handle_init       (EZHandle      *handle);
static void ez_handle_destroy    (GtkObject            *object);
static void ez_handle_set_arg    (GtkObject            *object,
                                  GtkArg               *arg,
                                  guint                 arg_id);
static void ez_handle_get_arg    (GtkObject            *object,
                                  GtkArg               *arg,
                                  guint                 arg_id);
static void ez_handle_realize    (GnomeCanvasItem *item);

static void ez_handle_unrealize   (GnomeCanvasItem *item);
static void ez_handle_translate   (GnomeCanvasItem *item, double dx, double dy);

static void ez_handle_update      (GnomeCanvasItem *item, double *affine, ArtSVP *clip_path, int flags);

static gint ez_handle_event(GnomeCanvasItem* item, GdkEvent* event);

static double ez_handle_point (GnomeCanvasItem *item, double x, double y, 
                               int cx, int cy, GnomeCanvasItem **actual_item);

static void ez_handle_bounds (GnomeCanvasItem *item, double *x1, double *y1, double *x2, double *y2);

static void   ez_handle_render      (GnomeCanvasItem *item, GnomeCanvasBuf *buf);

static void gtk_marshal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE (GtkObject * object,
                                                           GtkSignalFunc func,
                                                           gpointer func_data,
                                                           GtkArg * args);

static GdkCursor* cursor(EZHandle* handle);

enum {
  ARG_0,
  ARG_PRELIGHT,
  ARG_ANCHOR,
  ARG_SENSITIVE,
  ARG_X, 
  ARG_Y
};


enum {
  MOVED,
  START_MOVE,
  STOP_MOVE,
  LAST_SIGNAL
};

#define HALF_A_HANDLE 3.0

static GnomeCanvasItemClass *parent_class;

static gint handle_signals[LAST_SIGNAL] = { 0 };

GtkType
ez_handle_get_type (void)
{
  static GtkType handle_type = 0;

  if (!handle_type) {
    GtkTypeInfo handle_info = {
      "EZHandle",
      sizeof (EZHandle),
      sizeof (EZHandleClass),
      (GtkClassInitFunc) ez_handle_class_init,
      (GtkObjectInitFunc) ez_handle_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    handle_type = gtk_type_unique (gnome_canvas_item_get_type (), &handle_info);
  }

  return handle_type;
}

static void
ez_handle_class_init (EZHandleClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;

  object_class = (GtkObjectClass *) klass;
  item_class = (GnomeCanvasItemClass *) klass;

  parent_class = gtk_type_class (gnome_canvas_item_get_type ());

  gtk_object_add_arg_type ("EZHandle::prelight", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_PRELIGHT);
  gtk_object_add_arg_type ("EZHandle::sensitive", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_SENSITIVE);
  gtk_object_add_arg_type ("EZHandle::x", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("EZHandle::y", GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("EZHandle::anchor", GTK_TYPE_ANCHOR_TYPE, GTK_ARG_READWRITE, ARG_ANCHOR);

  handle_signals[MOVED] =
    gtk_signal_new ("moved",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (EZHandleClass, moved),
		    gtk_marshal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE,
		    GTK_TYPE_NONE, 4, 
                    GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE,
                    GTK_TYPE_DOUBLE, GTK_TYPE_DOUBLE);

  handle_signals[START_MOVE] =
    gtk_signal_new ("start_move",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (EZHandleClass, start_move),
                    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);

  handle_signals[STOP_MOVE] =
    gtk_signal_new ("stop_move",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (EZHandleClass, stop_move),
                    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);

  gtk_object_class_add_signals (object_class, handle_signals, 
				LAST_SIGNAL);

  klass->moved = NULL;
  klass->start_move = NULL;
  klass->stop_move = NULL;

  item_class->realize     = ez_handle_realize;
  item_class->update      = ez_handle_update;
  item_class->render      = ez_handle_render;
  item_class->event       = ez_handle_event;
  item_class->point       = ez_handle_point;
  item_class->unrealize   = ez_handle_unrealize;
  item_class->translate   = ez_handle_translate;
  item_class->bounds      = ez_handle_bounds;

  object_class->destroy = ez_handle_destroy;
  object_class->set_arg = ez_handle_set_arg;
  object_class->get_arg = ez_handle_get_arg;
}

static void
ez_handle_init (EZHandle *handle)
{
  handle->x = handle->y = 0.0;

  handle->svp = NULL;

  handle->anchor = EZ_ANCHOR_CENTER;
  handle->sensitive = TRUE;
  handle->prelight = FALSE;
  handle->dragging = FALSE;
}

static void
ez_handle_destroy (GtkObject *object)
{
  EZHandle *handle;

  g_return_if_fail (object != NULL);
  g_return_if_fail (EZ_IS_HANDLE (object));

  handle = EZ_HANDLE (object);

  if (handle->svp) art_svp_free(handle->svp);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
recalc_coords (EZHandle *handle, 
	       double* x1, double* y1, double* x2, double* y2)
{
  GnomeCanvasItem *item;

  double wx, wy;
  double half;
  double onepixel;

  g_return_if_fail(x1 != NULL);
  g_return_if_fail(y1 != NULL);
  g_return_if_fail(x2 != NULL);
  g_return_if_fail(y2 != NULL);
  
  item = GNOME_CANVAS_ITEM (handle);
  
  wx = handle->x;
  wy = handle->y;
  
  onepixel = 1/item->canvas->pixels_per_unit;

  half = HALF_A_HANDLE/item->canvas->pixels_per_unit;
  
  /* Anchor handle */
  
  switch (handle->anchor) {
  case EZ_ANCHOR_NW:
  case EZ_ANCHOR_W:
  case EZ_ANCHOR_SW:
    /* onepixel prevents overlap with dragged item */
    *x1 = wx - half*2 - onepixel;
    *x2 = wx - onepixel;
    break;
    
  case EZ_ANCHOR_N:
  case EZ_ANCHOR_CENTER:
  case EZ_ANCHOR_S:
    *x1 = wx - half;
    *x2 = wx + half;
    break;
    
  case EZ_ANCHOR_NE:
  case EZ_ANCHOR_E:
  case EZ_ANCHOR_SE:
    *x1 = wx + onepixel;
    *x2 = wx + half*2 + onepixel;
    break;
  }
  
  switch (handle->anchor) {
  case EZ_ANCHOR_NW:
  case EZ_ANCHOR_N:
  case EZ_ANCHOR_NE:
    *y1 = wy - half*2 - onepixel;
    *y2 = wy - onepixel;
    break;
    
  case EZ_ANCHOR_W:
  case EZ_ANCHOR_CENTER:
  case EZ_ANCHOR_E:
    *y1 = wy - half;
    *y2 = wy + half;
    break;
    
  case EZ_ANCHOR_SW:
  case EZ_ANCHOR_S:
  case EZ_ANCHOR_SE:
    *y1 = wy + onepixel;
    *y2 = wy + half*2 + onepixel;
    break;
  }
}

static void
ez_handle_bounds (GnomeCanvasItem *item, double *x1, double *y1, 
                  double *x2, double *y2)
{
  EZHandle* handle = EZ_HANDLE (item);

  recalc_coords(handle,x1,y1,x2,y2);
}

static void
ez_handle_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  GnomeCanvasItem *item;
  EZHandle *handle;

  item = GNOME_CANVAS_ITEM (object);
  handle = EZ_HANDLE (object);

  switch (arg_id) {
  case ARG_PRELIGHT:
    handle->prelight = GTK_VALUE_BOOL(*arg);
    break;
  case ARG_SENSITIVE:
    handle->sensitive = GTK_VALUE_BOOL(*arg);
    break;
  case ARG_X:
    handle->x = GTK_VALUE_DOUBLE (*arg);
    break;
  case ARG_Y:
    handle->y = GTK_VALUE_DOUBLE (*arg);
    break;
  case ARG_ANCHOR:
    handle->anchor = GTK_VALUE_ENUM (*arg);
    break;
  default:
    g_warning("EZHandle got an unknown arg type.");
    break;
  }

  gnome_canvas_item_request_update(item);
}


static void
ez_handle_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
  EZHandle *handle;

  handle = EZ_HANDLE (object);

  switch (arg_id) {
  case ARG_PRELIGHT:
    GTK_VALUE_BOOL (*arg) = handle->prelight;
    break;
  case ARG_SENSITIVE:
    GTK_VALUE_BOOL (*arg) = handle->sensitive;
    break;
  case ARG_X:
    GTK_VALUE_DOUBLE (*arg) = handle->x;
    break;
  case ARG_Y:
    GTK_VALUE_DOUBLE (*arg) = handle->y;
    break;
  case ARG_ANCHOR:
    GTK_VALUE_ENUM (*arg) = handle->anchor;
    break;
  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}


static void
ez_handle_realize (GnomeCanvasItem *item)
{
  GdkColor color;
  EZHandle* handle = EZ_HANDLE(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->realize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->realize) (item);

}


static void 
ez_handle_unrealize   (GnomeCanvasItem *item)
{
  EZHandle* handle = EZ_HANDLE(item);

  if (GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize)
    (* GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize) (item);
}


static void 
ez_handle_translate   (GnomeCanvasItem *item, double dx, double dy)
{
  EZHandle* handle = EZ_HANDLE(item);
  
  handle->x += dx;
  handle->y += dy;
  
  gnome_canvas_item_request_update(item);
}

static void 
ez_handle_update (GnomeCanvasItem *item, double *affine, 
                  ArtSVP *clip_path, int flags)
{
  EZHandle* handle;
  ArtVpath vpath[11];
  ArtVpath *vpath2;
  double x0, y0, x1, y1;
  int i;
  double width;

  if (parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);

  handle = EZ_HANDLE (item);
  
  recalc_coords(handle, &x0, &y0, &x1, &y1);

  gnome_canvas_item_reset_bounds (item);

  i = 0;
  vpath[i].code = ART_MOVETO;
  vpath[i].x = x0;
  vpath[i].y = y0;
  i++;
  vpath[i].code = ART_LINETO;
  vpath[i].x = x0;
  vpath[i].y = y1;
  i++;
  vpath[i].code = ART_LINETO;
  vpath[i].x = x1;
  vpath[i].y = y1;
  i++;
  vpath[i].code = ART_LINETO;
  vpath[i].x = x1;
  vpath[i].y = y0;
  i++;
  vpath[i].code = ART_LINETO;
  vpath[i].x = x0;
  vpath[i].y = y0;
  i++;
  
    
  width = 1/item->canvas->pixels_per_unit; /* 1 pixel */
  
  if (width < 0.25) /* copied from rect-ellipse, not understood though. */
    width = 0.25;

  if (!handle->prelight && 
      x1 - width > x0 + width &&
      y1 - width > y0 + width) 
    {      
      vpath[i].code = ART_MOVETO;
      vpath[i].x = x0 + width;
      vpath[i].y = y0 + width;
      i++;
      vpath[i].code = ART_LINETO;
      vpath[i].x = x1 - width;
      vpath[i].y = y0 + width;
      i++;
      vpath[i].code = ART_LINETO;
      vpath[i].x = x1 - width;
      vpath[i].y = y1 - width;
      i++;
      vpath[i].code = ART_LINETO;
      vpath[i].x = x0 + width;
      vpath[i].y = y1 - width;
      i++;
      vpath[i].code = ART_LINETO;
      vpath[i].x = x0 + width;
      vpath[i].y = y0 + width;
      i++;
    }
  vpath[i].code = ART_END;
  vpath[i].x = 0;
  vpath[i].y = 0;
  
  vpath2 = art_vpath_affine_transform (vpath, affine);
  
  gnome_canvas_item_update_svp_clip (item, &handle->svp, 
                                     art_svp_from_vpath (vpath2), 
                                     clip_path);
  art_free (vpath2);
}

static void 
ez_handle_render(GnomeCanvasItem* item, GnomeCanvasBuf* buf)
{
  EZHandle* handle;
  handle = EZ_HANDLE (item);

  if (handle->svp != NULL) 
    {
      guint color = 0xff; /* Black for now, I want XOR though */
      
      gnome_canvas_render_svp (buf, handle->svp, color);
    }
}

#if 0
static void
queue_redraw(EZHandle* handle) 
{
  GnomeCanvasItem* item = GNOME_CANVAS_ITEM(handle);
  double wx1, wy1, wx2, wy2;
  int cx1, cy1, cx2, cy2;
  
  recalc_coords(handle, &wx1, &wy1, &wx2, &wy2);
  gnome_canvas_item_i2w(item, &wx1, &wy1);
  gnome_canvas_item_i2w(item, &wx2, &wy2);
  gnome_canvas_w2c(item->canvas, wx1, wy1, &cx1, &cy1);
  gnome_canvas_w2c(item->canvas, wx2, wy2, &cx2, &cy2);
  /* Use a slightly larger area for safety */
  gnome_canvas_request_redraw(item->canvas, cx1-1, cy1-1, cx2+1, cy2+1);
}
#endif

static GdkCursor*
cursor(EZHandle* handle)
{
  GdkCursor* ptr;
    
  ptr = NULL;
  switch (handle->anchor) {
  case EZ_ANCHOR_NW:
    ptr = gdk_cursor_new (GDK_TOP_LEFT_CORNER);
    break;
  case EZ_ANCHOR_W:
    ptr = gdk_cursor_new (GDK_LEFT_SIDE);
    break;
  case EZ_ANCHOR_SW:
    ptr = gdk_cursor_new (GDK_BOTTOM_LEFT_CORNER);
    break;
  case EZ_ANCHOR_N:
    ptr = gdk_cursor_new (GDK_TOP_SIDE);
    break;
  case EZ_ANCHOR_CENTER:
    ptr = gdk_cursor_new (GDK_CROSSHAIR);
    break;
  case EZ_ANCHOR_S:
    ptr = gdk_cursor_new (GDK_BOTTOM_SIDE);
    break;      
  case EZ_ANCHOR_NE:
    ptr = gdk_cursor_new (GDK_TOP_RIGHT_CORNER);
    break;
  case EZ_ANCHOR_E:
    ptr = gdk_cursor_new (GDK_RIGHT_SIDE);
    break;
  case EZ_ANCHOR_SE:
    ptr = gdk_cursor_new (GDK_BOTTOM_RIGHT_CORNER);
    break;
    
  default:
    g_warning("Bad anchor");
    break;
  }
  
  return ptr;
}

static void
switch_cursors_and_grab(EZHandle* handle, guint32 grab_time)
{
  if (!handle->dragging) {
    GdkCursor* ptr = cursor(handle);
  
    gnome_canvas_item_grab (GNOME_CANVAS_ITEM(handle),
			    GDK_POINTER_MOTION_MASK | 
			    GDK_BUTTON_RELEASE_MASK | 
			    GDK_BUTTON_PRESS_MASK,
			    ptr,
			    grab_time);

    gdk_cursor_destroy (ptr);
 
    handle->dragging = TRUE;

    gtk_signal_emit(GTK_OBJECT(handle), handle_signals[START_MOVE]);
  }
}

static void
ungrab(EZHandle* handle, guint32 ungrab_time)
{
  if (handle->dragging) {
    gnome_canvas_item_ungrab(GNOME_CANVAS_ITEM(handle), ungrab_time);
    handle->dragging = FALSE;

    gtk_signal_emit(GTK_OBJECT(handle), handle_signals[STOP_MOVE]);
  }
}

static gint 
ez_handle_event(GnomeCanvasItem* item, GdkEvent* event)
{
  EZHandle* handle = EZ_HANDLE(item);
  gboolean need_redraw = FALSE;

  switch (event->type){

  case GDK_ENTER_NOTIFY:
    handle->prelight = TRUE;
    need_redraw = TRUE;
    break;

  case GDK_LEAVE_NOTIFY:
    handle->prelight = FALSE;
    need_redraw = TRUE;
    break;
    
  case GDK_BUTTON_PRESS:
    if (!handle->sensitive) return FALSE;
    switch (event->button.button) {
    case 1:
      switch_cursors_and_grab(handle, event->button.time);
      break;

    default:
      return FALSE;
      break;
    }
    break; /* Button press */
    
  case GDK_BUTTON_RELEASE:
    switch (event->button.button) {
    case 1:
      if (!handle->dragging) return FALSE;
      else ungrab(handle, event->button.time);
      break;

    default:
      return FALSE;
      break;
    }
    break; /* Button release */
    
  case GDK_MOTION_NOTIFY:
    if (handle->dragging) {
      double newx, newy, dx, dy;
      /* Queue a redraw on the old location */
      /* queue_redraw(handle); */

      /* Now move the handle, only on the dimension(s) allowed
	 by the anchor type, snapping as needed. */
      newx = event->motion.x;
      newy = event->motion.y;
      dx = 0.0;
      dy = 0.0;

      gnome_canvas_item_w2i(item, &newx, &newy);

      switch (handle->anchor) {
      case EZ_ANCHOR_NW:
      case EZ_ANCHOR_SW:
      case EZ_ANCHOR_W:
      case EZ_ANCHOR_NE:
      case EZ_ANCHOR_SE:
      case EZ_ANCHOR_E:
      case EZ_ANCHOR_CENTER:
        dx = newx - handle->x;
	handle->x = newx;
	break;

      case EZ_ANCHOR_N:
      case EZ_ANCHOR_S:
	/* Can't move in X direction */
	break;

      default:
	g_warning("Bad anchor");
	break;
      }

      switch (handle->anchor) {
      case EZ_ANCHOR_NW:
      case EZ_ANCHOR_NE:
      case EZ_ANCHOR_N:
      case EZ_ANCHOR_SW:
      case EZ_ANCHOR_SE:
      case EZ_ANCHOR_S:
      case EZ_ANCHOR_CENTER:
        dy = newy - handle->y;
	handle->y = newy;
	break;

      case EZ_ANCHOR_W:
      case EZ_ANCHOR_E:
	/* Can't move in Y direction */
	break;
	
      default:
	g_warning("Bad anchor");
	break;
      }

      /*       recalc_bounds(handle); */

      gtk_signal_emit(GTK_OBJECT(handle), handle_signals[MOVED], handle->x, handle->y, dx, dy);
      need_redraw = TRUE;
    }
    else return FALSE;
    break; /* Motion notify */
    
  default:
    return FALSE;
  }

  /* Draw new location or redraw with/without fill */
  /* if (need_redraw) queue_redraw(handle); */
  if (need_redraw) gnome_canvas_item_request_update(item);
  
  return TRUE;
}

static double
ez_handle_point (GnomeCanvasItem *item, double x, double y, 
			   int cx, int cy, GnomeCanvasItem **actual_item)
{
  EZHandle *handle;
  double x1, y1, x2, y2;
  double dx, dy;

  handle = EZ_HANDLE (item);

  *actual_item = item;

  recalc_coords(handle,&x1,&y1,&x2,&y2);

  /* On top of the handle */

  if ((x >= x1) && (y >= y1) && (x <= x2) && (y <= y2)) {
    return 0.0;
  }

  /* Point is outside */

  if (x < x1)
    dx = x1 - x;
  else if (x > x2)
    dx = x - x2;
  else
    dx = 0.0;

  if (y < y1)
    dy = y1 - y;
  else if (y > y2)
    dy = y - y2;
  else
    dy = 0.0;

  return sqrt (dx * dx + dy * dy);
}

typedef void (*GtkSignal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE) (GtkObject * object,
                                                             gdouble arg1,
                                                             gdouble arg2,
                                                             gdouble arg3,
                                                             gdouble arg4,
                                                             gpointer user_data);

static void gtk_marshal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE (GtkObject * object,
                                                           GtkSignalFunc func,
                                                           gpointer func_data,
                                                           GtkArg * args)
{
  GtkSignal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE rfunc;
  rfunc = (GtkSignal_NONE__DOUBLE_DOUBLE_DOUBLE_DOUBLE) func;
  (*rfunc) (object,
	    GTK_VALUE_DOUBLE (args[0]),
	    GTK_VALUE_DOUBLE (args[1]),
            GTK_VALUE_DOUBLE (args[2]),
            GTK_VALUE_DOUBLE (args[3]),
	    func_data);
}

const gchar* 
ez_anchor_name(EZAnchorType a)
{
  switch (a)
    {
    case EZ_ANCHOR_CENTER:
      return "Center";
      break;
    
    case EZ_ANCHOR_NORTH:
      return "North";
      break;
    
    case EZ_ANCHOR_NORTH_WEST:
      return "NorthWest";
      break;
    
    case EZ_ANCHOR_NORTH_EAST:
      return "NorthEast";
      break;
    
    case EZ_ANCHOR_SOUTH:
      return "South";
      break;
    
    case EZ_ANCHOR_SOUTH_WEST:
      return "SouthWest";
      break;
    
    case EZ_ANCHOR_SOUTH_EAST:
      return "SouthEast";
      break;
    
    case EZ_ANCHOR_WEST:
      return "West";
      break;
    
    case EZ_ANCHOR_EAST:
      return "East";
      break;
 
    default:
      return "Unknown";
      break;
    }

  return "Unknown"; 
}
