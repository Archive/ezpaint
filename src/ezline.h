/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZLine: editable line class
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_LINE_H
#define EZ_LINE_H

#include "ezitem.h"

BEGIN_GNOME_DECLS

#define EZ_TYPE_LINE            (ez_line_get_type ())
#define EZ_LINE(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_LINE, EZLine))
#define EZ_LINE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_LINE, EZLineClass))
#define EZ_IS_LINE(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_LINE))
#define EZ_IS_LINE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_LINE))

typedef struct _EZLine EZLine;
typedef struct _EZLineClass EZLineClass;

struct _EZLine {
  EZItem ezitem;

  /* struct members are all private. */

};

struct _EZLineClass {
  EZItemClass parent_class;
  
};

/* Standard Gtk function */

GtkType   ez_line_get_type (void);

END_GNOME_DECLS

#endif

