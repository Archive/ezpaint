/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZPaste: Paste board for editable items. Acts as a background.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_PASTE_H
#define EZ_PASTE_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-rect-ellipse.h>
#include <libart_lgpl/art_svp.h>

BEGIN_GNOME_DECLS

#define EZ_TYPE_PASTE            (ez_paste_get_type ())
#define EZ_PASTE(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_PASTE, EZPaste))
#define EZ_PASTE_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_PASTE, EZPasteClass))
#define EZ_IS_PASTE(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_PASTE))
#define EZ_IS_PASTE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_PASTE))

typedef struct _EZPaste EZPaste;
typedef struct _EZPasteClass EZPasteClass;

struct _EZItem;

struct _EZPaste {
  GnomeCanvasGroup group;

  double x1;
  double y1;
  double x2;
  double y2;

  GSList* items;

  struct _EZItem* editing_item;

  gdouble hgridspace;
  gdouble vgridspace;

  guint32 bg_color;
  guint32 grid_color;

  ArtSVP* bg_svp;  
  ArtSVP* grid_svp;

  guint has_grid    : 1;
  guint has_bg      : 1;
  guint recalc_grid : 1;
  guint recalc_bg   : 1;
};

struct _EZPasteClass {
  GnomeCanvasGroupClass parent_class;

  /* Signals */

  /* Class functions */
  

};

/* Standard Gtk function */
GtkType        ez_paste_get_type (void);

/* Internal use; pass an event to the pasteboard */
void           ez_paste_pass_event(EZPaste* paste, 
                                   struct _EZItem* origin, 
                                   struct _EZItem* toplevel,
                                   GdkEvent* event);

/* Add/remove a toplevel item to the pasteboard */
void           ez_item_set_toplevel (struct _EZItem* ezi, gboolean setting);


/********* Ignore all this, internal library stuff ***********/
#ifdef GNOME_ENABLE_DEBUG
#ifdef	__GNUC__
#define	ez_debug(format, args...)	g_log (G_LOG_DOMAIN, \
					       G_LOG_LEVEL_DEBUG, \
					       format, ##args)
#else	/* !__GNUC__ */
static inline void
ez_debug (const gchar *format,
	 ...)
{
  va_list args;
  va_start (args, format);
  g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, format, args);
  va_end (args);
}
#endif	/* !__GNUC__ */

#else /* GNOME_ENABLE_DEBUG */
#ifdef __GNUC__
#define ez_debug(format, args...)
#else
static inline void 
ez_debug (const gchar* format, ...) {}
#endif /* !GNUC */
#endif /* !GNOME_ENABLE_DEBUG */

END_GNOME_DECLS

#endif

