/* Abstract base for items with handles
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_HANDLED_H
#define GNOME_CANVAS_HANDLED_H

#include <libgnome/gnome-defs.h>
/*#include "gnome-canvas.h"*/
#include <libgnomeui/gnome-canvas.h>
#include "gnome-canvas-handle.h"

BEGIN_GNOME_DECLS


#define GNOME_TYPE_CANVAS_HANDLED            (gnome_canvas_handled_get_type ())
#define GNOME_CANVAS_HANDLED(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_HANDLED, GnomeCanvasHandled))
#define GNOME_CANVAS_HANDLED_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_HANDLED, GnomeCanvasHandledClass))
#define GNOME_IS_CANVAS_HANDLED(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_HANDLED))
#define GNOME_IS_CANVAS_HANDLED_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_HANDLED))

#define GNOME_CANVAS_HANDLED_IS_SENSITIVE(obj)  (obj->sensitive)
#define GNOME_CANVAS_HANDLED_IS_SELECTED(obj)   (obj->selected)

typedef struct _GnomeCanvasHandled GnomeCanvasHandled;
typedef struct _GnomeCanvasHandledClass GnomeCanvasHandledClass;

struct _GnomeCanvasHandleGroup;

struct _GnomeCanvasHandled {
  GnomeCanvasGroup group;

  /* Parent GnomeCanvasHandleGroup */
  struct _GnomeCanvasHandleGroup* handle_group;
  
  /* Not every subclass must use these fields, but most will.  They
     should at least keep them updated for get_arg purposes. */
  GnomeCanvasSnapFunc snap_func;
  gpointer            snap_func_data;

  /* Use macros (above) to access these */
  guint selected  : 1;  /* handles are showing, etc. */
  guint sensitive : 1;  /* responds to events */
  guint dragging  : 1;  /* in a drag */
  guint translucent : 1; /* stippled to be sort of see-thru */
};

struct _GnomeCanvasHandledClass {
  GnomeCanvasGroupClass parent_class;

  /* Select/unselect signals */
  void (*selected)   (GnomeCanvasHandled* handled);  
  void (*unselected) (GnomeCanvasHandled* handled);

  /* This signal is called only if the *user* drags things around,
     not if the programmer sets them. Item coords. */
  void (* resized)(GnomeCanvasHandled* handled, double x1, double y1, double x2, double y2);

  /* This is an internal virtual function to handle scaling */
  void (* scale)  (GnomeCanvasHandled* handled, 
		   double x1, double y1, double x2, double y2);

  /* virtual function to get current size */
  void (* size)   (GnomeCanvasHandled* handled, 
		   double* x1, double* y1, double* x2, double* y2);

  /* Virtual function to interactively "create" a handled item; 
     pass in the user's click, or NULL for a random default position. */
  void (*create) (GnomeCanvasHandled* handled, GdkEventButton* event);
  /* virtual function */
  void (*set_snap_func) (GnomeCanvasHandled* handled, 
			 GnomeCanvasSnapFunc func, 
			 gpointer data);

  /* Not implemented for all items */
  void (*set_translucent) (GnomeCanvasHandled* handled,
			   gboolean translucency);
};

/* Standard Gtk function */
GtkType gnome_canvas_handled_get_type (void);

/* Emits signal only if there's a change  */
void                gnome_canvas_handled_set_selected (GnomeCanvasHandled* handled,
						       gboolean selected);


/* Emits signal only. */
void gnome_canvas_handled_resized(GnomeCanvasHandled* handled,
				  double x1, double y1, double x2, double y2);


void gnome_canvas_handled_scale(GnomeCanvasHandled* handled,
				gdouble x1, gdouble y1,
				gdouble x2, gdouble y2);

void gnome_canvas_handled_size (GnomeCanvasHandled* handled, 
				double* x1, double* y1, double* x2, double* y2);

/* Can set the args of the new object as usual, except that setting
   its position or editing state is probably a bad idea. Event should
   be NULL or a canvas item event; NOT a canvas event. */
GnomeCanvasHandled* gnome_canvas_handled_create   (GnomeCanvasGroup* parent, 
						   GdkEventButton* event, 
						   GtkType type, 
						   const char* first_arg_name, ...);

/* Set the snap for this item. It's dependent on the item's shape
   what this means exactly. Current item position isn't changed; this 
   only affects future moves. */
void    gnome_canvas_handled_set_snap_func(GnomeCanvasHandled* handled,
					   GnomeCanvasSnapFunc func, 
					   gpointer data);


void    gnome_canvas_handled_set_translucent(GnomeCanvasHandled* handled,
					     gboolean translucency);

END_GNOME_DECLS

#endif


