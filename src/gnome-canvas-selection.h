/* Selection made up of GnomeCanvasHandled items.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GNOME_CANVAS_SELECTION_H
#define GNOME_CANVAS_SELECTION_H

#include <libgnome/gnome-defs.h>
/*#include "gnome-canvas.h"*/
#include "gnome-canvas-handled.h"

BEGIN_GNOME_DECLS


#define GNOME_TYPE_CANVAS_SELECTION            (gnome_canvas_selection_get_type ())
#define GNOME_CANVAS_SELECTION(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_SELECTION, GnomeCanvasSelection))
#define GNOME_CANVAS_SELECTION_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_SELECTION, GnomeCanvasSelectionClass))
#define GNOME_IS_CANVAS_SELECTION(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_SELECTION))
#define GNOME_IS_CANVAS_SELECTION_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_SELECTION))


typedef struct _GnomeCanvasSelection GnomeCanvasSelection;
typedef struct _GnomeCanvasSelectionClass GnomeCanvasSelectionClass;

struct _GnomeCanvasSelection {
  GnomeCanvasHandled handled;
     
};

struct _GnomeCanvasSelectionClass {
  /* Selection doesn't implement Handled methods that make no sense
     (e.g. create, though create could do a rubberband eventually) */
  GnomeCanvasHandledClass parent_class;

};

/* Standard Gtk function */
GtkType gnome_canvas_selection_get_type (void);

END_GNOME_DECLS

#endif

