/* Internal object used in HandleGroup
 * 
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef GNOME_CANVAS_PRIVATE_GROUP_H
#define GNOME_CANVAS_PRIVATE_GROUP_H

#include "gnome-canvas-handled.h"

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_PRIVATE_GROUP            (gnome_canvas_private_group_get_type ())
#define GNOME_CANVAS_PRIVATE_GROUP(obj)            (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_PRIVATE_GROUP, GnomeCanvasPrivateGroup))
#define GNOME_CANVAS_PRIVATE_GROUP_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_PRIVATE_GROUP, GnomeCanvasPrivateGroupClass))
#define GNOME_IS_CANVAS_PRIVATE_GROUP(obj)         (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_PRIVATE_GROUP))
#define GNOME_IS_CANVAS_PRIVATE_GROUP_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_PRIVATE_GROUP))

typedef struct _GnomeCanvasPrivateGroup GnomeCanvasPrivateGroup;
typedef struct _GnomeCanvasPrivateGroupClass GnomeCanvasPrivateGroupClass;

struct _GnomeCanvasPrivateGroup {
  GnomeCanvasItem item;

  GSList* children;

  double x1, y1, x2, y2; /* The old values, so we can scale properly; also used 
			    if we just need some coords offhand, they're ordered 
			    unlike the PrivateBox coords (this is wasteful of RAM,
			    of course; we could do it with virtual functions instead) */

  guint open : 1;
};

struct _GnomeCanvasPrivateGroupClass {
  GnomeCanvasItemClass parent_class;
};

/* Standard Gtk function */
GtkType gnome_canvas_private_group_get_type (void);

void gnome_canvas_private_group_set_open (GnomeCanvasPrivateGroup* group, 
					  gboolean openness);


END_GNOME_DECLS

#endif

