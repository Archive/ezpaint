/*  -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
 *   EZContainer: abstract base class for editable canvas item containers.
 *
 * Copyright (C) 1998 Free Software Foundation
 *
 * Developed by Havoc Pennington <hp@pobox.com>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef EZ_CONTAINER_H
#define EZ_CONTAINER_H

#include "ezitem.h"

BEGIN_GNOME_DECLS

#define EZ_TYPE_CONTAINER            (ez_container_get_type ())
#define EZ_CONTAINER(obj)            (GTK_CHECK_CAST ((obj), EZ_TYPE_CONTAINER, EZContainer))
#define EZ_CONTAINER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), EZ_TYPE_CONTAINER, EZContainerClass))
#define EZ_IS_CONTAINER(obj)         (GTK_CHECK_TYPE ((obj), EZ_TYPE_CONTAINER))
#define EZ_IS_CONTAINER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), EZ_TYPE_CONTAINER))

typedef struct _EZContainer EZContainer;
typedef struct _EZContainerClass EZContainerClass;

struct _EZContainer {
  EZItem ezitem;

  GSList* children;

  EZItem* editing_child; /* child currently being edited, or NULL */
};

struct _EZContainerClass {
  EZItemClass parent_class;

  /* Signals */

  void (* add)           (EZContainer* ezc, EZItem* child);
  void (* remove)        (EZContainer* ezc, EZItem* child);

  /* Class functions */
  

};

/* Standard Gtk function */
GtkType        ez_container_get_type (void);

/* GnomeCanvas does not permit objects to be "floating" with no
   group. So we create the EZItems with the EZPaste as their parent,
   and when we "remove" we reparent to EZPaste and hide the item. Kind
   of ugly, but there's little alternative. */

void           ez_container_add      (EZContainer* ezc, EZItem* child);
void           ez_container_remove   (EZContainer* ezc, EZItem* child);

END_GNOME_DECLS

#endif
